# SAR App

SAR App est une application web, utilisée pour administrer les référentiels du [Système d'Information du Milieu Marin](https://www.milieumarinfrance.fr/) (SIMM).

> SAR App is a unhosted web application, used to managed referential of the French marine environment information system.

## Contribuer

Voir le [guide du développeur](./doc/build.md)

## Licence

Logiciel libre de droit, distribué sous [licence AGPL v3](./LICENSE).
