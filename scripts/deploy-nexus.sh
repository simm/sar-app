#!/bin/bash

# Get to the root project
if [[ "_" == "_${PROJECT_DIR}" ]]; then
  SCRIPT_DIR=$(dirname $0)
  PROJECT_DIR=$(cd ${SCRIPT_DIR}/.. && pwd)
  export PROJECT_DIR
fi;

projectName=quadmire
projectDir=$(pwd)
repoUrl=https://nexus.e-is.pro/nexus/content/repositories/sar-releases
groupId=fr.milieumarinfrance.sar.app
artifactId=sar-app

# Get current version (package.json)
version=$(grep -oP "version\": \"\d+.\d+.\d+(-(alpha|beta|rc)[0-9]+)?" package.json | grep -m 1 -oP "\d+.\d+.\d+(-(alpha|beta|rc)[0-9]+)?")
if [[ "_$version" == "_" ]]; then
  echo ">> Unable to read the current version in 'package.json'. Please check version format is: x.y.z (x and y should be an integer)."
  exit 1;
fi
echo "Current version: $version"

finalFileUrl=$repoUrl/${groupId//.//}/$artifactId/$version/$artifactId-$version.zip

# Check if the version is already deployed
if curl --output /dev/null --silent --head --fail "$finalFileUrl"; then
  echo "This version is already deployed"
  exit 1;
fi

# Zip the www folder
mkdir -p "${projectDir}/dist"
zipFile=${projectDir}/dist/$projectName-$version.zip
if [[ -f "$zipFile" ]]; then
  rm "$zipFile"
fi
cd "$projectDir"/www || exit 1
if ! zip -q -r "$zipFile" . ; then
  echo "Cannot create the archive for the web artifact"
  exit 1
fi

# Deploy to Nexus as a Maven artifact
mvn deploy:deploy-file -DrepositoryId=eis-nexus-deploy \
  -Dfile="$zipFile" \
  -Durl=$repoUrl \
  -DgroupId=$groupId \
  -DartifactId=$artifactId \
  -Dversion="$version" \
  -Dpackaging=zip

# Check if the version is correctly deployed
if curl --output /dev/null --silent --head --fail "$finalFileUrl"; then
  echo "$projectName $version correctly deployed"
else
  echo "Deployment failed"
  exit 1;
fi
