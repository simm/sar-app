import {NgModule} from '@angular/core';
import {AppCoreModule} from '../core/core.module';
import {ReferentialTable} from './generic/referential.table';
import {SelectReferentialModal} from './generic/select-referential.modal';
import {ReferentialRefTable} from './generic/referential-ref.table';
import {ReferentialToStringPipe} from './services/pipes/referential-to-string.pipe';
import {TranslateModule} from '@ngx-translate/core';
import {TextMaskModule} from 'angular2-text-mask';
import {CommonModule} from '@angular/common';
import {SimpleReferentialTable} from './generic/simple-referential.table';
import {ReferentialForm} from '@app/referential/generic/referential.form';
import {SoftwarePage} from '@app/referential/software/software.page';

@NgModule({
  imports: [
    CommonModule,
    TextMaskModule,
    TranslateModule.forChild(),

    AppCoreModule
  ],
  declarations: [
    // Pipes
    ReferentialToStringPipe,
    // Components
    ReferentialForm,
    ReferentialTable,
    SimpleReferentialTable,
    ReferentialRefTable,
    SelectReferentialModal,
    SoftwarePage
  ],
  exports: [
    TranslateModule,

    // Pipes
    ReferentialToStringPipe,

    // Components
    ReferentialForm,
    ReferentialTable,
    ReferentialRefTable,
    SelectReferentialModal,
    SoftwarePage
  ]
})
export class AppReferentialModule {
}
