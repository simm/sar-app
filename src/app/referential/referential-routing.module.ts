import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ReferentialTable} from './generic/referential.table';
import {AppReferentialModule} from './referential.module';
import {SharedRoutingModule} from '@sumaris-net/ngx-components';
import {SoftwarePage} from '@app/referential/software/software.page';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'list'
  },
  {
    path: 'list',
    pathMatch: 'full',
    component: ReferentialTable,
    runGuardsAndResolvers: 'pathParamsChange',
    data: {
      profile: 'ADMIN'
    }
  },
  {
    path: 'software/:id',
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: SoftwarePage,
        data: {
          profile: 'ADMIN'
        }
      }
    ]
  }
];

@NgModule({
  imports: [
    SharedRoutingModule,
    AppReferentialModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ReferentialRoutingModule { }
