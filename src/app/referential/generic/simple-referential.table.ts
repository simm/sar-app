import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Injector, Input, OnInit} from '@angular/core';
import {TableElement, ValidatorService} from '@e-is/ngx-material-table';
import {
  AccountService,
  AppInMemoryTable,
  InMemoryEntitiesService,
  isNotNil,
  LocalSettingsService,
  Referential,
  RESERVED_END_COLUMNS,
  RESERVED_START_COLUMNS,
  splitById
} from '@sumaris-net/ngx-components';
import {ActivatedRoute, Router} from '@angular/router';
import {ModalController, Platform} from '@ionic/angular';
import {environment} from '@environments/environment';
import {ReferentialValidatorService} from '@app/referential/services/validator/referential.validator';
import {ReferentialFilter} from '@app/referential/services/filter/referential.filter';
import {DEFAULT_STATUS_LIST} from '@app/referential/generic/referential.model';


@Component({
  selector: 'app-simple-referential-table',
  templateUrl: 'simple-referential.table.html',
  styleUrls: ['simple-referential.table.scss'],
  providers: [
    {provide: ValidatorService, useExisting: ReferentialValidatorService},
    {
      provide: InMemoryEntitiesService,
      useFactory: () => new InMemoryEntitiesService(Referential, ReferentialFilter)
    }
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SimpleReferentialTable
  extends AppInMemoryTable<Referential, Partial<ReferentialFilter>>
  implements OnInit {

  statusList = DEFAULT_STATUS_LIST;
  statusById = splitById(DEFAULT_STATUS_LIST);

  @Input() set entityName(entityName: string) {
    this.setFilter({
      ...this.filter,
      entityName
    });
  }

  get entityName(): string {
    return this.filter.entityName;
  }

  @Input() canEdit = false;
  @Input() canDelete = false;
  @Input() hasRankOrder: boolean;

  @Input() set showUpdateDateColumn(value: boolean) {
    this.setShowColumn('updateDate', value);
  }


  constructor(
    protected route: ActivatedRoute,
    protected router: Router,
    protected platform: Platform,
    protected modalCtrl: ModalController,
    protected accountService: AccountService,
    protected settings: LocalSettingsService,
    protected validatorService: ValidatorService,
    protected memoryDataService: InMemoryEntitiesService<Referential, ReferentialFilter>,
    protected cd: ChangeDetectorRef,
    protected injector: Injector
  ) {
    super(injector,
      // columns
      RESERVED_START_COLUMNS
        .concat([
          'label',
          'name',
          'description',
          'status',
          'updateDate',
          'comments'])
        .concat(RESERVED_END_COLUMNS),
      Referential,
      memoryDataService,
      validatorService,
      {
        onRowCreated: (row) => this.onRowCreated(row),
        prependNewElements: false,
        suppressErrors: true
      },
      {
        entityName: 'ConstructionCategory'
      });

    this.i18nColumnPrefix = 'REFERENTIAL.';
    this.autoLoad = false; // waiting parent to load
    this.inlineEdition = true;
    this.confirmBeforeDelete = true;
    this.showUpdateDateColumn = false;

    this.debug = !environment.production;
  }

  ngOnInit() {
    if (isNotNil(this.hasRankOrder)) {
      // TODO use the replace map ?
      //this.memoryDataService.hasRankOrder = this.hasRankOrder;
    }

    super.ngOnInit();
  }

  protected onRowCreated(row: TableElement<Referential>) {
    const defaultValues = {
      entityName: this.entityName
    };
    if (row.validator) {
      row.validator.patchValue(defaultValues);
    }
    else {
      Object.assign(row.currentData, defaultValues);
    }
  }

  protected markForCheck() {
    this.cd.markForCheck();
  }
}

