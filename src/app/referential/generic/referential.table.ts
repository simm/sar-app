import {Component, Inject, InjectionToken, Injector, Input, OnDestroy, OnInit, Optional} from '@angular/core';
import {BehaviorSubject, from} from 'rxjs';
import {debounceTime, filter, map, switchMap, tap} from 'rxjs/operators';
import {TableElement, ValidatorService} from '@e-is/ngx-material-table';
import {ReferentialValidatorService} from '../services/validator/referential.validator';
import {ReferentialService} from '../services/referential.service';
import {AbstractControl, FormBuilder} from '@angular/forms';
import {
  AccountService,
  AppFormUtils,
  BaseReferential,
  changeCaseToUnderscore,
  EntitiesServiceWatchOptions,
  Entity,
  EntityFilter,
  firstNotNilPromise,
  FormFieldDefinition,
  FormFieldType,
  IEntitiesService,
  IEntityService,
  IReferentialRef,
  isNil,
  isNotEmptyArray,
  isNotNil,
  isNotNilOrBlank,
  PlatformService,
  Referential,
  ReferentialRef,
  referentialToString,
  RESERVED_END_COLUMNS,
  RESERVED_START_COLUMNS,
  slideUpDownAnimation, Software,
  sort,
  splitById,
  StartableService,
  toNumber
} from '@sumaris-net/ngx-components';
import {BaseReferentialFilter, ReferentialFilter} from '../services/filter/referential.filter';
import {DEFAULT_STATUS_LIST} from '@app/referential/generic/referential.model';
import {CoastalStructureLevel, CoastalStructureType} from '@app/referential/coastal-structure/coastal-structure.model';
import {AppBaseTable, BASE_TABLE_SETTINGS_ENUM, BaseTableOptions} from '@app/shared/table/base.table';
import {AppReferential} from '@app/referential/services/model/referential.model';
import {environment} from '@environments/environment';
import {ReferentialRefService} from '@app/referential/services/referential-ref.service';


export interface ReferentialTableOptions<
  T extends Entity<T, ID>,
  ID = number,
  O extends EntitiesServiceWatchOptions = EntitiesServiceWatchOptions>
  extends BaseTableOptions<T, ID, O> {

  propertyNames?: string[];
}

export const DATA_TYPE = new InjectionToken<new () => Entity<any, any>>('dataType');
export const FILTER_TYPE = new InjectionToken<new () => EntityFilter<any, any>>('filterType');
export const DATA_SERVICE = new InjectionToken<new () => IEntityService<any>>('dataService');
export const OPTIONS = new InjectionToken<ReferentialTableOptions<any>>('options');



const IGNORED_ENTITY_COLUMNS = ['__typename', 'id', 'updateDate', 'entityName'];

@Component({
  selector: 'app-referential-table',
  templateUrl: 'referential.table.html',
  styleUrls: ['referential.table.scss'],
  providers: [
    {provide: ValidatorService, useExisting: ReferentialValidatorService},
    {provide: DATA_TYPE, useValue: AppReferential},
    {provide: FILTER_TYPE, useValue: ReferentialFilter},
    {provide: DATA_SERVICE, useExisting: ReferentialService},
    {provide: OPTIONS, useValue: <ReferentialTableOptions<any>>{}},
  ],
  animations: [slideUpDownAnimation]
})
export class ReferentialTable<
  T extends BaseReferential<T, ID> = AppReferential<any, any>,
  ID = number,
  F extends BaseReferentialFilter<F, T, ID> = ReferentialFilter<any, T, ID>
  >
  extends AppBaseTable<T, ID, F>
  implements OnInit, OnDestroy {

  static DEFAULT_ENTITY_NAME = CoastalStructureLevel.ENTITY_NAME;


  canEdit = false;
  $selectedEntity = new BehaviorSubject<{ id: string; label: string; level?: string; levelLabel?: string }>(undefined);
  $entities = new BehaviorSubject<{ id: string; label: string; level?: string; levelLabel?: string }[]>(undefined);
  $levels = new BehaviorSubject<IReferentialRef[]>(undefined);
  statusById: any;
  columnDefinitions: FormFieldDefinition[];

  // eslint-disable-next-line @typescript-eslint/member-ordering
  canOpenDetail = false;

  // Fill with specific page
  detailsPath = {
    'Software': '/referential/software/:id?label=:label',
  };
  entityNamesWithParent = [CoastalStructureType.ENTITY_NAME];

  @Input() statusList: any[] = [...DEFAULT_STATUS_LIST];
  @Input() canSelectEntity = true;
  @Input() title = 'REFERENTIAL.LIST.TITLE';

  @Input() set showLevelColumn(value: boolean) {
    this.setShowColumn('levelId', value);
  }

  get showLevelColumn(): boolean {
    return this.getShowColumn('levelId');
  }

  @Input() set showParentColumn(value: boolean) {
    this.setShowColumn('parent', value);
  }

  get showParentColumn(): boolean {
    return this.getShowColumn('parent');
  }

  @Input() set entityName(value: string) {
    if (this._entityName !== value) {
      this._entityName = value;
      if (!this.loadingSubject.getValue()) {
        this.applyEntityName(value, { skipLocationChange: true });
      }
    }
  }

  get entityName(): string {
    return this._entityName;
  }

  private _entityName: string;
  private _accountService: AccountService;
  private _referentialService: ReferentialService;
  private _referentialRefService: ReferentialRefService;
  private _formBuilder: FormBuilder;
  private _propertyNames: string[];

  constructor(
    injector: Injector,
    protected platform: PlatformService,
    @Optional() @Inject(DATA_TYPE) dataType?: new () => T,
    @Optional() @Inject(FILTER_TYPE) filterType?: new () => F,
    @Optional() @Inject(DATA_SERVICE) entityService?: IEntitiesService<T, F>,
    @Optional() @Inject(OPTIONS) options?: ReferentialTableOptions<T, ID>,
  ) {
    super(
      injector,
      null,
      dataType || AppReferential as unknown as new () => T,
      filterType || ReferentialFilter as unknown as new () => F,
      entityService || injector.get(ReferentialService) as unknown as IEntitiesService<T, F>,
      injector.get(ValidatorService),
      {
        prependNewElements: false,
        suppressErrors: environment.production,
        ...options,
        saveAllOptions: {
          saveOnlyDirtyRows: true,
          ...options?.saveAllOptions
        }
      }
    );
    this._formBuilder = injector.get(FormBuilder);
    this._accountService = injector.get(AccountService);
    this._referentialService = injector.get(ReferentialService);
    this._referentialRefService = injector.get(ReferentialRefService);

    this.i18nColumnPrefix = 'REFERENTIAL.';
    this.allowRowDetail = false;
    this.confirmBeforeDelete = true;

    // Allow inline edition only if admin
    this.inlineEdition = this._accountService.isAdmin();
    this.canEdit = this._accountService.isAdmin();
    this.autoLoad = false;

    // Compute columns from entity
    this._propertyNames = options?.propertyNames || this.getEntityDisplayProperties(dataType, this.validatorService);
    this.columns = RESERVED_START_COLUMNS
      .concat(this._propertyNames)
      .concat(RESERVED_END_COLUMNS);

    const filterConfig = this.getFilterFormConfig();
    this.filterForm = this._formBuilder.group(filterConfig || {});

    // Default hidden columns
    this.excludesColumns.push('parent');
    if (this.mobile) this.excludesColumns.push('updateDate');

    // FOR DEV ONLY
    this.debug = true;
  }

  ngOnInit() {
    super.ngOnInit();

    this.statusList = this.statusList.map(item => ReferentialRef.fromObject({
      id: item.id,
      name: this.translate.instant(item.label)
    }));

    // Fill statusById, if not set by input
    if (this.statusList && !this.statusById) {
      this.statusById = splitById(this.statusList);
    }

    // Configure autocomplete fields
    this.registerAutocompleteField('statusId', {
      items: this.statusList,
      attributes: ['name'],
      displayWith: (status) => {
        const statusId = toNumber(status?.id, status as number);
        return this.statusById[statusId]?.name;
      }
    });
    this.registerAutocompleteField('levelId', {
      items: this.$levels,
      displayWith: (level) => {
        if (isNil(level)) return '';
        level = isNil(level.id) && (this.$levels.value ||[]).find(item => item.id === level/*= should be an ID*/) || level;
        return referentialToString(level);
      }
    });

    this.registerAutocompleteField('parent', {
      suggestFn: (value, filter) => this._referentialRefService.suggest(value, {
        ...filter,
        entityName: this.entityName
      }),
      attributes: ['label', 'name'],
      displayWith: referentialToString
    });

    // /!\ Create column AFTER autocomplete (used in getColumnDefinitions() function)
    this.columnDefinitions = this.getColumnDefinitions(this._propertyNames);

    // Load entities (after platform ready)
    this.registerSubscription(
      from(this.ready())
        .pipe(
          switchMap(() => this._referentialService.watchTypes()),
          map(types => types.map(type => ({
              id: type.id,
              label: this.getI18nEntityName(type.id),
              level: type.level,
              levelLabel: this.getI18nEntityName(type.level)
            }))),
          map(types => sort(types, 'label'))
        )
        .subscribe(types => this.$entities.next(types))
    );

    this.registerSubscription(
      this.onRefresh.subscribe(() => {
        this.filterForm.markAsUntouched();
        this.filterForm.markAsPristine();
      }));

    // Update filter when changes
    this.registerSubscription(
      this.filterForm.valueChanges
        .pipe(
          debounceTime(250),
          filter((_) => {
            const valid = this.filterForm.valid;
            if (!valid && this.debug) AppFormUtils.logFormErrors(this.filterForm);
            return valid;
          }),
          // Update the filter, without reloading the content
          tap(json => this.setFilter(json, {emitEvent: false})),
          // Save filter in settings (after a debounce time)
          debounceTime(500),
          tap(json => this.settings.savePageSetting(this.settingsId, json, BASE_TABLE_SETTINGS_ENUM.filterKey))
        )
        .subscribe()
      );

    this.registerSubscription(this.onStartEditingRow.subscribe(row => this.startEditingRow(row)));

    if (this.canSelectEntity) {
      this.restoreFilterOrLoad();
    }
    else if (this._entityName) {
      this.applyEntityName(this._entityName);
    }

    this.markAsReady();
  }

  async ready(): Promise<void> {
    await (this.entityService instanceof StartableService
      ? this.entityService.ready()
      : this.platform.ready());

    return super.ready();
  }

  async applyEntityName(entityName: string, opts?: { emitEvent?: boolean; skipLocationChange?: boolean }) {
    opts = {emitEvent: true, skipLocationChange: false, ...opts};
    this._entityName = entityName;

    this.canOpenDetail = false;

    // Wait end of entities loading
    if (this.canSelectEntity) {
      const entities = await firstNotNilPromise(this.$entities);

      const entity = entities.find(e => e.id === entityName);
      if (!entity) {
        throw new Error(`[referential] Entity {${entityName}} not found !`);
      }

      this.$selectedEntity.next(entity);
    }

    // Load levels
    await this.loadLevels(entityName);

    // Hide parent columns
    // FIXME: EntityClasses.get() not working -> always empty. Try to register the map into the angular injector ?
    this.showParentColumn = this.entityNamesWithParent.includes(entityName);

    this.canOpenDetail = !!this.detailsPath[entityName];
    this.inlineEdition = !this.canOpenDetail;

    // Applying the filter (will reload if emitEvent = true)
    const filter = this.asFilter({
      ...this.filterForm.value,
      entityName
    });
    this.filterForm.patchValue({entityName}, {emitEvent: false});
    this.setFilter(filter, {emitEvent: opts.emitEvent});

    if (this.canSelectEntity) {
      // Update route location
      if (opts.skipLocationChange !== true) {
        this.router.navigate(['.'], {
          relativeTo: this.route,
          skipLocationChange: false,
          queryParams: {
            entity: entityName
          }
        });
      }
      // Save to settings
      if (opts.emitEvent !== false) {
        this.settings.savePageSetting(this.settingsId, filter.asObject(), BASE_TABLE_SETTINGS_ENUM.filterKey);
      }
    }
  }

  async onEntityNameChange(entityName: string): Promise<any> {
    // No change: skip
    if (this._entityName === entityName) return;
    this.applyEntityName(entityName);
  }

  async addRow(event?: any): Promise<boolean> {
    // Create new row
    const result = super.addRow(event);
    if (!result) return result;

    const row = this.dataSource.getRow(-1);
    row.validator.controls['entityName'].setValue(this._entityName);
    return true;
  }

  async loadLevels(entityName: string): Promise<Referential[]> {
    const res = await this._referentialService.loadLevels(entityName, {
      fetchPolicy: 'network-only'
    });

    if (this.canSelectEntity) {
      this.showLevelColumn = isNotEmptyArray(res);
    }

    this.$levels.next(res);

    return res;
  }

  getI18nEntityName(entityName: string, self?: ReferentialTable<T, ID, F>): string {
    self = self || this;

    if (isNil(entityName)) return undefined;

    const tableName = entityName.replace(/([a-z])([A-Z])/g, '$1_$2').toUpperCase();
    const key = `REFERENTIAL.ENTITY.${tableName}`;
    let message = self.translate.instant(key);

    if (message !== key) return message;
    // No I18n translation: continue

    // Use tableName, but replace underscore with space
    message = tableName.replace(/[_-]+/g, ' ').toUpperCase() || '';
    // First letter as upper case
    if (message.length > 1) {
      return message.substring(0, 1) + message.substring(1).toLowerCase();
    }
    return message;
  }

  async openRow(id: ID, row: TableElement<T>): Promise<boolean> {
    const path = this.detailsPath[this._entityName];

    if (isNotNilOrBlank(path)) {
      await this.router.navigateByUrl(
        path
          // Replace the id in the path
          .replace(':id', isNotNil(row.currentData.id) ? row.currentData.id.toString() : '')
          // Replace the label in the path
          .replace(':label', row.currentData.label || '')
      );
      return true;
    }

    return super.openRow(id, row);
  }

  clearControlValue(event: UIEvent, formControl: AbstractControl): boolean {
    if (event) event.stopPropagation(); // Avoid to enter input the field
    formControl.setValue(null);
    return false;
  }

  applyFilterAndClosePanel(event?: UIEvent) {
    const filter = this.filterForm.value;
    this.setFilter(filter, {emitEvent: false});
    super.applyFilterAndClosePanel(event);
  }

  resetFilter(event?: UIEvent) {
    this.filterForm.reset({entityName: this._entityName}, {emitEvent: true});
    this.setFilter(this.asFilter(<F>{entityName: this._entityName}), {emitEvent: true});
    this.filterCriteriaCount = 0;
    if (this.filterExpansionPanel && this.filterPanelFloating) this.filterExpansionPanel.close();
  }

  openUri(event: UIEvent, uri: string) {
    if (this.platform instanceof PlatformService) {
      this.platform.open(uri, '_blank');
    }
  }

  /* -- protected function -- */


  getEntityDisplayProperties<T>(dataType: new () => any, validatorService?: ValidatorService): string[] {
    return Object.keys(validatorService && validatorService.getRowValidator().controls || new dataType())
      .filter(key => !IGNORED_ENTITY_COLUMNS.includes(key));
  }

  getColumnType(key: string): FormFieldType {
    key = key.toLowerCase();
    if (key.endsWith('date')) return 'dateTime';
    if (key.endsWith('uri')) return 'string';
    if (key.endsWith('label') || key.endsWith('name') || key.endsWith('code')) return 'string';
    return 'string';
  }

  protected getColumnDefinitions(properties: string[]): FormFieldDefinition[] {
    return properties
      .map(key => {
        const label = (this.i18nColumnPrefix || '') + changeCaseToUnderscore(key).toUpperCase();
        const autocomplete = this.autocompleteFields[key];
        const type = autocomplete ? 'entity' : this.getColumnType(key);
        return <FormFieldDefinition>{
          key,
          label,
          type,
          autocomplete
        };
      })
      .filter(isNotNil)
      .filter(def => !IGNORED_ENTITY_COLUMNS.includes(def.key));
  }

  protected getFilterFormConfig(): any {
    console.debug('[referential-table] Creating filter form group...');

    // Base form config
    const config = {
      entityName: [null],
      searchText: [null],
      levelId: [null],
      parentId: [null],
      statusId: [null]
    };

    // Add other properties
    return Object.keys(new this.filterType())
      .filter(key => !IGNORED_ENTITY_COLUMNS.includes(key) && !config[key])
      .reduce((config, key) => {
        console.debug(this.logPrefix + ' Adding filter control: ' + key);
        config[key] = [null];
        return config;
      }, config);
  }

  protected async restoreFilterOrLoad() {
    this.markAsLoading();

    let json: Partial<F>;
    let skipLocationChange = false;

    // Check route parameters
    const {entity, q, level, status} = this.route.snapshot.queryParams;
    if (entity) {
      json = <Partial<F>>{
        entityName: entity,
        searchText: q || null,
        levelId: isNotNil(level) ? +level : null,
        statusId: isNotNil(status) ? +status : null
      };
      skipLocationChange = true;
      console.debug('[referentials] Restoring filter from route path...', json);
    }

    // Restore from settings
    else {
      json = this.settings.getPageSettings(this.settingsId, BASE_TABLE_SETTINGS_ENUM.filterKey);
      if (json?.entityName) {
        console.debug('[referentials] Restoring filter from settings...', json);
      }
    }
    if (json?.entityName) {
      const filter = this.asFilter(json);
      this.filterForm.patchValue(json, {emitEvent: false});
      this.filterCriteriaCount = filter.countNotEmptyCriteria();
      this.markForCheck();
      return this.applyEntityName(filter.entityName, {skipLocationChange});
    }

    // Load default entity
    await this.applyEntityName(ReferentialTable.DEFAULT_ENTITY_NAME);
  }

  protected async openNewRowDetail(): Promise<boolean> {
    const path = this.detailsPath[this._entityName];

    if (path) {
      await this.router.navigateByUrl(path
        .replace(':id', 'new')
        .replace(':label', ''));
      return true;
    }

    return super.openNewRowDetail();
  }

  protected startEditingRow(row: TableElement<T>) {
    console.debug(this.logPrefix + ' Start editing row');
  }
}

