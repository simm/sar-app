import {changeCaseToUnderscore, FormFieldDefinition, FormFieldDefinitionMap} from '@sumaris-net/ngx-components';
import {TypePolicies} from '@apollo/client';

export const REFERENTIAL_GRAPHQL_TYPE_POLICIES = <TypePolicies>{
  ReferentialVO: {
    keyFields: ['entityName', 'id']
  }
};

export const REFERENTIAL_CONFIG_OPTIONS: FormFieldDefinitionMap = {
  RDF_MODEL_PREFIX: <FormFieldDefinition>{
    key: 'rdf.model.prefix',
    label: 'CONFIGURATION.OPTIONS.RDF.MODEL_PREFIX',
    type: 'string',
    defaultValue: 'sar'
  },
  RDF_MODEL_VERSION: <FormFieldDefinition>{
    key: 'rdf.model.version',
    label: 'CONFIGURATION.OPTIONS.RDF.MODEL_VERSION',
    type: 'string',
    defaultValue: '0.1'
  },
  RDF_MODEL_BASE_URI: <FormFieldDefinition>{
    key: 'rdf.model.baseUri',
    label: 'CONFIGURATION.OPTIONS.RDF.MODEL_BASE_URI',
    type: 'string',
    defaultValue: 'http://id.milieumarinfrance.fr/'
  },
  RDF_MODEL_LANGUAGE: <FormFieldDefinition>{
    key: 'rdf.model.language',
    label: 'CONFIGURATION.OPTIONS.RDF.MODEL_LANGUAGE',
    type: 'string',
    defaultValue: 'fr'
  },
  RDF_MODEL_DESCRIPTION: <FormFieldDefinition>{
    key: 'rdf.model.description',
    label: 'CONFIGURATION.OPTIONS.RDF.MODEL_DESCRIPTION',
    type: 'string',
    defaultValue: 'Modèle de données du SAR'
  }
};


export const REFERENTIAL_LOCAL_SETTINGS_OPTIONS: FormFieldDefinitionMap = {

  // Display attributes for referential useful entities
  ... ['constructionCategory', 'constructionClass', 'constructionType']
    // Allow user to choose how to display field (by code+label, code, etc)
    .reduce((res, fieldName) => {
      res[`FIELD_${changeCaseToUnderscore(fieldName).toUpperCase()}_ATTRIBUTES`] = {
        key: `sumaris.field.${fieldName}.attributes`,
        label: `SETTINGS.FIELDS.${changeCaseToUnderscore(fieldName).toUpperCase()}`,
        type: 'enum',
        values: [
          {key: 'label,name',   value: 'SETTINGS.FIELDS.ATTRIBUTES.LABEL_NAME'},
          {key: 'name',         value: 'SETTINGS.FIELDS.ATTRIBUTES.NAME'},
          {key: 'name,label',   value: 'SETTINGS.FIELDS.ATTRIBUTES.NAME_LABEL'},
          {key: 'label',        value: 'SETTINGS.FIELDS.ATTRIBUTES.LABEL'}
        ]
      };
      return res;
    }, {})
};
