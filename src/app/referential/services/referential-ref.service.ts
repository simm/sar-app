import {Injectable} from '@angular/core';
import {FetchPolicy, gql} from '@apollo/client/core';
import {Observable, Subject} from 'rxjs';
import {first, map} from 'rxjs/operators';
import {ErrorCodes} from './errors';
import {
  AccountService, BaseEntityGraphqlQueries,
  BaseGraphqlService, ConfigService, Configuration,
  EntitiesStorage, firstTruePromise,
  fromDateISOString,
  GraphqlService,
  IEntitiesService,
  isEmptyArray,
  LoadResult,
  NetworkService,
  Referential,
  ReferentialRef,
  ReferentialUtils,
  SuggestService
} from '@sumaris-net/ngx-components';
import {ReferentialFragments} from './referential.fragments';
import {SortDirection} from '@angular/material/sort';
import {Moment} from 'moment';
import {environment} from '@environments/environment';
import {ReferentialRefFilter} from './filter/referential-ref.filter';
import {LocationLevelIds} from '@app/hubeau/service/hubeau.model';

const LastUpdateDate: any = gql`
  query LastUpdateDate{
    lastUpdateDate
  }
`;

const LoadAllQuery: any = gql`
  query ReferentialRefs($entityName: String, $offset: Int, $size: Int, $sortBy: String, $sortDirection: String, $filter: ReferentialFilterVOInput){
    data: referentials(entityName: $entityName, offset: $offset, size: $size, sortBy: $sortBy, sortDirection: $sortDirection, filter: $filter){
      ...ReferentialFragment
    }
  }
  ${ReferentialFragments.referential}
`;

const LoadAllWithTotalQuery: any = gql`
  query ReferentialRefsWithTotal($entityName: String, $offset: Int, $size: Int, $sortBy: String, $sortDirection: String, $filter: ReferentialFilterVOInput){
    data: referentials(entityName: $entityName, offset: $offset, size: $size, sortBy: $sortBy, sortDirection: $sortDirection, filter: $filter){
      ...ReferentialFragment
    }
    total: referentialsCount(entityName: $entityName, filter: $filter)
  }
  ${ReferentialFragments.referential}
`;

const LoadAllTaxonNamesQuery: any = gql`
  query TaxonNames($offset: Int, $size: Int, $sortBy: String, $sortDirection: String, $filter: TaxonNameFilterVOInput){
    data: taxonNames(offset: $offset, size: $size, sortBy: $sortBy, sortDirection: $sortDirection, filter: $filter){
      ...FullTaxonNameFragment
    }
  }
  ${ReferentialFragments.fullTaxonName}
`;

export const ReferentialRefQueries: BaseEntityGraphqlQueries = {
  loadAll: LoadAllQuery,
  loadAllWithTotal: LoadAllWithTotalQuery,
};

@Injectable({providedIn: 'root'})
export class ReferentialRefService extends BaseGraphqlService<ReferentialRef, ReferentialRefFilter>
  implements SuggestService<ReferentialRef, ReferentialRefFilter>,
      IEntitiesService<ReferentialRef, ReferentialRefFilter> {

  constructor(
    protected graphql: GraphqlService,
    protected accountService: AccountService,
    protected network: NetworkService,
    protected entities: EntitiesStorage,
    protected configService: ConfigService
  ) {
    super(graphql, environment);
  }

  protected async ngOnStart(): Promise<void> {

    const $ready = new Subject<void>();

    this.registerSubscription(
      this.configService.config.subscribe(config => {
        this.updateModelEnumerations(config);
        if (!$ready.isStopped) {
          $ready.next();
          $ready.complete();
        }
      })
    );

    return $ready.pipe(first()).toPromise();
  }

  /**
   * @param offset
   * @param size
   * @param sortBy
   * @param sortDirection
   * @param filter
   * @param opts
   */
  watchAll(offset: number,
           size: number,
           sortBy?: string,
           sortDirection?: SortDirection,
           filter?: Partial<ReferentialRefFilter>,
           opts?: {
             [key: string]: any;
             fetchPolicy?: FetchPolicy;
             withTotal?: boolean;
             toEntity?: boolean;
           }): Observable<LoadResult<ReferentialRef>> {

    if (!filter || !filter.entityName) {
      console.error('[referential-ref-service] Missing filter.entityName');
      // eslint-disable-next-line no-throw-literal
      throw {code: ErrorCodes.LOAD_REFERENTIAL_ERROR, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR'};
    }

    const entityName = filter.entityName;
    filter = this.asFilter(filter);

    const variables: any = {
      entityName,
      offset: offset || 0,
      size: size || 100,
      sortBy: sortBy || filter.searchAttribute || 'label',
      sortDirection: sortDirection || 'asc'
    };

    let now = this._debug && Date.now();
    if (this._debug) console.debug(`[referential-ref-service] Watching ${entityName} items...`, variables);
    let res: Observable<LoadResult<any>>;

    if (this.network.offline) {
      res = this.entities.watchAll(entityName,
        {
          ...variables,
          filter: filter && filter.asFilterFn()
        });
    }

    else {
      const query = (!opts || opts.withTotal !== false) ? LoadAllWithTotalQuery : LoadAllQuery;
      res = this.graphql.watchQuery<LoadResult<any>>({
        query,
        variables: {
          ...variables,
          filter: filter && filter.asPodObject()
        },
        error: {code: ErrorCodes.LOAD_REFERENTIAL_ERROR, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR'},
        fetchPolicy: opts && opts.fetchPolicy || 'cache-first'
      });
    }

    return res
      .pipe(
        map(({data, total}) => {
          const entities = (!opts || opts.toEntity !== false)
            ? (data || []).map(ReferentialRef.fromObject)
            : (data || []) as ReferentialRef[];
          if (now) {
            console.debug(`[referential-ref-service] References on ${entityName} loaded in ${Date.now() - now}ms`);
            now = undefined;
          }
          return {
            data: entities,
            total
          };
        })
      );
  }

  async loadAll(offset: number,
                size: number,
                sortBy?: string,
                sortDirection?: SortDirection,
                filter?: Partial<ReferentialRefFilter>,
                opts?: {
                  [key: string]: any;
                  fetchPolicy?: FetchPolicy;
                  debug?: boolean;
                  withTotal?: boolean;
                  toEntity?: boolean;
                }): Promise<LoadResult<ReferentialRef>> {


    const offline = this.network.offline && (!opts || opts.fetchPolicy !== 'network-only');
    if (offline) {
      return this.loadAllLocally(offset, size, sortBy, sortDirection, filter, opts);
    }

    const entityName = filter && filter.entityName;
    if (!entityName) {
      console.error('[referential-ref-service] Missing filter.entityName');
      throw {code: ErrorCodes.LOAD_REFERENTIAL_ERROR, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR'};
    }

    filter = this.asFilter(filter);
    const uniqueEntityName = entityName + (filter.searchJoin || '');

    const debug = this._debug && (!opts || opts.debug !== false);

    const variables = {
      entityName,
      offset: offset || 0,
      size: size || 100,
      sortBy: sortBy || filter.searchAttribute
        || filter.searchAttributes && filter.searchAttributes.length && filter.searchAttributes[0]
        || 'label',
      sortDirection: sortDirection || 'asc',
      filter: filter.asPodObject()
    };
    const now = debug && Date.now();
    if (debug) console.debug(`[referential-ref-service] Loading ${uniqueEntityName} items (ref)...`, variables);

    // Online mode: use graphQL
    const query = (!opts || opts.withTotal !== false) ? LoadAllWithTotalQuery : LoadAllQuery;
    const { data, total } = await this.graphql.query<LoadResult<any>>({
      query,
      variables,
      error: {code: ErrorCodes.LOAD_REFERENTIAL_ERROR, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR'},
      fetchPolicy: opts && opts.fetchPolicy || 'cache-first'
    });

    const entities = (!opts || opts.toEntity !== false) ?
      (data || []).map(ReferentialRef.fromObject) :
      (data || []) as ReferentialRef[];

    // Force entity name (if searchJoin)
    if (filter.entityName !== uniqueEntityName) {
      entities.forEach(item => item.entityName = uniqueEntityName);
    }

    if (debug) console.debug(`[referential-ref-service] Loading ${uniqueEntityName} items (ref) [OK] ${entities.length} items, in ${Date.now() - now}ms`);
    return {
      data: entities,
      total
    };
  }

  protected async loadAllLocally(offset: number,
                size: number,
                sortBy?: string,
                sortDirection?: SortDirection,
                filter?: Partial<ReferentialRefFilter>,
                opts?: {
                  [key: string]: any;
                  toEntity?: boolean;
                }): Promise<LoadResult<ReferentialRef>> {

    if (!filter || !filter.entityName) {
      console.error('[referential-ref-service] Missing argument \'filter.entityName\'');
      // eslint-disable-next-line no-throw-literal
      throw {code: ErrorCodes.LOAD_REFERENTIAL_ERROR, message: 'REFERENTIAL.ERROR.LOAD_REFERENTIAL_ERROR'};
    }
    const uniqueEntityName = filter.entityName + (filter.searchJoin || '');
    filter = this.asFilter(filter);

    const variables = {
      entityName: filter.entityName,
      offset: offset || 0,
      size: size || 100,
      sortBy: sortBy || filter.searchAttribute
        || filter.searchAttributes && filter.searchAttributes.length && filter.searchAttributes[0]
        || 'label',
      sortDirection: sortDirection || 'asc',
      filter: filter.asFilterFn()
    };

    const {data, total} = await this.entities.loadAll(uniqueEntityName + 'VO', variables);

    const entities = (!opts || opts.toEntity !== false) ?
      (data || []).map(ReferentialRef.fromObject) :
      (data || []) as ReferentialRef[];

    // Force entity name (if searchJoin)
    if (filter.entityName !== uniqueEntityName) {
      entities.forEach(item => item.entityName = uniqueEntityName);
    }

    return { data: entities, total };
  }

  async load(id: number,
             entityName: string,
             opts?: {
               [key: string]: any;
               fetchPolicy?: FetchPolicy;
               debug?: boolean;
               toEntity?: boolean;
             }): Promise<ReferentialRef> {
    const res = await this.loadAll(0, 1, null, null, {id, entityName}, opts);
    if (!res || isEmptyArray(res.data)) return undefined;
    return res.data[0];
  }

  async suggest(value: any, filter?: Partial<ReferentialRefFilter>, sortBy?: keyof Referential, sortDirection?: SortDirection,
                opts?: {
                  fetchPolicy?: FetchPolicy;
                }): Promise<LoadResult<ReferentialRef>> {
    if (ReferentialUtils.isNotEmpty(value)) return {data: [value]};
    value = (typeof value === 'string' && value !== '*') && value || undefined;
    return this.loadAll(0, !value ? 30 : 10, sortBy, sortDirection,
      { ...filter, searchText: value},
      { withTotal: true /* Used by autocomplete */ , ...opts }
    );
  }

  saveAll(data: ReferentialRef[], options?: any): Promise<ReferentialRef[]> {
    throw new Error('Not implemented yet');
  }

  deleteAll(data: ReferentialRef[], options?: any): Promise<any> {
    throw new Error('Not implemented yet');
  }

  async lastUpdateDate(opts?: {fetchPolicy?: FetchPolicy}): Promise<Moment> {
    try {
      const {lastUpdateDate} = await this.graphql.query<{lastUpdateDate: string}>({
        query: LastUpdateDate,
        variables: {},
        fetchPolicy: opts && opts.fetchPolicy || 'network-only'
      });

      return fromDateISOString(lastUpdateDate);
    }
    catch (err) {
      console.error('[referential-ref] Cannot get remote lastUpdateDate: ' + (err && err.message || err), err);
      return undefined;
    }
  }

  asFilter(filter: Partial<ReferentialRefFilter>): ReferentialRefFilter {
    return ReferentialRefFilter.fromObject(filter);
  }

  private updateModelEnumerations(config: Configuration) {
    if (!config.properties) {
      console.warn('[referential-ref] No properties found in pod config! Skip model enumerations update');
      return;
    }
    console.info('[referential-ref] Updating model enumerations...');
  }
}
