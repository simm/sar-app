import {Injectable} from '@angular/core';
import {AbstractControlOptions, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Referential, AppValidatorService, SharedValidators} from '@sumaris-net/ngx-components';
import {AppReferential} from '@app/referential/services/model/referential.model';

@Injectable({providedIn: 'root'})
export class ReferentialValidatorService<T extends AppReferential = AppReferential>
  extends AppValidatorService<T> {

  constructor(protected formBuilder: FormBuilder) {
    super(formBuilder);
  }

  getRowValidator(): FormGroup {
    return this.getFormGroup();
  }

  getFormGroup(data?: T, opts?: {
    withDescription?: boolean;
    withComments?: boolean;
  }): FormGroup {
    return this.formBuilder.group(
      this.getFormGroupConfig(data, opts),
      this.getFormGroupOptions(data, opts)
    );
  }

  getFormGroupConfig(data?: T, opts?: {
    withDescription?: boolean;
    withComments?: boolean;
    withUri?: boolean;
  }): {[key: string]: any} {
    const controlsConfig: {[key: string]: any} = {
      id: [data && data.id || null],
      label: [data && data.label || null, Validators.compose([Validators.required, Validators.maxLength(50)])],
      name: [data && data.name || null, Validators.compose([Validators.required, Validators.maxLength(100)])],
      levelId: [data && data.levelId || null],
      parent: [data && data.parent || null],
      statusId: [data && data.statusId || null, Validators.required],
      creationDate: [data && data.creationDate || null],
      updateDate: [data && data.updateDate || null],
      entityName: [data && data.entityName || null, Validators.required]
    };

    if (!opts || opts.withDescription !== false) {
      controlsConfig.description = [data && data.description || null, Validators.maxLength(2000)];
    }
    if (!opts || opts.withComments !== false) {
      controlsConfig.comments = [data && data.comments || null, Validators.maxLength(2000)];
    }
    if (!opts || opts.withUri !== false) {
      // TODO : configure as required ?
      //controlsConfig.uri = [data && data.uri || null, Validators.compose([Validators.required, Validators.maxLength(255)])];
      //controlsConfig.uri = [data && data.uri || null, Validators.maxLength(255)];
    }
    return controlsConfig;
  }

  getFormGroupOptions(data?: T, opts?: any): AbstractControlOptions | null {
    return null;
  }
}
