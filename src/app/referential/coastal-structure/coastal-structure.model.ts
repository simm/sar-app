import {StoreObject} from '@apollo/client';
import {
  EntityClass,
  BaseReferential,
  EntityFilter,
  Moment,
  fromDateISOString,
  EntityAsObjectOptions,
  Entity,
  splitByProperty,
  isNilOrBlank,
  toDateISOString,
  IStatus, StatusById, isNotNil
} from '@sumaris-net/ngx-components';
import {BehaviorSubject} from 'rxjs';
import {SparqlUtils} from '@app/core/sparql/sparql.utils';
import {TranslateService} from '@ngx-translate/core';

export enum CoastalStructureLevelEnum {
  CATEGORY = 1,
  CLASS = 2,
  TYPE = 3,
}

export type CoastalStructureTypeFields = keyof CoastalStructureType;

@EntityClass({typename: 'CoastalStructureLevelVO'})
export class CoastalStructureLevel extends BaseReferential<CoastalStructureLevel> {
  static ENTITY_NAME = 'CoastalStructureLevel';

  code: string;
  name: string;
  // validityStatus: MilieuMarinFranceValidityStatus = null;
  creationDate: Moment = null;
  updateDate: Moment = null;

  static fromObject: (source: any, opts?: any) => CoastalStructureLevel;

  constructor() {
    super(CoastalStructureLevel.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    Object.assign(this, source);
    this.creationDate = fromDateISOString(this.creationDate);
    if (isNilOrBlank(this.updateDate) && !isNilOrBlank(this.creationDate)) this.updateDate = this.creationDate.clone();
  }
  asObject(opts?: EntityAsObjectOptions): StoreObject {
    const target = super.asObject(opts);
    target.creationDate = this.creationDate && toDateISOString(this.creationDate);
    // target.validityStatus = Object.assign({}, this.validityStatus);
    return target;
  }
}

@EntityClass({typename: 'CoastalStructureTypeVO'})
export class CoastalStructureType extends Entity<CoastalStructureType, string> {
  static ENTITY_NAME = 'CoastalStructureType';

  static fieldsForExports: CoastalStructureTypeFields[] = [
    'code',
    'name',
    'description',
    'parent',
    // 'validityStatus',
    'status',
    'creationDate',
    'updateDate',
  ]

  code: string = null;
  name: string = null;
  description: string = null;
  level: CoastalStructureLevel = null;
  parent: CoastalStructureType = null;
  // validityStatus: MilieuMarinFranceValidityStatus = null;
  status: IStatus = null;
  childrens = new BehaviorSubject<CoastalStructureType[]>([]);
  creationDate: Moment = null;
  updateDate: Moment = null;

  static fromObject: (source: any, opts?: any) => CoastalStructureType;

  static convertDataToCsvExport(datas: CoastalStructureType[], translate: TranslateService):{ headers: string[], values: any[] } {
    const expotsFields = (CoastalStructureType.fieldsForExports as any[]);
    const headers = expotsFields.reduce((acc, item) => {
      switch (item) {
        case('parent'):
          return acc.concat(['parentCode', 'parentName']);
          break;
        default:
          return acc.concat([item])
      }
    }, []);
    const values = datas.map(d => {
      return expotsFields.reduce((acc, item) => {
        switch (item) {
          case('parent'):
            acc['parentCode'] = isNilOrBlank(d.parent?.code) ? '' : d.parent.code;
            acc['parentName'] = isNilOrBlank(d.parent?.name) ? '' : d.parent.name;
            break;
          case('status'):
            acc['status'] = translate.instant('COASTAL_STRUCTURE_TYPE.PAGE.'+d[item].label);
            break;
          // case('validityStatus'):
          //   acc['validityStatus'] = translate.instant(d.validityStatus.label);
          //   break;
          default:
            acc[item] = d[item];
        }
        return acc;
      }, {});
    });
    return {headers, values};
  }

  constructor() {
    super(CoastalStructureType.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    SparqlUtils.jsonTree(source);
    super.fromObject(source, opts);
    Object.assign(this, source);
    this.creationDate = fromDateISOString(this.creationDate);
    if (isNilOrBlank(this.updateDate)) this.updateDate = this.creationDate.clone();
    this.level = CoastalStructureLevel.fromObject(source.level);
    if (!this.parent || !this.parent.code || (this.parent.level.code === this.level.code)) delete this.parent;
    else this.parent = CoastalStructureType.fromObject(this.parent);
    // this.validityStatus = MilieuMarinFranceValidityStatusList.find((item) => item.uri == source.validityStatus);
    this.status = StatusById[source.statusId];
  }

  asObject(opts?: EntityAsObjectOptions): StoreObject {
    const target = super.asObject(opts);
    target.creationDate = this.creationDate && toDateISOString(this.creationDate);
    target.parent = this.parent && this.parent.asObject(opts);
    target.level = this.level.asObject();
    // target.validityStatus = Object.assign({}, this.validityStatus);
    if (isNotNil(target.status)) {
      delete target.status['icon'];
    };
    if (this.childrens.value.length > 0) {
      let childrens = this.childrens.value;
      target.childrens = childrens.map((child => child.asObject(opts)));
    } else {
      target.childrens = [];
    }
    return target;
  }
}

@EntityClass({typename: 'CoastalStructureTypeFilterVO'})
export class CoastalStructureTypeFilter
  extends EntityFilter<CoastalStructureTypeFilter, CoastalStructureType, number> {
  searchText: string;
  id: number;
  code: string;
  categoryCode: number;
  classCode: number;
  validityStatusID: number;
  levelCode: keyof CoastalStructureLevelEnum;

  static fromObject: (source: any, opts?: any) => CoastalStructureTypeFilter;

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.searchText = source.searchText;
    this.code = source.code;
    this.classCode = source.classCode;
    this.categoryCode = source.categoryCode
    this.validityStatusID = source.validityStatusID;
    this.levelCode = source.levelCode;
  }

}
