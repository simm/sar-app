import { NgModule } from '@angular/core';
import {RouterModule, Routes, UrlSegment} from '@angular/router';
import { SharedRoutingModule } from '@sumaris-net/ngx-components';
import { CoastalStructureTypePage } from './coastal-structure-type.page';
import { CoastalStructureModule } from './coastal-structure.module';
import { CoastalStructureTypeSearchView } from './type/search/search-view.component';

const urlMatchID = (url) => {
  return url.length === 1 && url[url.length-1].path.match(/\d$/) ? ({
    consumed: url,
    posParams: {
      id: new UrlSegment(url[url.length-1].path, {})
    },
  }) : null;
};

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'search'
  },
  {
    path: 'search',
    component: CoastalStructureTypeSearchView,
  },
  {
    matcher: urlMatchID,
    component: CoastalStructureTypePage,
  },
  {
    path: 'embed',
    data: {
      menu: false,
      showToolbar: false
    },
    children: [
      {
        path: '',
        redirectTo: 'search',
      },
      {
        path: 'search',
        component: CoastalStructureTypeSearchView
      },
      {
        matcher: urlMatchID,
        component: CoastalStructureTypePage,
      },
    ],
  }
];

@NgModule({
  imports: [
    SharedRoutingModule,
    CoastalStructureModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class CoastalStructureRoutingModule { }
