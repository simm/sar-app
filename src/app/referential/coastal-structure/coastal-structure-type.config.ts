import { FormFieldDefinition } from "@sumaris-net/ngx-components";

export const COASTAL_STRUCTURE_TYPE_CONFIG_OPTION = {
  COASTAL_STRUCTURE_TYPE_HELP_URL: <FormFieldDefinition> {
    key: 'coastalStructureType.help.url',
    label: 'COASTAL_STRUCTURE_TYPE.OPTIONS.HELP_URL',
    type: 'string',
    defaultValue: 'https://sar.milieumarinfrance.fr/content/download/7737/file/Tutoriel_outil_ouvrages.pdf',
  },
  COASTAL_STRUCTURE_TYPE_NEW_DATA_URL: <FormFieldDefinition> {
    key: 'coastalStructureType.newData.url',
    label: 'COASTAL_STRUCTURE_TYPE.OPTIONS.NEW_DATA_URL',
    type: 'string',
    defaultValue: 'https://sar.milieumarinfrance.fr/sendform/contact',
  },
}
