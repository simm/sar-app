import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CoastalStructureTypePage } from './coastal-structure-type.page';

describe('CoastalStructureTypePage', () => {
  let component: CoastalStructureTypePage;
  let fixture: ComponentFixture<CoastalStructureTypePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CoastalStructureTypePage ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CoastalStructureTypePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
