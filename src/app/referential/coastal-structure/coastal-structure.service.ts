import {Inject, Injectable} from '@angular/core';
import {SortDirection} from '@angular/material/sort';
import {BaseEntitySparqlService} from '@app/core/sparql/base-entity-sparql.service';
import {SparqlQueryDef, SparqlResultFormatItem} from '@app/core/sparql/sparql.model';
import {SparqlService, SparqlServiceWatchOptions} from '@app/core/sparql/sparql.service';
import {SparqlUtils} from '@app/core/sparql/sparql.utils';
import {
  ConfigService,
  Configuration,
  EntitiesServiceWatchOptions,
  ENVIRONMENT,
  Environment,
  FilesUtils,
  firstNotNilPromise,
  isNilOrBlank,
  isNotEmptyArray,
  isNotNilOrBlank,
  IStatus,
  LoadResult,
  StatusById
} from '@sumaris-net/ngx-components';
import {BehaviorSubject, combineLatest, from, Observable} from 'rxjs';
import {map, switchMap} from 'rxjs/operators';
import {CoastalStructureLevelEnum, CoastalStructureType, CoastalStructureTypeFilter} from './coastal-structure.model';
import {REFERENTIAL_CONFIG_OPTIONS} from '@app/referential/services/config/referential.config';

const FILTER_MAP = {
  // Regex filter, on a property
  exactMatch: '?{{searchField}}="{{q}}"',
  prefixMatch: 'regex( ?{{searchField}}, "^{{q}}", "i" )',
  anyMatch: 'regex( ?{{searchField}}, "{{q}}", "i" )',

  // Regex filter, on uri (label)
  codeExactMatch: '?id="{{code}}"',
  codePrefixMatch: 'regex( str(?id), "/{{code}}", "i" )',

  parentMatch: '?parent=csttd:{{parentCode}}',
  levelMatch: '?level=cstld:{{levelCode}}',
  validityStatusExactMatch: '?statusId = "{{status}}"',
};

const Queries = {
  load: <SparqlQueryDef>{
    prefixes: ['cst', 'cstt', 'cstl', 'shr', 'sts'],
    query: `
      SELECT DISTINCT
        ?id
        ?code
        ?name
        ?description
        ?level_id
        ?level_code
        ?level_name
        ?level_creationDate
        ?level_updateDate
        ?validityStatus
        ?statusId
        ?creationDate
        ?updateDate
        ?parent_id
        ?parent_code
        ?parent_name
        ?parent_description
        ?parent_creationDate
        ?parent_updateDate
        ?parent_level_id
        ?parent_level_code
        ?parent_level_name
        ?parent_level_creationDate
        ?parent_level_updateDate
        ?parent_validityStatus
        ?parent_parent_id
        ?parent_parent_code
        ?parent_parent_name
        ?parent_parent_description
        ?parent_parent_creationDate
        ?parent_parent_updateDate
        ?parent_parent_level_id
        ?parent_parent_level_code
        ?parent_parent_level_name
        ?parent_parent_level_creationDate
        ?parent_parent_level_updateDate
        ?parent_parent_validityStatus
      WHERE {
        ?sub a cst:CoastalStructureType;
          cstt:id ?id;
          cstt:label ?code;
          cstt:name ?name;
          cstt:status ?validityStatus;
          cstt:creationDate ?creationDate;
          cstt:updateDate ?updateDate .
          OPTIONAL{ ?sub cstt:description ?description } .
          ?sub cstt:status ?status .
          ?status a shr:Status;
              sts:id ?statusId .
          ?sub cstt:level ?level .
          ?level a cst:CoastalStructureLevel;
            cstl:id ?level_id;
            cstl:label ?level_code;
            cstl:name ?level_name;
            cstl:creationDate ?level_creationDate;
            cstl:updateDate ?level_updateDate .
          OPTIONAL {
            ?sub cstt:parent ?parent .
            ?parent a cst:CoastalStructureType;
              cstt:id ?parent_id;
              cstt:label ?parent_code;
              cstt:name ?parent_name ;
              cstt:level ?parent_level;
              cstt:status ?parent_validityStatus;
              cstt:creationDate ?parent_creationDate;
              cstt:updateDate ?parent_updateDate;
              cstt:level ?parent_level .
              OPTIONAL{ ?parent cstt:description ?parent_description } .
              ?parent_level a cst:CoastalStructureLevel;
                cstl:id ?parent_level_id;
                cstl:label ?parent_level_code;
                cstl:name ?parent_level_name;
                cstl:creationDate ?parent_level_creationDate;
                cstl:updateDate ?parent_level_updateDate .
              OPTIONAL {
                ?parent cstt:parent ?parent_parent .
                ?parent_parent a cst:CoastalStructureType;
                  cstt:id ?parent_parent_id;
                  cstt:label ?parent_parent_code;
                  cstt:name ?parent_parent_name ;
                  cstt:level ?parent_parent_level;
                  cstt:status ?parent_parent_validityStatus;
                  cstt:creationDate ?parent_parent_creationDate;
                  cstt:updateDate ?parent_parent_updateDate;
                  cstt:level ?parent_parent_level .
                  ?parent_parent_level a cst:CoastalStructureLevel;
                    cstl:id ?parent_parent_level_id;
                    cstl:label ?parent_parent_level_code;
                    cstl:name ?parent_parent_level_name;
                    cstl:creationDate ?parent_parent_level_creationDate;
                    cstl:updateDate ?parent_parent_level_updateDate .
                  OPTIONAL {
                    ?parent_parent cstt:description ?parent_parent_description;
                  }
              }
          }
        FILTER (( ?code = "{{code}}" )) .
      }
      LIMIT 1
    `,
  },
  loadAll: <SparqlQueryDef>{
    prefixes: ['xsd', 'cst', 'cstt', 'cstl', 'csttd', 'shr', 'sts'],
    query: `
      SELECT DISTINCT
        ?id
        ?code
        ?name
        ?description
        ?level_id
        ?level_code
        ?level_name
        ?level_creationDate
        ?level_updateDate
        ?validityStatus
        ?creationDate
        ?updateDate
        ?parent_name
        ?parent_code
        ?parent_level_code
        ?parent_creationDate
        ?parent_updateDate
        ?statusId
      WHERE {
        ?sub a cst:CoastalStructureType;
          cstt:id ?id;
          cstt:label ?code;
          cstt:name ?name;
          cstt:level ?level;
          cstt:status ?validityStatus;
          cstt:creationDate ?creationDate;
          cstt:updateDate ?updateDate .
          OPTIONAL{ ?sub cstt:description ?description } .
          {{sortByNameField}}
          ?sub cstt:status ?status .
          ?status a shr:Status;
              sts:id ?statusId .
          ?sub cstt:level ?level .
          ?level a cst:CoastalStructureLevel;
            cstl:id ?level_id;
            cstl:label ?level_code;
            cstl:name ?level_name;
            cstl:creationDate ?level_creationDate;
            cstl:updateDate ?level_updateDate .
          OPTIONAL {
            ?sub cstt:parent ?parent .
            ?parent a cst:CoastalStructureType;
              cstt:label ?parent_code;
              cstt:name ?parent_name;
              cstt:level ?parent_level;
              cstt:creationDate ?parent_creationDate;
              cstt:updateDate ?parent_updateDate;
              cstt:level ?parent_level .
              ?parent_level a cst:CoastalStructureLevel;
                cstl:label ?parent_level_code;
          }
        FILTER (
          ( {{filter}} )
        ) .
      }
      ORDER BY {{sortDirection}}({{sortBy}})
      OFFSET {{offset}}
      LIMIT {{size}}
    `,
  },
  loadAllLight: <SparqlQueryDef>{ // like loadAll but without parent
    prefixes: ['xsd', 'cst', 'cstt', 'cstl', 'csttd', 'cstld', 'shr', 'sts'],
    query: `
      SELECT DISTINCT
        ?id
        ?code
        ?name
        ?description
        ?parent
        ?creationDate
        ?updateDate
        ?validityStatus
        ?level_code
        ?level_name
        ?statusId
      WHERE {
        ?sub a cst:CoastalStructureType;
        cstt:id ?id;
        cstt:label ?code;
        cstt:name ?name;
        cstt:status ?validityStatus;
        cstt:creationDate ?creationDate;
        cstt:updateDate ?updateDate .
        OPTIONAL{ ?sub cstt:description ?description } .
        FILTER( {{filter}} )
        ?sub cstt:level ?level .
        ?level a cst:CoastalStructureLevel;
          cstl:id ?level_id;
          cstl:label ?level_code;
          cstl:name ?level_name;
          cstl:creationDate ?level_creationDate;
          cstl:updateDate ?level_updateDate .
        ?sub cstt:status ?status .
        ?status a shr:Status;
            sts:id ?statusId .
        FILTER( {{filter}} )
      }
      ORDER BY {{sortDirection}}(({{sortBy}}))
      OFFSET {{offset}}
      LIMIT {{size}}
    `,
  },
  loadByLevel: {
    prefixes: ['cst', 'cstt', 'cstl', 'csttd', 'shr', 'sts'],
    query: `
      SELECT DISTINCT
        ?id
        ?code
        ?name
        ?description
        ?parent
        ?creationDate
        ?updateDate
        ?validityStatus
        ?level_code
        ?level_name
        ?statusId
      WHERE {
        ?sub a cst:CoastalStructureType;
        cstt:id ?id;
        cstt:label ?code;
        cstt:name ?name;
        cstt:status ?validityStatus;
        cstt:creationDate ?creationDate;
        cstt:updateDate ?updateDate .
        OPTIONAL { ?sub cstt:description ?description } .
        OPTIONAL { ?sub cstt:parent ?parent } .
        ?sub cstt:level ?level .
        ?level a cst:CoastalStructureLevel;
          cstl:id ?level_id;
          cstl:label ?level_code;
          cstl:name ?level_name;
          cstl:creationDate ?level_creationDate;
          cstl:updateDate ?level_updateDate .
        ?sub cstt:status ?status .
        ?status a shr:Status;
            sts:id ?statusId .
        FILTER({{filter}})
      }
    `,
  },
  loadWebSemantic: {
    prefixes: ['cst', 'cstt', 'cstl', 'csttd'],
    query: `
      DESCRIBE ?sub
      WHERE {
        ?sub a cst:CoastalStructureType;
        cstt:id ?id;
        cstt:label ?code;
        cstt:name ?name;
        cstt:status ?validityStatus;
        cstt:creationDate ?creationDate;
        cstt:updateDate ?updateDate .
        OPTIONAL {
          ?sub cstt:description ?description .
          ?sub cstt:parent ?parent
        } .
        ?sub cstt:level ?level .
        ?level a cst:CoastalStructureLevel;
          cstl:id ?level_id;
          cstl:label ?level_code;
          cstl:name ?level_name;
          cstl:creationDate ?level_creationDate;
          cstl:updateDate ?level_updateDate .
        FILTER( {{filter}} )
      }
    `,
  },
  loadStatus: {
    prefixes: ['shr', 'sts'],
    query: `
      SELECT DISTINCT ?id ?label WHERE {
        ?sub a shr:Status;
        sts:id ?id;
        sts:label ?label .
      }
    `,
  },
  // Generate later
  countAll: null
};
Queries.countAll = SparqlUtils.toCountQuery(Queries.loadAll, {fieldName: 'id'});

@Injectable({providedIn: 'root'})
export class CoastalStructureTypeService
  extends BaseEntitySparqlService<CoastalStructureType, CoastalStructureTypeFilter, string> {

  protected readonly _logPrefix = '[coastal-structure-type-service]';

  readonly nbOfLinesToFetchWenGetAllWithPaginate = 500;

  constructor(
    protected sparql: SparqlService,
    protected configService: ConfigService,
    @Inject(ENVIRONMENT) protected environment: Environment
  ) {
    super(sparql, CoastalStructureType, CoastalStructureTypeFilter, environment);
    if (this._debug) console.debug(`${this._logPrefix} : constructor`);
  }

  protected async ngOnStart(): Promise<any> {
    if (this._debug) console.debug(`${this._logPrefix} : ngOnStart`);
    const config = await firstNotNilPromise(this.configService.config);
    this.registerAdditionalSparqlSchema(config);
    // In cas of the user update its config after this service was loaded
    this.registerSubscription(this.configService.config.subscribe((config) => {
      this.registerAdditionalSparqlSchema(config);
    }));
    return super.ngOnStart();
  }


  watchAll(
    offset: number,
    size: number,
    sortBy?: string,
    sortDirection?: SortDirection,
    filter?: Partial<CoastalStructureTypeFilter>,
    options?: SparqlServiceWatchOptions,
  ): Observable<LoadResult<CoastalStructureType>> {
    if (this._debug) console.debug(`${this._logPrefix} : watchAll`, {offset, size, sortBy, sortDirection, filter, options});

    if (!this.started) {
      console.info(`${this._logPrefix} watchAll : Waiting while service not ready...`);
      return from(this.ready())
        .pipe(
          switchMap(() => this.watchAll(offset, size, sortBy, sortDirection, filter, options))
        );
    }

    const now = Date.now();
    console.debug(this._logPrefix + `Loading coastal structure type...`);

    let sortByNameField  = '';
    if (sortBy === 'code') {
      sortBy = 'xsd:integer(?id)';
    } else if (isNotNilOrBlank(sortBy) || sortBy === 'name') {
      sortBy = '?sortName';
      sortByNameField = 'BIND( REPLACE(REPLACE(?name,"[ÉÈÊ]","E"), "[éèê]", "e") as ?sortName) .';
    } else {
      sortBy = `?${sortBy}`;
    }

    const variables = {
      offset,
      size,
      sortBy,
      sortDirection: sortDirection && sortDirection.toUpperCase() || 'ASC',
      sortByNameField,
      filter: this.bindFilter(filter)
    };
    const result = combineLatest([
      // get data
      this.sparql.watch<any>({
        query: (!options || !options?.lightLoad) ? Queries.loadAll : Queries.loadAllLight,
        variables
      }),
      (!options || options?.withTotal !== false) ?
        // get count
        this.sparql.watch<any>({
          query: Queries.countAll,
          variables
        }) :
        new BehaviorSubject([])
    ]);
    return result.pipe(
      map(([res1, res2]) => {
        const entities = (!options || options.toEntity !== false)
          ? (res1 || []).map(this.fromObject)
          : (res1 || []) as CoastalStructureType[];
        const total = res2?.length ? res2[0].count : entities.length;
        console.debug(this._logPrefix + `${entities.length} coastal structure type loaded in ${Date.now() - now}ms`);
        return {
          data: entities,
          total
        };
      })
    )
  }

  watchCategories(): Observable<LoadResult<CoastalStructureType>> {
    if (!this.started) {
      console.info(`${this._logPrefix} watchCategory : Waiting while service not ready...`);
      return from(this.ready()).pipe(switchMap(() => this.watchCategories()));
    }
    const filter = CoastalStructureTypeFilter.fromObject({
      levelCode: CoastalStructureLevelEnum.CATEGORY,
    });
    console.debug(this._logPrefix + `Loading coastal categories list...`);
    return this.watchAll(
      0,
      9999,
      'code',
      'asc',
      filter,
      {withTotal: false, lightLoad: true});
  }

  watchClasses(): Observable<LoadResult<CoastalStructureType>> {
    if (!this.started) {
      console.info(`${this._logPrefix} watchClasses : Waiting while service not ready...`);
      return from(this.ready()).pipe(switchMap(() => this.watchClasses()));
    }
    const filter = CoastalStructureTypeFilter.fromObject({
      levelCode: CoastalStructureLevelEnum.CLASS,
    });
    console.debug(this._logPrefix + `Loading coastal classes list...`);
    return this.watchAll(
      0,
      9999,
      'code',
      'asc',
      filter,
      {withTotal: false, lightLoad: true});
  }

  watchStatusList(): Observable<LoadResult<IStatus>> {
    if (!this.started) {
      console.info(`${this._logPrefix} watchStatus : Waiting while service not ready...`);
      return from(this.ready()).pipe(switchMap(() => this.watchStatusList()));
    }
    const now = Date.now();
    console.debug(this._logPrefix + `Loading coastal status list...`);
    const result = combineLatest([
        this.sparql.watch<{ id: string, label: string }>({
          query: Queries.loadStatus,
          variables: {},
        }),
        new BehaviorSubject([]),
      ]);
    return result.pipe(
      map(([res1, _]) => {
        console.debug(this._logPrefix + `${res1.length} coastal structure type loaded in ${Date.now() - now}ms`);
        const statusList = res1.map((item) => StatusById[parseInt(item.id)]);
        return {
          data: statusList,
          total: statusList.length,
        };
      })
    )
  }

  watchLevel(parentId: string = '', options?: EntitiesServiceWatchOptions): Observable<CoastalStructureType[]> {
    if (!this.started) {
      console.info(`${this._logPrefix} watchLevel : Waiting while service not ready...`);
      return from(this.ready())
        .pipe(
          switchMap(() => this.watchLevel(parentId, options))
        );
    }
    let filter;
    if (isNotNilOrBlank(parentId)) filter = `(?parent = csttd:${parentId})`;
    else filter = 'NOT EXISTS {?sub cstt:parent ?parent}';
    if (this._debug) console.debug(`${this._logPrefix} : watchLevel`);
    return this.sparql.watch<any>({
      query: Queries.loadByLevel,
      variables: {filter},
    }).pipe(
      map((res) => {
          return (!options || options.toEntity !== false)
            ? (res || []).map(this.fromObject)
            : (res || []) as CoastalStructureType[];
        }
      )
    );
  }

  fromObject(source: any): CoastalStructureType {
    if (!source) return undefined;
    return CoastalStructureType.fromObject(source);
  }

  protected bindFilter(filter: Partial<CoastalStructureTypeFilter>, opts = {exactMatch: false}): string {
    if (this._debug) console.debug(`${this._logPrefix} : bindFilter`, {filter, opts});
    filter = this.asFilter(filter) || new CoastalStructureTypeFilter();

    const filterKeys = ['anyMatch'];
    const variables: Record<string, any> = {searchField: <keyof CoastalStructureType>'name'};

    const searchTerms = SparqlUtils.getSearchTerms(filter.searchText);

    // Code
    let code = filter.code?.trim();
    // When no code, try to parse it input search, to find a code
    if (isNilOrBlank(filter.code) && isNotEmptyArray(searchTerms)) {
      const codeIndex = searchTerms.findIndex(s => RegExp(/^\d+$/).test(s));
      if (codeIndex !== -1) {
        code = searchTerms.splice(codeIndex, 1)[0];
      }
    }
    if (isNotNilOrBlank(code)) {
      variables.code = code.trim();
      if (!filterKeys.includes('codeExactMatch')) filterKeys.push('codeExactMatch');
    }

    // Parent
    if (isNotNilOrBlank(filter.classCode)) variables.parentCode = filter.classCode;
    else if (isNotNilOrBlank(filter.categoryCode)) variables.parentCode = filter.categoryCode;
    if (isNotNilOrBlank(variables.parentCode) && !filterKeys.includes('parentMatch'))
      filterKeys.push('parentMatch');

    // NOTE : For now the status is used a validityStatus : is a temporaray hack
    if (isNotNilOrBlank(filter.validityStatusID)) {
      variables.status = filter.validityStatusID;
      if (!filterKeys.includes('validityStatusExactMatch')) filterKeys.push('validityStatusExactMatch');
    }

    // Level
    if (isNotNilOrBlank(filter.levelCode)) {
      variables.levelCode = filter.levelCode;
      if (!filterKeys.includes('levelMatch')) filterKeys.push('levelMatch');
    }

    const filters = filterKeys
      .map(key => {
        // If exactMatch, replace 'prefixMatch' with 'exactMatch'
        if (opts.exactMatch) {
          if (key === 'prefixMatch' || key === 'anyMatch') return 'exactMatch';
          if (key === 'codePrefixMatch') return 'codeExactMatch';
        }
        return key;
      })
      .map(key => FILTER_MAP[key])
      .filter(isNotNilOrBlank);

    variables.q = searchTerms;
    return SparqlUtils.bindFilters(filters, variables, {exactMatch: opts?.exactMatch});
  }

  async load(code: number): Promise<CoastalStructureType> {
    if (this._debug) console.debug(`${this._logPrefix} : load`, {code});
    await this.sparql.ready();
    const result = await this.sparql.watch<any>({
      query: Queries.load,
      variables: {code},
    }).toPromise();
    if (result.length === 0) throw new Error('ERROR.DATA_NOT_FOUND');
    return CoastalStructureType.fromObject(result[0]);
  }

  async loadAllByPage(
    sortBy?: string,
    sortDirection?: SortDirection,
    filter?: Partial<CoastalStructureTypeFilter>,
    options?: SparqlServiceWatchOptions,
  ): Promise<LoadResult<CoastalStructureType>> {
    if (this._debug) console.debug(`${this._logPrefix} : watchByPages`, {sortBy, sortDirection, filter, options});
    const result = await firstNotNilPromise(this.watchAll(0, this.nbOfLinesToFetchWenGetAllWithPaginate, sortBy, sortDirection, filter, options));
    while (result.data.length < result.total) {
      const nextResult = await firstNotNilPromise(this.watchAll(result.data.length-1, this.nbOfLinesToFetchWenGetAllWithPaginate, sortBy, sortDirection, filter, options));
      result.data = result.data.concat(nextResult.data);
    }
    return  result;
  }


  async downloadRdfData(
    filename: string,
    format: SparqlResultFormatItem,
    filter: CoastalStructureTypeFilter,
  ) {
    if (this._debug) console.debug(`${this._logPrefix} : downloadRdfData`, {filename, format, filter});
    await this.sparql.ready();
    const data = await this.sparql.sendRequest(
      {
        query: Queries.loadWebSemantic,
        variables: {
          filter: this.bindFilter(filter)
        },
      },
      {},
      format,
    ).toPromise()
    let result = null;
    if (format.exportFileType === 'application/json') {
      result = JSON.stringify(data, null, 2);
    } else {
      result = data;
    }
    FilesUtils.writeTextToFile(result, {
      filename: filename,
      encoding: format.exportFileEncoding,
      type: format.responseType,
    });
  }

  private registerAdditionalSparqlSchema(config: Configuration) {
    if (this._debug) console.debug(`${this._logPrefix} : registerAdditionalSparqlSchema`, {config});
    const baseUri = config.getProperty(REFERENTIAL_CONFIG_OPTIONS.RDF_MODEL_BASE_URI)
      .replace(/\/$/, '');

    this.sparql.registerSchemas([
      {
        name: 'CoastalStructure',
        prefix: 'cst',
        namespace: `${baseUri}/schema/cst/0.1/`,
      },
      {
        name: 'CoastalStructureType',
        prefix: 'cstt',
        namespace: `${baseUri}/schema/cst/0.1/CoastalStructureType#`,
      },
      {
        name: 'CoastalStructureLevel',
        prefix: 'cstl',
        namespace: `${baseUri}/schema/cst/0.1/CoastalStructureLevel#`,
      },
      {
        name: 'CoastalStructureTypeData',
        prefix: 'csttd',
        namespace: `${baseUri}/data/cst/CoastalStructureType/`
      },
      {
        name: 'CoastalStructureLevelData',
        prefix: 'cstld',
        namespace: `${baseUri}/data/cst/CoastalStructureLevel/`,
      },
      {
        name: 'Schema',
        prefix: 'shr',
        namespace: `${baseUri}/schema/shr/0.1/`,
      },
      {
        name: 'StatusData',
        prefix: 'sts',
        namespace: `${baseUri}/schema/shr/0.1/Status#`,
      },
    ], {overideSchema: true});
  }

}
