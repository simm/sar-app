import { NgModule } from '@angular/core';

import { AppCoreModule } from '@app/core/core.module';
import { TranslateModule } from '@ngx-translate/core';
import { CoastalStructureTypePage } from './coastal-structure-type.page';
import { BtnMenuActionModule } from '@app/shared/menu/btn-menu-action/btn-menu-action.module';
import { MatTreeModule } from '@angular/material/tree';
import { CoastalStructureTypeSearchView } from './type/search/search-view.component';
import { CoastalStructureTypeTable } from './type/search/table/table.component';
import { CoastalStructureTypeTree } from './type/search/tree/tree.component';
import { MatTabsModule } from '@angular/material/tabs';
import { MiscUtilsModule } from '@app/shared/misc-utils/misc-utils.module';
import { ExportsUtilsModule } from '@app/shared/popover/exports/exports-popover.module';
import { StarRatingModule } from '@app/shared/star-rating/star-rating.module';


@NgModule({
  imports: [
    AppCoreModule,
    TranslateModule.forChild(),
    MatTreeModule,
    MatTabsModule,
    BtnMenuActionModule,
    MiscUtilsModule,
    ExportsUtilsModule,
    StarRatingModule,
  ],
  exports: [
    TranslateModule,
    CoastalStructureTypeTable,
    CoastalStructureTypePage,
    CoastalStructureTypeTree,
    CoastalStructureTypeSearchView,
  ],
  declarations: [
    CoastalStructureTypeTable,
    CoastalStructureTypeTree,
    CoastalStructureTypePage,
    CoastalStructureTypeSearchView,
  ],
})
export class CoastalStructureModule {
  constructor() {
    console.debug('[coastal-structure-module] Creating module');
  }
}
