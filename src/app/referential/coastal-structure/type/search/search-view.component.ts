import {Component, ElementRef, Inject, Injector, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Popovers} from '@app/shared/popover/popover.utils';
import {IonContent, PopoverController} from '@ionic/angular';
import {TranslateService} from '@ngx-translate/core';
import {ConfigService, ENVIRONMENT, LocalSettingsService, MenuService, PlatformService, toBoolean, waitFor} from '@sumaris-net/ngx-components';
import {interval, Subscription} from 'rxjs';
import {COASTAL_STRUCTURE_TYPE_CONFIG_OPTION} from '../../coastal-structure-type.config';
import {CoastalStructureTypeService} from '../../coastal-structure.service';

type AvailableView = 'table' | 'tree';

@Component({
  selector: 'coastal-structure-type-search',
  templateUrl: './search-view.component.html',
  styleUrls: ['./search-view.component.scss'],
})
export class CoastalStructureTypeSearchView implements OnInit, OnDestroy {

  static pageSettingsId = 'coastal-structure-type-search-view';
  static pageSettingDefaultView = 'default_view';

  private readonly _logPrefix = '[coastal-structure-type-search-view]';

  protected route: ActivatedRoute;
  protected router: Router;
  protected settings: LocalSettingsService;
  protected translate: TranslateService;
  protected menu: MenuService;
  protected platform: PlatformService;
  protected configService: ConfigService;
  protected popoverController: PopoverController;
  protected coastalStructureTypeService: CoastalStructureTypeService;
  protected $subscription = new Subscription();
  protected debug:boolean = false;

  readonly i18nPrefix = 'COASTAL_STRUCTURE_TYPE.SEARCH.';
  title = this.i18nPrefix + 'TITLE';
  descritpion = this.i18nPrefix + 'DESCRIPTION';
  helpUrl: string;
  newDataUrl: string;
  indexOfSelectedTab: number = 0;
  ionContentHeight = 0;
  selectedView: AvailableView;
  loadTableView = false;
  loadTreeView = false;
  showFooter: boolean;

  @Input() showToolbar = true;

  @ViewChild(IonContent, { read: ElementRef }) ionContentElementRef;

  constructor(
    protected injector: Injector,
    private _element: ElementRef,
    @Inject(ENVIRONMENT) environment,
  ) {
    this.debug = !environment.production;
    if (this.debug) console.debug(`${this._logPrefix} : constructor`);
    this.route = this.injector.get(ActivatedRoute);
    this.router = this.injector.get(Router);
    this.settings = this.injector.get(LocalSettingsService);
    this.translate = this.injector.get(TranslateService);
    this.menu = this.injector.get(MenuService);
    this.platform = this.injector.get(PlatformService);
    this.configService = this.injector.get(ConfigService);
    this.coastalStructureTypeService = this.injector.get(CoastalStructureTypeService);
    this.popoverController = this.injector.get(PopoverController);
  }

  ngOnInit(): void {
    if (this.debug) console.debug(`${this._logPrefix} : ngOnInit`);
    this.readRouteData();
    // TODO : Get this param from readRouteData ?
    this.showFooter = !this.platform.mobile;
    this.initSubscription();
    this.selectDefaultView();
  }

  ngOnDestroy(): void {
    if (this.debug) console.debug(`${this._logPrefix} : ngOnDestroy`);
    this.$subscription.unsubscribe();
  }

  switchView(view: AvailableView): void {
    if (this.debug) console.debug(`${this._logPrefix} : switchView`, { view });
    this.selectedView = view;
    if (view == 'tree') {
      this.loadTreeView = true;
      this.descritpion = this.i18nPrefix + 'DESCRIPTION_TREE';
    } else if (view == 'table') {
      this.loadTableView = true;
      this.descritpion = this.i18nPrefix + 'DESCRIPTION_TABLE';
    }
    this.router.navigate(['.'], {
      relativeTo: this.route,
      queryParams: {...this.route.snapshot.queryParamMap['params'], view},
      replaceUrl: true,
    });
  }

  protected readRouteData(): void {
    if (this.debug) console.debug(`${this._logPrefix} : readRouteData`);
    const routeData = this.route.snapshot.data;
    if (routeData) {
      const showMenu = toBoolean(routeData.menu, true);
      this.menu.enable(showMenu);
      this.showToolbar = toBoolean(routeData.showToolbar, showMenu);
    }
  }

  protected selectDefaultView(): void {
    if (this.debug) console.debug(`${this._logPrefix} : selectDefaultView`);
    if (this.route.snapshot.queryParamMap.get('view') == 'tree') this.switchView('tree');
    else this.switchView('table');
  }

  protected initSubscription(): void {
    if (this.debug) console.debug(`${this._logPrefix} : initSubscription`);
    this.$subscription.add(
      this.configService.config.subscribe((sub) => {
        this.helpUrl = sub.getProperty(COASTAL_STRUCTURE_TYPE_CONFIG_OPTION.COASTAL_STRUCTURE_TYPE_HELP_URL);
        this.newDataUrl = sub.getProperty(COASTAL_STRUCTURE_TYPE_CONFIG_OPTION.COASTAL_STRUCTURE_TYPE_NEW_DATA_URL);
      })
    );
    this.$subscription.add(
      interval(250).subscribe((_) => {
        this.ionContentHeight = this.ionContentElementRef.nativeElement.offsetHeight;
      })
    );
  }

  showEmbeddedPopover(event: UIEvent): Promise<any> {
    if (this.debug) console.debug(`${this._logPrefix} : showEmbeddedPopover`, { event });
    const url = window.location.origin
      + this.router.createUrlTree(
        ['..', 'embed', 'tree'],
        { relativeTo: this.route }
      ).toString();
    const title = this.translate.instant(this.title);
    const html = `<iframe src="${url}"`
      + ` title="${title}"`
      + ` width="100%"`
      + ` height="800px"`
      + ` sandbox="allow-same-origin allow-scripts allow-popups allow-forms allow-downloads"`
      + ` frameborder="0"`
      + ` allow="clipboard-write;"`
      + ` </iframe>`;
    return Popovers.showText(
      this.popoverController,
      event,
      {
        editing: false,
        multiline: true,
        text: html,
        placeholder: this.translate.instant(this.i18nPrefix + 'EMBEDDED_IFRAME_HELP'),
        maxLength: 0,
      }
    );
  }

  async showHelp() {
    if (this.debug) console.debug(`${this._logPrefix} : showHelp`);
    if (!this.helpUrl) {
      await waitFor(() => !this.helpUrl)
    }
    this.platform.open(this.helpUrl, '_blank');
  }

}
