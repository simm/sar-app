import {NestedTreeControl} from '@angular/cdk/tree';
import {AfterViewInit, Component, ElementRef, Inject, Injector, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatTree, MatTreeNestedDataSource} from '@angular/material/tree';
import {ActivatedRoute, Router} from '@angular/router';
import {CoastalStructureType, CoastalStructureTypeFilter} from '@app/referential/coastal-structure/coastal-structure.model';
import {CoastalStructureTypeService} from '@app/referential/coastal-structure/coastal-structure.service';
import {IonFooter, IonHeader, PopoverController} from '@ionic/angular';
import {TranslateService} from '@ngx-translate/core';
import {ENVIRONMENT, FilesUtils, isNilOrBlank, isNotNil, LoadResult, PlatformService, PropertyFormatPipe, waitForFalse} from '@sumaris-net/ngx-components';
import {BehaviorSubject, combineLatest, Observable, Subscription} from 'rxjs';
import {filter, first} from 'rxjs/operators';
import {SparqlResultFormat} from '@app/core/sparql/sparql.model';
import {ExportsType} from '@app/shared/popover/exports/exports-popover.model';
import {ExportsPopoverComponent} from '@app/shared/popover/exports/exports-popover.component';
import {exportToCSV} from '@app/shared/entities/entities.utils';

@Component({
  selector: 'coastal-structure-type-tree',
  templateUrl: './tree.component.html',
  styleUrls: ['./tree.component.scss']
})
export class CoastalStructureTypeTree implements OnInit, AfterViewInit, OnDestroy {

  private readonly _logPrefix = '[coastal-structure-type-tree]';
  private readonly _limitAutoUnfoldNodeLevel = 3;

  protected router: Router;
  protected route: ActivatedRoute;
  protected translate: TranslateService;
  protected platform: PlatformService;
  protected dataService: CoastalStructureTypeService;
  protected $loadingSubject = new BehaviorSubject<boolean>(true);
  protected $allNodesExpanded = new BehaviorSubject<boolean>(false);
  protected $allNodesCollapsed = new BehaviorSubject<boolean>(true);
  protected $subscription = new Subscription();
  protected popoverController: PopoverController;
  protected propertyFormat: PropertyFormatPipe;
  protected debug: boolean = false;

  treeControl: NestedTreeControl<CoastalStructureType>;
  dataSource: MatTreeNestedDataSource<CoastalStructureType>;
  availableExportsTypes: ExportsType[];

  readonly i18nPrefix = 'COASTAL_STRUCTURE_TYPE.TREE_VIEW.';
  readonly loadingNode = new Map<string, boolean>();
  readonly nodesShowDetails = new Map<string, boolean>();
  readonly nodesLoaded = new Map<string, boolean>();
  readonly errorSubject = new BehaviorSubject<string>(undefined);
  readonly elementUsedToCalculateHeightOfIonContent: ElementRef[] = [];

  get detailsPageUrl(): string { return this.router.url.split('/').slice(0, -1).join('/') + '/' };
  get loading(): boolean { return this.$loadingSubject.value };
  get loaded(): boolean { return !this.$loadingSubject.value };
  get allNodesExpanded() { return this.$allNodesExpanded.value };
  get allNodesCollapsed() { return this.$allNodesCollapsed.value };
  get mobile() { return this.platform.mobile };

  hasChild = (_: number, node: CoastalStructureType) => {
    if (!this.nodesLoaded.get(node.code)) return (parseInt(node.level.code) < 3);
    else return (node.childrens.value.length > 0);
  };

  @Input() availableHeightForTheComponent = 0;

  @ViewChild(MatTree) matTree: MatTree<CoastalStructureType>;
  // Used to auto calculate available height for the
  @ViewChild(IonFooter, { read: ElementRef }) ionFooterElementRef: ElementRef;
  @ViewChild(IonHeader, { read: ElementRef }) ionHeaderElementRef: ElementRef;

  constructor(
    private injector: Injector,
    @Inject(ENVIRONMENT) environment,
  ) {
    this.debug = !environment.production;
    if (this.debug) console.debug(`${this._logPrefix} : constructor`);

    this.router = this.injector.get(Router);
    this.route = this.injector.get(ActivatedRoute);
    this.translate = this.injector.get(TranslateService);
    this.platform = this.injector.get(PlatformService);
    this.dataService = this.injector.get(CoastalStructureTypeService);

    this.treeControl = new NestedTreeControl<CoastalStructureType>((node: CoastalStructureType) => node.childrens);
    this.dataSource = new MatTreeNestedDataSource();

    this.popoverController = this.injector.get(PopoverController);
    this.propertyFormat = this.injector.get(PropertyFormatPipe);
  }

  ngOnInit(): void {
    if (this.debug) console.debug(`${this._logPrefix} : ngOnInit`);
    this.start();
  }

  ngOnDestroy(): void {
    if (this.debug) console.debug(`${this._logPrefix} : ngOnDestroy`);
    this.$subscription.unsubscribe();
    this.$loadingSubject.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.elementUsedToCalculateHeightOfIonContent.push(this.ionHeaderElementRef);
    this.elementUsedToCalculateHeightOfIonContent.push(this.ionFooterElementRef);
  }

  collapseAll(
    nodes: CoastalStructureType[] = this.dataSource.data,
    limitLevel = this._limitAutoUnfoldNodeLevel
  ): void {
    if (this.debug) console.debug(`${this._logPrefix}: collapseAll`);
    nodes.forEach((node) => {
      if (parseInt(node.level.code) < limitLevel && this.hasChild(0, node)) {
        // Check if node is already loaded
        if (node.childrens.value.length === 0) {
          this.loadNodeChildrens(node);
          node.childrens.pipe(
            filter((childrens) => childrens.length > 0),
            first()
          ).subscribe((childrens) => {
            this.collapseAll(childrens);
          });
        } else {
          this.collapseAll(node.childrens.value);
          this.updateNodesExpandedCollapsedState();
        };
        this.treeControl.expand(node);
      }
    });
  }

  expandAll() {
    if (this.debug) console.debug(`${this._logPrefix} : expandAll`);
    this.treeControl.collapseAll();
    this.$allNodesCollapsed.next(true);
    this.$allNodesExpanded.next(false);
  }

  toggleShowNodeDetails(node: CoastalStructureType) {
    if (this.debug) console.debug(`${this._logPrefix} : toggleShowNodeDetails`);
    if (!this.mobile) return;
    const showDetails = this.nodesShowDetails.get(node.code);
    this.nodesShowDetails.set(node.code, !showDetails);
  }

  openDetailPage(node: CoastalStructureType, event: UIEvent) {
    if (isNilOrBlank(this.detailsPageUrl)) return; // Skip
    event.stopPropagation();
    return this.router.navigateByUrl(this.detailsPageUrl + node.code);
  }

  async start(): Promise<void> {
    if (this.debug) console.debug(`${this._logPrefix} : start`);
    try {
      this.$subscription.add(
        this.dataService.watchLevel().subscribe((items) => {
          if (this.debug) console.debug(`${this._logPrefix} : initialise dataSource`, { items });
          items.forEach((item) => {
            this.loadingNode.set(item.code, false);
            this.nodesShowDetails.set(item.code, !this.mobile);
          });
          this.dataSource.data = items;
        })
      );
      this.loadAvailableExportsFormats();
    } catch (err) {
      if (this.debug) console.debug(err);
      this.setError(err && err.message || err);
    }
    this.markAsLoaded();
  }

  async loadNodeChildrens(node: CoastalStructureType) {
    if (this.debug) console.debug(`${this._logPrefix} : loadNodeChildrens`, { node });
    // Not re fetch childrens if already present
    if (!this.nodesLoaded.get(node.code)) {
      this.loadingNode.set(node.code, true);
      if (this.nodesShowDetails.get(node.code) === undefined) this.nodesShowDetails.set(node.code, !this.platform.mobile);
      try {
        const childrens = await this.dataService.watchLevel(node.code).toPromise();
        if (this.debug) console.debug(`${this._logPrefix} : get nodes childrens`, { childrens });
        childrens.forEach((child) => { if (this.hasChild(0, child)) this.loadingNode.set(node.code, false) });
        node.childrens.next(childrens);
        this.nodesLoaded.set(node.code, true);
      } catch (err) {
        if (this.debug) console.debug(err);
        this.setError(err && err.message || err);
      }
      this.loadingNode.set(node.code, false);
    }
    this.updateNodesExpandedCollapsedState();
  }

  async showExportsPopover(event: UIEvent) {
    if (this.debug) console.debug(`${this._logPrefix} : showExportsPopover`, {event});
    const popover = await this.popoverController.create({
      component: ExportsPopoverComponent,
      event,
      componentProps: {
        defaultFileName: this.translate.instant('COASTAL_STRUCTURE_TYPE.EXPORT_FILE_NAME'),
        exportsTypes: this.availableExportsTypes,
      },
    });
    await popover.present();
  }

  protected updateNodesExpandedCollapsedState() {
    if (this.debug) console.debug(`${this._logPrefix} : updateNodesExpandedCollapsedState`);
    this.$allNodesCollapsed.next(this.checkIfAllNodesAreCollapsed());
    this.$allNodesExpanded.next(this.checkIfAllNodesAreExpanded());
  }

  protected checkIfAllNodesAreExpanded(nodes: CoastalStructureType[] = this.dataSource.data): boolean {
    if (this.debug) console.debug(`${this._logPrefix} : checkIfAllNodesAreExpanded`);
    return nodes.every((node) => {
      if (!this.hasChild(0, node)) return true;
      if (this.treeControl.isExpanded(node)) {
        return this.checkIfAllNodesAreExpanded(node.childrens.value);
      }
      else return false;
    });
  }

  protected checkIfAllNodesAreCollapsed(): boolean {
    if (this.debug) console.debug(`${this._logPrefix} : checkIfAllNodesAreCollapsed`);
    return this.dataSource.data.every((node) => !this.treeControl.isExpanded(node));
  }

  protected markAsLoaded(): void {
    if (this.debug) console.debug(`${this._logPrefix} : markAsLoaded`);
    if (this.$loadingSubject.value) {
      this.$loadingSubject.next(false);
    }
  };

  protected setError(value: string, opts?: { emitEvent?: boolean }) {
    if (this.debug) console.debug(`${this._logPrefix} : setError`, { value, opts })
    if (this.errorSubject.value !== value) {
      this.errorSubject.next(value);
    }
  }

  protected loadAvailableExportsFormats() {
    if (this.debug) console.debug(`${this._logPrefix} : loadAvailableExportsFormats`);
    const filter = new CoastalStructureTypeFilter();
    this.availableExportsTypes = [
      {
        key: 'simple',
        label: 'SIMPLE',
        formats: [
          {
            key: 'csv',
            label: 'CSV',
            action: async (fileName, event, extrasParams) => {
              const datas = (await this.dataService.loadAllByPage('code')).data;
              const result = CoastalStructureType.convertDataToCsvExport(datas, this.translate);
              exportToCSV(
                result.values,
                result.headers,
                'COASTAL_STRUCTURE_TYPE.PAGE.FIELDS.',
                this.translate,
                fileName,
                this.propertyFormat,
                {withTotal: false},
              );
            },
          },
          {
            key: 'json',
            label: 'JSON',
            action: async (fileName, event, extrasParams) => {
              const datas = (await this.dataService.loadAllByPage('code')).data;
              FilesUtils.writeTextToFile(
                JSON.stringify(datas, null, 2),
                {
                  type: 'application/json',
                  filename: fileName,
                }
              );
            },
          },
        ],
      },
      {
        key: 'webSemantic',
        label: 'WEB_SEMANTIC',
        formats: [
          {
            key: 'rdf.json',
            label: 'JSON_RDF',
            action: async (filename, event, extrasParams) => {
              const format = SparqlResultFormat.jsonRdf;
              this.dataService.downloadRdfData(filename, format, filter);
            },
          },
          {
            key: 'rdf.xml',
            label: 'XML_RDF',
            action: async (filename, event, extrasParams) => {
              const format = SparqlResultFormat.xmlRdf;
              this.dataService.downloadRdfData(filename, format, filter);
            },
          },
          {
            key: 'turtle',
            label: 'TURTLE',
            action: async (filename, event, extrasParams) => {
              const format = SparqlResultFormat.turtle;
              this.dataService.downloadRdfData(filename, format, filter);
            },
          },
        ],
      },
    ]
  }

}
