import {AfterViewInit, Component, ElementRef, Injector, Input, OnInit, ViewChild} from '@angular/core';
import {PUBLIC_CONFIG_DEFAULTS} from '@app/public/public.config';
import {CoastalStructureType, CoastalStructureTypeFilter} from '@app/referential/coastal-structure/coastal-structure.model';
import {CoastalStructureTypeService} from '@app/referential/coastal-structure/coastal-structure.service';
import {ReadonlyBaseTable} from '@app/shared/table/readonly-base.table';
import {AccountService, FilesUtils, isNotNilOrBlank, IStatus, LoadResult, PropertyFormatPipe, waitForFalse} from '@sumaris-net/ngx-components';
import {BehaviorSubject, combineLatest, merge, Observable} from 'rxjs';
import {debounceTime, filter, tap} from 'rxjs/operators';
import {IonFooter, IonHeader, PopoverController} from '@ionic/angular';
import {ExportsPopoverComponent} from '@app/shared/popover/exports/exports-popover.component';
import {SparqlResultFormat} from '@app/core/sparql/sparql.model';
import {exportToCSV} from '@app/shared/entities/entities.utils';
import {getCountNbOfPages} from '@app/shared/misc-utils/misc-utils.function';
import {ExportsType} from '@app/shared/popover/exports/exports-popover.model';

@Component({
  selector: 'coastal-structure-type-table',
  templateUrl: './table.component.html',
  styleUrls: [
    '../../../../../public/public.scss',
    './table.component.scss'],
})
export class CoastalStructureTypeTable
  extends ReadonlyBaseTable<CoastalStructureType, string, CoastalStructureTypeFilter>
  implements OnInit, AfterViewInit {

  private readonly _logPrefix = '[coastal-structure-type-table]';

  protected propertyFormat: PropertyFormatPipe;
  protected popoverController: PopoverController;

  excludesColumns: string[] = ['description', 'childrens'];
  availableExportsTypes: ExportsType[];
  elementUsedToCalculateHeightOfIonContent: ElementRef[] = [];

  $category = new BehaviorSubject<{ name: string, code: string }[]>(undefined);
  $class = new BehaviorSubject<{ name: string, code: string }[]>(undefined);
  $statusList = new BehaviorSubject<IStatus[]>(undefined);

  get detailsPageUrl(): string {
    return this.router.url.split('/').slice(0, -1).join('/') + '/'
  };

  @Input() debounceTime: number = PUBLIC_CONFIG_DEFAULTS.debounceTime;
  // TODO : make this required
  @Input() newDataUrl: string;
  @Input() availableHeightForTheComponent = 0;

  // Used to auto calculate available height for the
  @ViewChild(IonFooter, {read: ElementRef}) ionFooterElementRef: ElementRef;
  @ViewChild(IonHeader, {read: ElementRef}) ionHeaderElementRef: ElementRef;

  constructor(
    protected injector: Injector,
    protected entityService: CoastalStructureTypeService,
    protected accountService: AccountService,
  ) {
    super(
      injector,
      CoastalStructureType,
      CoastalStructureTypeFilter,
      entityService,
      {
        i18nColumnPrefix: 'COASTAL_STRUCTURE_TYPE.SEARCH.',
        //restoreFilterOrLoad: accountService.isLogin() || false,
        restoreCompactMode: accountService.isLogin() || false,
        restoreColumnWidths: accountService.isLogin() || false
      },
    );
    if (this.debug) console.debug(`${this._logPrefix} : constructor`)
    this.popoverController = this.injector.get(PopoverController);
    this.propertyFormat = this.injector.get(PropertyFormatPipe);
  }

  ngOnInit(): void {
    if (this.debug) console.debug(`${this._logPrefix} : ngOnInit`)
    super.ngOnInit();
    this.initSubscription();
    this.loadAvailableExportsFormats();
  }

  ngAfterViewInit(): void {
    super.ngAfterViewInit();
    this.elementUsedToCalculateHeightOfIonContent.push(this.ionHeaderElementRef);
    this.elementUsedToCalculateHeightOfIonContent.push(this.ionFooterElementRef);
  }

  navigateToDetail(code:string) {
    this.router.navigateByUrl(this.detailsPageUrl + code);
  }

  async showExportsPopover(event: UIEvent) {
    if (this.debug) console.debug(`${this._logPrefix} : showExportsPopover`, {event});
    const popover = await this.popoverController.create({
      component: ExportsPopoverComponent,
      event,
      componentProps: {
        defaultFileName: this.translate.instant('COASTAL_STRUCTURE_TYPE.EXPORT_FILE_NAME'),
        exportsTypes: this.availableExportsTypes,
      },
    });
    await popover.present();
  }

  protected initSubscription(): void {
    if (this.debug) console.debug(`${this._logPrefix} : initSubscription`)
    this.registerSubscription(
      this.entityService.watchCategories()
        .subscribe((items) => {
          this.$category.next(items.data);
        })
    )
    this.registerSubscription(
      this.entityService.watchClasses()
        .subscribe((items) => {
          this.$class.next(items.data);
        })
    )
    this.registerSubscription(
      this.entityService.watchStatusList()
        .subscribe((items) => {
          this.$statusList.next(items.data);
        })
    );
    // Mark as ready when categories and class lists are provided
    this.registerSubscription(
      combineLatest([this.$category, this.$class])
        .pipe(filter(([resCateg, resClass]) => (resCateg !== undefined && resClass !== undefined)))
        .subscribe((_) => this.markAsReady())
    )
    this.registerSubscription(
      merge(
        this.filterForm.get('searchText').valueChanges,
        this.filterForm.get('code').valueChanges,
        this.filterForm.get('categoryCode').valueChanges,
        this.filterForm.get('classCode').valueChanges,
        this.filterForm.get('validityStatusID').valueChanges,
      ).pipe(
        tap(() => this.markAsLoading()),
        debounceTime(this.debounceTime)
      ).subscribe(q => {
        this.onRefresh.emit();
      })
    );
    this.registerSubscription(
      this.filterForm.get('categoryCode').valueChanges.subscribe((sub) => {
        if (isNotNilOrBlank(sub) && isNotNilOrBlank(this.filterForm.get('classCode').value))
          this.filterForm.get('classCode').reset();
      })
    );
    this.registerSubscription(
      this.filterForm.get('classCode').valueChanges.subscribe((sub) => {
        if (isNotNilOrBlank(sub) && isNotNilOrBlank(this.filterForm.get('categoryCode')))
          this.filterForm.get('categoryCode').reset();
      })
    );
  }

  protected getEntityDisplayProperties<T>(dataType: { new(): any }): string[] {
    if (this.debug) console.debug(`${this._logPrefix} : getEntityDisplayProperties`)
    return super.getEntityDisplayProperties(dataType)
      // Insert updateDate just after 'creationDate'
      .reduce((res, key) => {
        if (key === <keyof CoastalStructureType>'creationDate')
          return res.concat(key, <keyof CoastalStructureType>'updateDate');
        return res.concat(key);
      }, []);
  }

  protected getFilterFormConfig(): any {
    if (this.debug) console.debug(`${this._logPrefix} : getFilterFormConfig`)
    return {
      searchText: [null],
      code: [null],
      categoryCode: [null],
      classCode: [null],
      validityStatusID: [null],
    };
  }

  protected async retrieveDataByPagesForExport(): Promise<Observable<LoadResult<CoastalStructureType>[]>> {
    // Await data are loaded to get this.totalRowCount
    await waitForFalse(this.loadingSubject);
    const nbOfPage = getCountNbOfPages(this.totalRowCount, this.entityService.nbOfLinesToFetchWenGetAllWithPaginate);
    return combineLatest(
      Array(nbOfPage)
        .fill(1)
        .map((_, index) => index * this.entityService.nbOfLinesToFetchWenGetAllWithPaginate)
        .map((offset) => this.entityService.watchAll(
          offset,
          this.entityService.nbOfLinesToFetchWenGetAllWithPaginate,
          this.sortActive,
          this.sortDirection,
          this.filter,
          {withTotal: false},
        ))
    );
  }

  protected loadAvailableExportsFormats() {
    if (this.debug) console.debug(`${this._logPrefix} : loadAvailableExportsFormats`);
    this.availableExportsTypes = [
      {
        key: 'simple',
        label: 'SIMPLE',
        formats: [
          {
            key: 'csv',
            label: 'CSV',
            action: async (fileName, event, extrasParams) => {
              const datas = (await this.entityService.loadAllByPage(this.sortActive, this.sortDirection, this.filter)).data;
              const result = CoastalStructureType.convertDataToCsvExport(datas, this.translate);
              exportToCSV(
                result.values,
                result.headers,
                'COASTAL_STRUCTURE_TYPE.PAGE.FIELDS.',
                this.translate,
                fileName,
                this.propertyFormat,
                {withTotal: false},
              );
            },
          },
          {
            key: 'json',
            label: 'JSON',
            action: async (fileName, event, extrasParams) => {
              const datas = (await this.entityService.loadAllByPage(this.sortActive, this.sortDirection, this.filter)).data;
              FilesUtils.writeTextToFile(
                JSON.stringify(datas, null, 2),
                {
                  type: 'application/json',
                  filename: fileName,
                }
              );
            },
          },
        ]
      },
      {
        key: 'webSemantic',
        label: 'WEB_SEMANTIC',
        formats: [
          {
            key: 'rdf.json',
            label: 'JSON_RDF',
            action: async (filename, event, extrasParams) => {
              const format = SparqlResultFormat.jsonRdf;
              this.entityService.downloadRdfData(filename, format, this.filter);
            },
          },
          {
            key: 'rdf.xml',
            label: 'XML_RDF',
            action: async (filename, event, extrasParams) => {
              const format = SparqlResultFormat.xmlRdf;
              this.entityService.downloadRdfData(filename, format, this.filter);
            },
          },
          {
            key: 'turtle',
            label: 'TURTLE',
            action: async (filename, event, extrasParams) => {
              const format = SparqlResultFormat.turtle;
              this.entityService.downloadRdfData(filename, format, this.filter);
            },
          },
        ],
      },
    ]
  }

}
