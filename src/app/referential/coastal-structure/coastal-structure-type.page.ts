import {Component, Inject, Injector, Input, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {SparqlResultFormat} from '@app/core/sparql/sparql.model';
import {Popovers} from '@app/shared/popover/popover.utils';
import {PopoverController} from '@ionic/angular';
import {TranslateService} from '@ngx-translate/core';
import {
  ConfigService,
  ENVIRONMENT,
  FilesUtils,
  FormFieldDefinition,
  isNotNilOrBlank,
  LocalSettingsService,
  MenuService,
  PlatformService,
  PropertyFormatPipe,
  toBoolean
} from '@sumaris-net/ngx-components';
import {BehaviorSubject, Subscription} from 'rxjs';
import {CoastalStructureType, CoastalStructureTypeFilter} from './coastal-structure.model';
import {CoastalStructureTypeService} from './coastal-structure.service';
import {exportToCSV, getEntityFieldDefinitions} from '@app/shared/entities/entities.utils';
import {ExportsPopoverComponent} from '@app/shared/popover/exports/exports-popover.component';
import {ExportsType} from '@app/shared/popover/exports/exports-popover.model';
import {CoastalStructureTypeSearchView} from './type/search/search-view.component';
import {COASTAL_STRUCTURE_TYPE_CONFIG_OPTION} from './coastal-structure-type.config';

@Component({
  selector: 'app-coastal-structure-type',
  templateUrl: './coastal-structure-type.page.html',
  styleUrls: ['./coastal-structure-type.page.scss']
})
export class CoastalStructureTypePage implements OnInit, OnDestroy {

  private _logPrefix = '[coastal-structure-type-page]';

  protected readonly fieldsToDisplay: string[] = [
    'code',
    'description',
    'level',
    'class',
    'category',
    // 'validityStatus',
    'status',
    'creationDate',
    'updateDate',
  ];

  protected route: ActivatedRoute;
  protected router: Router;
  protected settings: LocalSettingsService;
  protected platform: PlatformService;
  protected configService: ConfigService;
  protected propertyFormat: PropertyFormatPipe;
  protected popoverController: PopoverController;
  protected propertyFormatPipe: PropertyFormatPipe;
  protected $subscription = new Subscription();
  protected loadingSubject = new BehaviorSubject<boolean>(true);
  protected debug: boolean = false;

  protected id: number;

  data: CoastalStructureType;
  title: string;
  helpUrl: string;
  filedDefinition: FormFieldDefinition[] = [];
  availableExportsTypes: ExportsType[];

  readonly i18nPrefix = 'COASTAL_STRUCTURE_TYPE.PAGE.';
  readonly errorSubject = new BehaviorSubject<string>(undefined);

  @Input() showToolbar = true;

  get loading(): boolean { return this.loadingSubject.value };
  get loaded(): boolean { return !this.loadingSubject.value };
  get detailsPageUrl(): string { return this.router.url.split('/').slice(0, -1).join('/') + '/' };

  constructor(
    private injector: Injector,
    private coastalStructureService: CoastalStructureTypeService,
    private translate: TranslateService,
    protected menu: MenuService,
    @Inject(ENVIRONMENT) environment,
  ) {
    this.debug = !environment.production;
    if (this.debug) console.debug(`${this._logPrefix} : constructor`);
    this.route = this.injector.get(ActivatedRoute);
    this.router = this.injector.get(Router);
    this.settings = this.injector.get(LocalSettingsService);
    this.configService = this.injector.get(ConfigService);
    this.platform = this.injector.get(PlatformService);
    this.propertyFormat = this.injector.get(PropertyFormatPipe);
    this.title = this.i18nPrefix && (this.i18nPrefix + 'TITLE') || '';
    this.popoverController = this.injector.get(PopoverController);
    this.propertyFormatPipe = injector.get(PropertyFormatPipe);
  }

  ngOnInit() {
    // this.coastalStructureService.ready();
    const routeData = this.route.snapshot.data;
    if (routeData) {
      const showMenu = toBoolean(routeData.menu, true);
      this.menu.enable(showMenu);
      this.showToolbar = toBoolean(routeData.showToolbar, showMenu);
    }
    this.start();
    this.initSubscription();
  }

  ngOnDestroy(): void {
    if (this.debug) console.debug(`${this._logPrefix} : ngOnDestroy`);
    this.$subscription.unsubscribe();
  }

  async start() {
    if (this.debug) console.debug(`${this._logPrefix} : start`);
    await this.platform.ready();
    await this.coastalStructureService.ready();
    try {
      await this.load();
      this.filedDefinition = this.getFieldsDefinition();
    } catch (err) {
      this.setError(err && err.message || err);
    }
    this.markAsLoaded();
  }

  async load() {
    if (this.debug) console.debug(`${this._logPrefix} : load`);
    this.id = this.route.snapshot.params.id;
    this.data = await this.coastalStructureService.load(this.id);
    this.loadAvailableExportsFormats();
  }

  protected initSubscription(): void {
    if (this.debug) console.debug(`${this._logPrefix} : initSubscription`);
    this.$subscription.add(
      this.configService.config.subscribe((sub) => {
        this.helpUrl = sub.getProperty(COASTAL_STRUCTURE_TYPE_CONFIG_OPTION.COASTAL_STRUCTURE_TYPE_HELP_URL);
      })
    );

  }

  protected markAsLoaded() {
    if (this.debug) console.debug(`${this._logPrefix} : markAsLoaded`);
    if (this.loadingSubject.value) {
      this.loadingSubject.next(false);
    }
  }

  private loadAvailableExportsFormats() {
    if (this.debug) console.debug(`${this._logPrefix} : loadAvailableExportsFormats`);
    this.availableExportsTypes = [
      {
        key: 'simple',
        label: 'SIMPLE',
        formats: [
          {
            key: 'csv',
            label: 'CSV',
            action: async (fileName, event, extrasParams) => {
              const result = CoastalStructureType.convertDataToCsvExport([this.data], this.translate);
              exportToCSV(
                result.values,
                result.headers,
                this.i18nPrefix + 'FIELDS.',
                this.translate,
                fileName,
                this.propertyFormatPipe,
              );
            }
          },
          {
            key: 'json',
            label: 'JSON',
            action: async (fileName, event, extrasParams) => {
              FilesUtils.writeTextToFile(
                JSON.stringify(this.data.asObject(), null, 2),
                {
                  type: 'application/json',
                  filename: fileName,
                }
              );
            }
          },
        ]
      },
      {
        key: 'webSemantic',
        label: 'WEB_SEMANTIC',
        formats: [
          {
            key: 'rdf.json',
            label: 'JSON_RDF',
            action: async (filename, event, extrasParams) => {
              const format = SparqlResultFormat.jsonRdf;
              const filter = CoastalStructureTypeFilter.fromObject({ code: this.data.code });
              this.coastalStructureService.downloadRdfData(filename, format, filter);
            },
          },
          {
            key: 'rdf.xml',
            label: 'XML_RDF',
            action: async (filename, event, extrasParams) => {
              const format = SparqlResultFormat.xmlRdf;
              const filter = CoastalStructureTypeFilter.fromObject({ code: this.data.code });
              this.coastalStructureService.downloadRdfData(filename, format, filter);
            },
          },
          {
            key: 'turtle',
            label: 'TURTLE',
            action: async (filename, event, extrasParams) => {
              const format = SparqlResultFormat.turtle;
              const filter = CoastalStructureTypeFilter.fromObject({ code: this.data.code });
              this.coastalStructureService.downloadRdfData(filename, format, filter);
            },
          },
        ],
      },
    ]
  }

  getFieldsDefinition(): FormFieldDefinition[] {
    if (this.debug) console.debug(`${this._logPrefix} : getFieldsDefinition`);
    const res2 = Object.keys(this.data)
      .filter((d) => this.fieldsToDisplay.includes(d))
    ;
    const res = getEntityFieldDefinitions(this.fieldsToDisplay, this.i18nPrefix + 'FIELDS.')
      .map((item) => {
        switch (item.key) {
          case 'category':
            if (isNotNilOrBlank(this.data.parent)) {
              if (isNotNilOrBlank(this.data.parent.parent)) {
                item.values = [
                  {key: 'code', value: this.data.parent.parent.code},
                  {key: 'name', value: this.data.parent.parent.name},
                ]
              } else {
                item.values = [
                  {key: 'code', value: this.data.parent.code},
                  {key: 'name', value: this.data.parent.name},
                ]
              }
            } else {
              item.values = [];
            }
            break;
          case 'class':
            if (isNotNilOrBlank(this.data.parent) && isNotNilOrBlank(this.data.parent.parent)) {
              item.values = [
                {key: 'code', value: this.data.parent.code},
                {key: 'name', value: this.data.parent.name},
              ]
            } else {
              item.values = [];
            }
            break;
          default:
            item.values = [this.data[item.key]];
        }
        return item;
      })
      .filter((item) => !!item.values[0]); // Hide fields when they have no value (by example Category has no Class)
    return res;
  }

  backToSearch() {
    if (this.debug) console.debug(`${this._logPrefix} : backToSearch`);
    const defaultSearchView = this.settings.getPageSettings(
      CoastalStructureTypeSearchView.pageSettingsId,
      CoastalStructureTypeSearchView.pageSettingDefaultView,
    );
    let urlParams = '';
    if (defaultSearchView === 'tree') urlParams = '?defaultView=tree';
    this.router.navigateByUrl(this.detailsPageUrl + 'search' + urlParams);
  }

  showEmbeddedPopover(event: UIEvent) {
    if (this.debug) console.debug(`${this._logPrefix} : showEmbeddedPopover`, { event });
    if (!this.popoverController) {
      this.popoverController = this.injector.get(PopoverController);
    }
    const url = window.location.origin
      + this.router.createUrlTree(['..', 'embed', this.id], { relativeTo: this.route }).toString();
    const title = this.translate.instant(this.title);
    const html = `<iframe src="${url}" title="${title}" width="100%" height="800px" sandbox="allow-same-origin allow-scripts allow-popups allow-forms allow-downloads" frameborder="0" allow="clipboard-write;"></iframe>`;
    return Popovers.showText(
      this.popoverController,
      event,
      {
        editing: false,
        multiline: true,
        text: html,
        placeholder: this.translate.instant(this.i18nPrefix + 'EMBEDDED_IFRAME_HELP'),
        maxLength: 0,  // Disable text progress hint
      }
    );
  }

  notePage(note: number) {
    console.debug(`${this._logPrefix} : notePage`, { note });
  }

  async showExportsPopover(event: UIEvent) {
    if (this.debug) console.debug(`${this._logPrefix} : showExportsPopover`, { event });
    const popover = await this.popoverController.create({
      component: ExportsPopoverComponent,
      event,
      componentProps: {
        defaultFileName: this.translate.instant('COASTAL_STRUCTURE_TYPE.EXPORT_FILE_NAME') + `_${this.id}`,
        exportsTypes: this.availableExportsTypes,
      },
    });
    await popover.present();
  }

  protected setError(value: string, opts?: { emitEvent?: boolean }) {
    if (this.debug) console.debug(`${this._logPrefix} : setError`, { value, opts })
    if (this.errorSubject.value !== value) {
      this.errorSubject.next(value);
    }
  }

}
