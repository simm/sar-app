import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

type StarItem = {
  index: number;
  status: 'selected' | 'unselected';
}

@Component({
  selector: 'star-rating',
  templateUrl: './star-rating.component.html',
  styleUrls: ['./star-rating.component.scss']
})
export class StarRatingComponent implements OnInit {

  private _logPrefix = '[star-rating]';

  starItems: StarItem[] = [];

  @Input() nbOfStart: number = 0;
  @Input() color = 'black';
  @Output() note = new EventEmitter<number>();

  constructor() {
    console.debug(`${this._logPrefix} : constructor`);
  }

  ngOnInit(): void {
    console.debug(`${this._logPrefix} : ngOnInit`);
    this.starItems = Array(this.nbOfStart).fill(1).map((_, index) => {
      return { index: index, status: 'unselected' };
    });
  }

  selectStar(selectedStar: StarItem) {
    console.debug(`${this._logPrefix} : selectStar`, { selectedStar })
    selectedStar.status = 'selected';
    this.starItems.filter((star) => star.index < selectedStar.index)
      .forEach((star) => {
        star.status = 'selected';
      });
    this.starItems.filter((star) => star.index > selectedStar.index)
      .forEach((star) => {
        star.status = 'unselected';
      });
  }

  clickStar(selectedStar: StarItem) {
    this.selectStar(selectedStar);
    console.debug('MYTEST', selectedStar.index);
    this.note.emit(selectedStar.index);
  }

}
