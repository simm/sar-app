import { NgModule } from '@angular/core';
import { StarRatingComponent } from './star-rating.component';
import { AppCoreModule } from '@app/core/core.module';
import { MatIcon } from '@angular/material/icon';



@NgModule({
  declarations: [
    StarRatingComponent,
  ],
  imports: [
    AppCoreModule,
  ],
  exports: [
    StarRatingComponent,
  ],
})
export class StarRatingModule { }
