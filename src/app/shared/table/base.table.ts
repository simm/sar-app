import {AfterViewInit, ChangeDetectorRef, Directive, ElementRef, Injector, Input, OnDestroy, OnInit, QueryList, ViewChild, ViewChildren} from '@angular/core';
import {
  AppTable,
  EntitiesTableDataSourceConfig,
  EntitiesServiceWatchOptions,
  EntitiesTableDataSource,
  Entity,
  EntityFilter,
  ENVIRONMENT,
  IEntitiesService,
  ResizableComponent
} from '@sumaris-net/ngx-components';
import {TableElement} from '@e-is/ngx-material-table';
import {PredefinedColors} from '@ionic/core';
import {FormGroup} from '@angular/forms';
import {BaseValidatorService} from '@app/shared/service/base.validator.service';
import {MatExpansionPanel} from '@angular/material/expansion';
import {Subscription} from 'rxjs';
import {debounceTime} from 'rxjs/operators';


export const BASE_TABLE_SETTINGS_ENUM = {
  filterKey: 'filter',
  compactRowsKey: 'compactRows',
  columnWidths: 'columnWidths'
};

export declare interface ColumnWidthSetting {
  column: string;
  width: number;
}

export interface BaseTableOptions<
  T extends Entity<T, ID>,
  ID = number,
  O extends EntitiesServiceWatchOptions = EntitiesServiceWatchOptions,
  SO = any>
  extends EntitiesTableDataSourceConfig<T, ID, O> {

  restoreFilterOrLoad?: boolean;
  restoreCompactMode?: boolean;
  restoreColumnWidths?: boolean;
}

@Directive()
export abstract class AppBaseTable<E extends Entity<E, ID>,
  ID,
  F extends EntityFilter<any, E, any>,
  V extends BaseValidatorService<E, ID> = any>
  extends AppTable<E, F, ID> implements OnInit, OnDestroy, AfterViewInit {

  @Input() canGoBack = false;
  @Input() showTitle = true;
  @Input() showPaginator = true;
  @Input() showFooter = true;
  @Input() toolbarColor: PredefinedColors = 'primary';
  @Input() sticky = false;
  @Input() stickyEnd = false;
  @Input() compact = false;

  @ViewChild('tableContainer', { read: ElementRef }) tableContainerRef: ElementRef;
  @ViewChild(MatExpansionPanel, {static: true}) filterExpansionPanel: MatExpansionPanel;
  @ViewChildren(ResizableComponent) resizableColumns: QueryList<ResizableComponent>;

  filterForm: FormGroup = null;
  filterCriteriaCount = 0;
  filterPanelFloating = true;
  columnWidthSubscriptions: { name: string; subscription: Subscription }[] = [];

  get filterIsEmpty(): boolean {
    return this.filterCriteriaCount === 0;
  }

  protected logPrefix: string = null;
  protected cd: ChangeDetectorRef;

  constructor(
    protected injector: Injector,
    columns: string[],
    protected dataType: new () => E,
    protected filterType: new () => F,
    protected entityService: IEntitiesService<E, F>,
    protected validatorService?: V,
    protected options?: BaseTableOptions<E, ID>
  ) {
    super(
      injector,
      columns,
      new EntitiesTableDataSource<E, F, ID>(dataType, entityService, validatorService, {
          prependNewElements: false,
          keepOriginalDataAfterConfirm: true,
          suppressErrors: injector.get(ENVIRONMENT).production,
          onRowCreated: (row) => this.onDefaultRowCreated(row),
          ...options,
          saveAllOptions: {
            saveOnlyDirtyRows: true,
            ...options?.saveAllOptions
          }
        }),
        null
    );
    this.cd = injector.get(ChangeDetectorRef);

    this.i18nColumnPrefix = options?.i18nColumnPrefix || '';
    this.logPrefix = '[contaminant-table]';
    this.defaultSortBy = 'label';
  }

  ngOnInit() {
    super.ngOnInit();

    // Restore compact mode
    if (this.options?.restoreCompactMode !== false) {
      this.restoreCompactMode();
    }

    // Restore column widths
    if (this.options?.restoreColumnWidths !== false) {
      this.restoreColumnWidths();
    }

  }

  ngAfterViewInit() {
    super.ngAfterViewInit();

    // Create resizable column subscriptions
    this.registerSubscription(
      this.listenResizableColumns()
    );
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.columnWidthSubscriptions.forEach(col => col.subscription?.unsubscribe());
  }

  scrollToBottom() {
    if (this.tableContainerRef) {
      // scroll to bottom
      this.tableContainerRef.nativeElement.scroll({
        top: this.tableContainerRef.nativeElement.scrollHeight,
      });
    }
  }

  setFilter(filter: F, opts?: { emitEvent: boolean }) {

    filter = this.asFilter(filter);

    // Update criteria count
    const criteriaCount = filter.countNotEmptyCriteria();
    if (criteriaCount !== this.filterCriteriaCount) {
      this.filterCriteriaCount = criteriaCount;
      this.markForCheck();
    }

    // Update the form content
    if (!opts || opts.emitEvent !== false) {
      this.filterForm.patchValue(filter.asObject(), {emitEvent: false});
    }

    super.setFilter(filter, opts);
  }

  toggleFilterPanelFloating() {
    this.filterPanelFloating = !this.filterPanelFloating;
    this.markForCheck();
  }

  applyFilterAndClosePanel(event?: UIEvent) {
    this.onRefresh.emit(event);
    if (this.filterExpansionPanel && this.filterPanelFloating) this.filterExpansionPanel.close();
  }

  closeFilterPanel() {
    if (this.filterExpansionPanel) this.filterExpansionPanel.close();
  }

  resetFilter(event?: UIEvent, opts = {emitEvent: true}) {
    this.filterForm.reset();
    this.setFilter(null, opts);
    this.filterCriteriaCount = 0;
    if (this.filterExpansionPanel && this.filterPanelFloating) this.filterExpansionPanel.close();
  }

  restoreCompactMode() {
    if (!this.compact) {
      const compact = this.settings.getPageSettings(this.settingsId, BASE_TABLE_SETTINGS_ENUM.compactRowsKey) || false;
      if (this.compact !== compact) {
        this.compact = compact;
        this.markForCheck();
      }
    }
  }

  toggleCompactMode() {
    this.compact = !this.compact;
    this.markForCheck();
    this.settings.savePageSetting(this.settingsId, this.compact, BASE_TABLE_SETTINGS_ENUM.compactRowsKey);
  }

  /* -- protected function -- */

  protected restoreFilterOrLoad(opts?: { emitEvent: boolean }) {
    this.markAsLoading();

    let json = this.settings.getPageSettings(this.settingsId, BASE_TABLE_SETTINGS_ENUM.filterKey);
    if (json) {
      console.debug(this.logPrefix + 'Restoring filter from settings...', json);
    }
    else {
      const {q} = this.route.snapshot.queryParams;
      if (q) {
        console.debug(this.logPrefix + 'Restoring filter from route query param: ', q);
        json = JSON.parse(q);
      }
    }

    if (json) {
      this.setFilter(json, opts);
    }
    else if (!opts || opts.emitEvent !== false) {
      this.onRefresh.emit();
    }
  }

  protected getColumnWidthSettings(): ColumnWidthSetting[] {
    return this.settings.getPageSettings(this.settingsId, BASE_TABLE_SETTINGS_ENUM.columnWidths) || [];
  }

  protected restoreColumnWidths() {
    console.info(this.logPrefix + 'Restoring column widths...');
    setTimeout(() => {
        this.cd.detectChanges(); // Needed to detect added columns
        this.getColumnWidthSettings().forEach((setting) => {
          if (this.debug) console.debug(`${this.logPrefix} Apply saved width on column '${setting.column}' width: ${setting.width}`);
          this.resizableColumns
            .find((column) => column.resizable && column.columnDef?.name === setting.column)
            ?.onResize(setting.width, { emitEvent: false });
        });
        this.markForCheck();
      }, 10);
  }

  protected listenResizableColumns(): Subscription {
    if (!this.resizableColumns) return null;
    return this.resizableColumns.changes
      .pipe(debounceTime(100))
      .subscribe(() => {
        this.resizableColumns
          .filter((column) => column.resizable && !!column.columnDef?.name)
          .forEach((column) => {
            // If no observer on size changed event
            if (!column.sizeChanged.observers.length) {
              const name = column.columnDef.name;
              if (this.debug) console.debug(`${this.logPrefix} Add subscription on resizable column: ${name}`);
              // Create subscription
              const subscription = column.sizeChanged.subscribe((width) => this.saveColumnWidth(name, width));
              const existing = this.columnWidthSubscriptions.find((sub) => sub.name === name);
              if (existing) {
                // Clear previous subscription and replace it
                existing.subscription.unsubscribe();
                existing.subscription = subscription;
              } else {
                // Register new one
                this.columnWidthSubscriptions.push({ name, subscription });
              }
            }
          });
      });
  }

  protected async saveColumnWidth(column: string, width: number) {
    const columns = this.getColumnWidthSettings();
    const currentSetting = columns.find((c) => c.column === column);
    if (currentSetting) {
      currentSetting.width = width;
    } else {
      columns.push({ column, width });
    }

    await this.settings.savePageSetting(this.settingsId, columns, BASE_TABLE_SETTINGS_ENUM.columnWidths);
  }

  protected onDefaultRowCreated(row: TableElement<E>) {
    if (row.validator) {
      row.validator.patchValue(this.defaultNewRowValue());
    } else {
      Object.assign(row.currentData, this.defaultNewRowValue());
    }

    this.clickRow(undefined, row);
    this.scrollToBottom();
  }

  protected defaultNewRowValue(): any {
    return {};
  }

  protected asFilter(source: Partial<F>): F {
    const target = new this.filterType();
    if (source) target.fromObject(source);
    return target;
  }

  protected getExportFileName(): string {
    const key = this.i18nColumnPrefix + 'EXPORT_CSV_FILENAME';
    const filename = this.translate.instant(key, this.filter?.asObject());
    if (filename !== key) return filename;
    return 'export.csv'; // Default filename
  }

  protected getExportSeparator(): string {
    const key = 'FILE.CSV.SEPARATOR';
    const separator = this.translate.instant(key);
    if (separator !== key) return separator;
    return ','; // Default separator
  }

  protected getExportEncoding(): string {
    const key = 'FILE.CSV.ENCODING';
    const encoding = this.translate.instant(key);
    if (encoding !== key) return encoding;
    return 'UTF-8'; // Default encoding
  }
}
