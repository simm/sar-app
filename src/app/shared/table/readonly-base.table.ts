import {AfterViewInit, Directive, Injector, Input, OnInit, ViewChild} from '@angular/core';
import {
  AppFormUtils,
  changeCaseToUnderscore,
  CsvUtils,
  EntitiesServiceWatchOptions,
  Entity,
  EntityFilter,
  FormFieldDefinition,
  IEntitiesService, isNotNil,
  PlatformService,
  PropertyFormatPipe,
  RESERVED_END_COLUMNS,
  RESERVED_START_COLUMNS,
  StartableService, FormFieldType
} from '@sumaris-net/ngx-components';
import {AppBaseTable, BASE_TABLE_SETTINGS_ENUM, BaseTableOptions} from './base.table';
import {environment} from '@environments/environment';
import {FormBuilder} from '@angular/forms';
import {debounceTime, filter, tap} from 'rxjs/operators';
import {IonInfiniteScroll} from '@ionic/angular';
import {BtnMenuActionItem} from '../menu/btn-menu-action/btn-menu-action.model';

export interface ReadonlyTableDataSourceOptions<
  T extends Entity<T, ID>,
  ID = number,
  O extends EntitiesServiceWatchOptions = EntitiesServiceWatchOptions>
  extends BaseTableOptions<T, ID, O> {

  propertyNames?: string[];
  restoreFilterOrLoad?: boolean;
}

export declare type ColumnType = FormFieldType | 'uri';


@Directive()
export abstract class ReadonlyBaseTable<E extends Entity<E, ID>,
  ID,
  F extends EntityFilter<any, E, any>
>
  extends AppBaseTable<E, ID, F>
  implements OnInit, AfterViewInit {

  static IGNORED_ENTITY_COLUMNS = ['__typename', 'id', 'updateDate'];

  static getFirstEntityColumn<T>(dataType: new () => T): string {
    return Object.keys(new dataType()).find(key => !this.IGNORED_ENTITY_COLUMNS.includes(key));
  }

  downloadMenuEntry: BtnMenuActionItem[] = [];

  @Input() title: string;
  @Input() columnDefinitions: FormFieldDefinition[] = null;
  @Input() enableInfiniteScroll = false;
  @Input() canDownload: boolean;
  @Input() exportFetchPageSize = 500;

  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  protected platform: PlatformService;
  protected propertyFormatPipe: PropertyFormatPipe;

  constructor(
    injector: Injector,
    dataType: new () => E,
    filterType: new () => F,
    entityService: IEntitiesService<E, F>,
    options?: ReadonlyTableDataSourceOptions<E, ID>
  ) {
    super(
      injector,
      null,
      dataType,
      filterType,
      entityService,
      null,
      options
    );

    this.platform = injector.get(PlatformService);
    this.propertyFormatPipe = injector.get(PropertyFormatPipe);
    this.title = this.i18nColumnPrefix && (this.i18nColumnPrefix + 'TITLE') || '';
    this.logPrefix = '[readonly-table] ';
    this.autoLoad = false; // filter restoration will load data

    // Compute columns from entity
    const propertyNames = options?.propertyNames || this.getEntityDisplayProperties(dataType)
    this.columns = RESERVED_START_COLUMNS
      .concat(propertyNames)
      .concat(RESERVED_END_COLUMNS);
    this.columnDefinitions = this.getColumnDefinitions(propertyNames);

    // Compute filter form
    const config = this.getFilterFormConfig();
    this.filterForm = config && injector.get(FormBuilder).group(config);

    this.defaultSortBy = ReadonlyBaseTable.getFirstEntityColumn(dataType);
    this.debug = !environment.production;

  }

  ngOnInit() {
    super.ngOnInit();

    this.registerSubscription(
      this.onRefresh.subscribe(() => {
        this.filterForm.markAsUntouched();
        this.filterForm.markAsPristine();
      }));

    // Update filter when changes
    this.registerSubscription(
      this.filterForm.valueChanges
        .pipe(
          debounceTime(250),
          filter((_) => {
            const valid = this.filterForm.valid;
            if (!valid && this.debug) AppFormUtils.logFormErrors(this.filterForm);
            return valid;
          }),
          // Update the filter, without reloading the content
          tap(json => this.setFilter(json, {emitEvent: false})),
          // Save filter in settings (after a debounce time)
          debounceTime(500),
          tap(json => {
              const res = this.settings.savePageSetting(this.settingsId, json, BASE_TABLE_SETTINGS_ENUM.filterKey);
              return res;
            }
          )
        )
        .subscribe());

    if (this.options?.restoreFilterOrLoad !== false) {
      this.ready().then(() => this.restoreFilterOrLoad());
    }
  }

  async ready(): Promise<void> {
    await (this.entityService instanceof StartableService
      ? this.entityService.ready()
      : this.platform.ready());

    return super.ready();
  }

  getDefaultDownloadMenuEntryTableData(): BtnMenuActionItem[] {
    return [
      {
        label: 'CSV',
        action: async (event) => {
          this.exportToCsv(event);
        },
      },
    ]
  }

  applyFilterAndClosePanel(event?: UIEvent) {
    const filter = this.filterForm.value;
    this.setFilter(filter, {emitEvent: false});
    super.applyFilterAndClosePanel(event);
  }

  fetchMore(event?: CustomEvent & { target?: EventTarget & { complete?: () => void } }) {
    console.debug(this.logPrefix + 'TODO Fetching more...', event);

    //this.markAsLoading();
    this._dataSource.fetchMore()
      .then(fetched => {
        /*this.infiniteScroll.disabled = !fetched;

        // When target wait for a complete (e.g. IonRefresher)
        if (event?.target && event.target.complete) {
          //setTimeout(async () => {
            //await this.waitIdle();
          event.target.complete();
          //});
        }*/
      });
  }

  async exportToCsv(event: UIEvent) {
    const filename = this.getExportFileName();
    const separator = this.getExportSeparator();
    const encoding = this.getExportEncoding();
    const columnDefinitions = this.displayedColumns.slice() // Copy
      .filter(key => !RESERVED_START_COLUMNS.includes(key) && !RESERVED_END_COLUMNS.includes(key))
      .map(key => this.columnDefinitions.find(def => def.key === key))
      .filter(isNotNil);

    const headers = columnDefinitions.map(def => this.translate.instant(def.label)) as string[];
    let allPages = [];
    for (let offset = 0; offset < this.totalRowCount; offset += this.exportFetchPageSize) {
      // Do not re-fetch current data
      let result = await this.entityService.watchAll(offset, this.exportFetchPageSize, this.sortActive, this.sortDirection).toPromise();
      allPages.push(result.data);
    }
    const rows = allPages.flat().map(data => columnDefinitions.map(def => this.propertyFormatPipe.transform(data, def)));
    CsvUtils.exportToFile(rows, {filename, headers, separator, encoding});
  }

  /* -- protected functions -- */

  protected getEntityDisplayProperties<T>(dataType: new () => any): string[] {
    return Object.keys(new dataType()).filter(key => !ReadonlyBaseTable.IGNORED_ENTITY_COLUMNS.includes(key));
  }

  protected getColumnType(key: string): ColumnType {
    const lowerKey = key.toLowerCase();
    if (lowerKey.endsWith('date')) return 'date';
    if (lowerKey.endsWith('uri')) return 'uri';
    if (lowerKey.endsWith('label') || lowerKey.endsWith('name') || lowerKey.endsWith('code')) return 'string';
    if (lowerKey === 'measurement' || lowerKey.endsWith('longitude') || lowerKey.endsWith('latitude')) return 'double';
    return 'string';
  }

  protected getColumnDefinitions(propertyNames: string[]) {
    return propertyNames
      .map(key => <FormFieldDefinition>{
        key,
        type: this.getColumnType(key),
        label: (this.i18nColumnPrefix) + changeCaseToUnderscore(key).toUpperCase()
      });
  }

  protected getFilterFormConfig(): any {
    console.debug(this.logPrefix + 'Creating filter form group...');
    return this.getEntityDisplayProperties(this.filterType)
      .reduce((config, key) => {
        console.debug(this.logPrefix + ' Adding filter control: ' + key);
        config[key] = [null];
        return config;
      }, {});
  }

}
