import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExportsPopoverComponent } from './exports-popover.component';
import { TranslateModule } from '@ngx-translate/core';
import { AppCoreModule } from '@app/core/core.module';



@NgModule({
  declarations: [
    ExportsPopoverComponent,
  ],
  imports: [
    AppCoreModule,
    CommonModule,
    TranslateModule.forChild(),
  ],
  exports: [
    ExportsPopoverComponent,
  ]
})
export class ExportsUtilsModule { }
