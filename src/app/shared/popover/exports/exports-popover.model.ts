export interface ExportsType {
  key: string,
  label: string,
  formats: ExportFormat[],
  i18nPrefix?: string,
}

export interface ExportFormat {
  key: string,
  label: string,
  action: (fileName: string, event?: UIEvent,  extrasParams?: any) => void,
}
