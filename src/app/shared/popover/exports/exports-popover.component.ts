import { AfterContentChecked, AfterContentInit, AfterViewInit, Component, ComponentFactoryResolver, ElementRef, Inject, Input, OnDestroy, OnInit, Renderer2, ViewChild } from '@angular/core';
import { FormControl, FormControlName, FormGroup, Validators } from '@angular/forms';
import { environment } from '@environments/environment';
import { PopoverController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { ENVIRONMENT, isNil } from '@sumaris-net/ngx-components';
import { Subscription } from 'rxjs';
import { ExportsType } from './exports-popover.model';

@Component({
  selector: 'app-exports',
  templateUrl: './exports-popover.component.html',
  styleUrls: ['./exports-popover.component.scss']
})
export class ExportsPopoverComponent implements OnInit, AfterContentChecked, OnDestroy {

  private readonly _logPrefix = '[exports-popover]'
  protected debug:boolean = false;
  protected $subscription = new Subscription();

  exportForm = new FormGroup({
    formatType:  new FormControl(null, Validators.required),
    exportsType:  new FormControl(null, Validators.required),
  });

  currentExportType:ExportsType = null;
  currentExportFormat = null;

  @Input() i18nPrefix = 'EXPORT_DATA.';
  @Input() exportsTypes:ExportsType[];
  @Input() defaultFileName: string = 'export';

  constructor(
    private _element: ElementRef,
    protected popoverController: PopoverController,
    protected translate: TranslateService,
    private _render: Renderer2,
    @Inject(ENVIRONMENT) environment,
  ) {
    this.debug = !environment.production;
    if (this.debug) console.debug(`${this._logPrefix} : constructor`);
  }

  ngOnInit(): void {
    if (this.debug) console.debug(`${this._logPrefix} : ngOnInit`);
    this.registerSubscriptions();
    this.setDefaultExportType();
  }

  ngOnDestroy(): void {
    if (this.debug) console.debug(`${this._logPrefix} : ngOnDestroy`);
    this.$subscription.unsubscribe();
  }

  ngAfterContentChecked(): void {
    this.checkHeightWindowOverflow();
  }

  protected checkHeightWindowOverflow(): void {
    const screenBottom = window.visualViewport.height;
    const popoverBottom = this._element.nativeElement.getBoundingClientRect().bottom;
    const offset = screenBottom - popoverBottom;
    const parent = this._element.nativeElement.offsetParent;
    if (offset < 0 && !isNil(parent)) {
      const newTop = screenBottom - parent.offsetHeight;
      this._render.setStyle(parent, 'top', `${newTop}px`);
    }
  }

  protected registerSubscriptions() {
    if (this.debug) console.debug(`${this._logPrefix} : registerSubscriptions`);
    this.$subscription.add(
      this.exportForm.controls.exportsType.valueChanges.subscribe((value) => {
        if (this.debug) console.debug(`${this._logPrefix} : exportsType valueChanges[exportsType]`, {value});
        this.setCurrentExportType(value);
      })
    );
    this.$subscription.add(
      this.exportForm.controls.formatType.valueChanges.subscribe((value) => {
        if (this.debug) console.debug(`${this._logPrefix} : formatType valueChanges[formatType]`, {value});
        this.setCurrentExportFormat(value);
      })
    );
  }

  cancel() {
    if (this.debug) console.debug(`${this._logPrefix} : cancel`);
    this.popoverController.dismiss();
  }

  validate(event: UIEvent) {
    if (this.debug) console.debug(`${this._logPrefix} : validate`);
    // TODO : Explicit error message if form is invalid or error on export
    if (this.exportForm.valid) {
      const fileNameWithExt = `${this.defaultFileName}.${this.currentExportFormat.key}`
      this.currentExportFormat.action(fileNameWithExt, event, {});
    }
    this.popoverController.dismiss();
  }

  protected setDefaultExportType() {
    if (this.debug) console.debug(`${this._logPrefix} : setDefaultExportType`);
    if (this.exportsTypes.length > 0) {
      const defaultExportType = this.exportsTypes[0];
      this.exportForm.controls.exportsType.setValue(defaultExportType.key);
    }
  }

  protected setDefaultExportFormat() {
    if (this.debug) console.debug(`${this._logPrefix} : setDefaultExportFormat`);
    if (this.currentExportType.formats.length > 0) {
      const defaultExportFormat = this.currentExportType.formats[0];
      this.exportForm.controls.formatType.setValue(defaultExportFormat.key);
    }
  }

  protected setCurrentExportType(selected?:string) {
    if (this.debug) console.debug(`${this._logPrefix} : setCurrentExportType`, { selected });
    const selectedItem = this.exportsTypes.find((i) => i.key === selected) || this.exportsTypes[0];
    this.currentExportType = selectedItem;
    this.setDefaultExportFormat();
  }

  protected setCurrentExportFormat(selected?:string) {
    if (this.debug) console.debug(`${this._logPrefix} : setCurrentExportFormat`, { selected });
    this.currentExportFormat = this.currentExportType.formats.find((format) => format.key == selected);
  }

}
