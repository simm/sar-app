import { Component, Input, OnInit } from '@angular/core';
import { BtnMenuActionItem } from './btn-menu-action.model';

@Component({
  selector: 'btn-menu-action',
  templateUrl: './btn-menu-action.component.html',
})
export class BtnMenuActionComponent implements OnInit {

  @Input() i18nPrefix:string = 'COMMON.';
  @Input() btnLabel?: string;
  @Input() btnIcon?: string;
  @Input() isRoot = true;
  @Input() menuItems: BtnMenuActionItem[];

  constructor() { }

  ngOnInit(): void {
  }

  isExpandable(item: BtnMenuActionItem): boolean {
    return !!item.childrens;
  }

}
