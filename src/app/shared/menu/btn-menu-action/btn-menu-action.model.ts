export declare type BtnMenuActionItem = {
  label: string,
  extrasParams?: any,
  childrens?: BtnMenuActionItem[],
  // TODO Type extraParams
  action?: (event?: UIEvent, extrasParams?: any) => void,
}
