import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AppCoreModule } from '@app/core/core.module';
import { SharedPipesModule } from '@sumaris-net/ngx-components';
import { BtnMenuActionComponent } from './btn-menu-action.component';



@NgModule({
  declarations: [
    BtnMenuActionComponent,
  ],
  imports: [
    CommonModule,
    AppCoreModule,
    SharedPipesModule,
  ],
  exports: [
    BtnMenuActionComponent,
  ],
})
export class BtnMenuActionModule { }
