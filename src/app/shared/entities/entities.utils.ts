import { TranslateService } from '@ngx-translate/core';
import {changeCaseToUnderscore, FilesUtils, FormFieldDefinition, FormFieldType, isNotNil, PropertyFormatPipe} from '@sumaris-net/ngx-components';

export declare type EntityFieldType = FormFieldType | 'uri' | 'validitystatus';

export function getFieldType(key: string): EntityFieldType {
  const lowerKey = key.toLowerCase();
  if (lowerKey.endsWith('date')) return 'date';
  if (lowerKey.endsWith('uri')) return 'uri';
  if (lowerKey.endsWith('label') || lowerKey.endsWith('name') || lowerKey.endsWith('code')) return 'string';
  if (lowerKey === 'measurement' || lowerKey.endsWith('longitude') || lowerKey.endsWith('latitude')) return 'double';
  return 'string';
}

// TODO Type params
export function getEntityFieldDefinitions(propertyNames: string[], i18nPrefix: string): FormFieldDefinition[] {
  const result = propertyNames.map(key => <FormFieldDefinition>{
    key,
    type: getFieldType(key),
    label: (i18nPrefix) + changeCaseToUnderscore(key).toUpperCase()
  });
  return result;
}

// TODO Type params
export async function exportToCSV(
  datas: {[key:string]:any}[],
  fieldsToExports: string[],
  i18nPrefix: string,
  translate: TranslateService,
  filename: string,
  formater: PropertyFormatPipe,
  opts?: any,
) {
  const eofChar = '\n';

  const separatorKey = 'FILE.CSV.SEPARATOR';
  let separator = translate.instant(separatorKey);
  if (separator === separatorKey) separator = ';';

  const encodingKey = 'FILE.CSV.ENCODING';
  let encoding = translate.instant(encodingKey);
  if (encoding == encodingKey) encoding = 'UTF-8';

  const fieldDef = getEntityFieldDefinitions(fieldsToExports, i18nPrefix);
  const headers = fieldDef
    .map((field) => `"${translate.instant(field.label)}"`)
    .join(separator);

  const rows = await Promise.all(
    datas.map(async (data) => {
      const result = await Promise.all(
        fieldDef.map(async (def) => {
          // TODO This is very tricky
          const result = await formater.transform(data, def);
          // escape quotes and wrap text field into quote
          if (!['integer', 'double'].includes(def.type)) {
            if (isNotNil(result))
              return `"${result.replace(/[^\\]"+/g, '\\"')}"`;
          }
          return result;
        })
      );
      return result.join(separator);
    })
  );

  const textContent = headers + eofChar + rows.join(eofChar);
  FilesUtils.writeTextToFile(
    textContent, {
      type: 'text/csv',
      filename: filename,
      ...opts,
    }
  );
}
