export function getCountNbOfPages(totalOfItems: number, nbItemPeerPage:number):number {
    if (totalOfItems <= nbItemPeerPage) return 1;
    const nbPages = totalOfItems/nbItemPeerPage;
    const truncNbPages = Math.trunc(nbPages);
    if (nbPages === truncNbPages) return nbPages;
    else return truncNbPages+1;
}
