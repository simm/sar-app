import { AfterViewInit, Directive, ElementRef, Inject, Input, OnDestroy, Renderer2 } from '@angular/core';
import { environment } from '@environments/environment';
import { ENVIRONMENT } from '@sumaris-net/ngx-components';
import { interval, Subscription } from 'rxjs';

@Directive({
  selector: '[autoFixedHeight]',
})
export class AutoFixedHeight implements AfterViewInit, OnDestroy {

  private _logPrefix = '[auto-fixed-height]'
  protected previousElementHeight: number = 0;
  protected $checkLoop: Subscription;
  protected debug:boolean = false;
  @Input() autoFixedHeightElementsToSubstituteOffsetHeight: ElementRef[] = [];
  @Input() availableHeight: number = 0;
  @Input() autoFixedHeightCheckInterval: number = 250;

  constructor(
    private _element: ElementRef,
    private _render: Renderer2,
    @Inject(ENVIRONMENT) environment,
  ) {
    this.debug = !environment.production;
    if (this.debug) console.debug(`${this._logPrefix} : construct`, { _element })
  }

  ngAfterViewInit(): void {
    if (this.debug) console.debug(`${this._logPrefix} : ngAfterViewInit`, {
      autoFixedHeightElementsToSubstituteOffsetHeight: this.autoFixedHeightElementsToSubstituteOffsetHeight,
      elementTopPosition: this.availableHeight,
      autoFixedHeightCheckInterval: this.autoFixedHeightCheckInterval,
    })
    this.$checkLoop = interval(this.autoFixedHeightCheckInterval).subscribe((_) => {
      const heightOfAllElementsToSubstitute = this.autoFixedHeightElementsToSubstituteOffsetHeight
        .map((elem) => {
          return elem.nativeElement.offsetHeight
        })
        .reduce((p, n) => (p + n));
      const elementHeight = this.availableHeight - heightOfAllElementsToSubstitute;
      if (this.previousElementHeight !== elementHeight) {
        if (this.debug) console.debug(`${this._logPrefix} : update element height`, {
          element: this._element,
          previousElementHeight: this.previousElementHeight,
          elementHeight,
        });
        this.previousElementHeight = elementHeight;
        this._render.setStyle(this._element.nativeElement, 'height', `${elementHeight}px`)
      }
    });
  }

  ngOnDestroy(): void {
    if (this.debug) console.debug(`${this._logPrefix} : ngOnDestroy`)
    this.$checkLoop.unsubscribe();
  }

}
