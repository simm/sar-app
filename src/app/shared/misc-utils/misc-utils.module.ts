import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AutoFixedHeight } from './misc-utils.directive';

@NgModule({
  declarations: [
    AutoFixedHeight,
  ],
  exports: [
    AutoFixedHeight,
  ],
  imports: [
    CommonModule
  ]
})
export class MiscUtilsModule { }
