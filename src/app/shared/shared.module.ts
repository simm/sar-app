import {ModuleWithProviders, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {Environment, SharedModule, ResizableModule} from '@sumaris-net/ngx-components';

@NgModule({
  imports: [
    SharedModule,
    ResizableModule
  ],
  declarations: [
  ],
  exports: [
    SharedModule,
    RouterModule,
    TranslateModule,
    ResizableModule
  ]
})
export class AppSharedModule {

  static forRoot(environment: Environment): ModuleWithProviders<AppSharedModule> {

    console.info('[app-shared] Creating module (root)');
    return {
      ngModule: AppSharedModule,
      providers: [
        ...SharedModule.forRoot(environment).providers
      ]
    };
  }
}
