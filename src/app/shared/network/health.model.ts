export interface NodeHealthResult {
  status?: 'UP' | string;
  components?: {
    [key: string]: {
      status?: 'UP' | string;
      details?: Record<string, any>;
    };
  };
}
