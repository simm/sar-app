import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {SharedRoutingModule} from '@sumaris-net/ngx-components';
import {AppHubEauModule} from '@app/hubeau/hubeau.module';
import {ContaminantTable} from '@app/hubeau/contaminant/contaminant.table';
import {MonitoringLocationTable} from '@app/hubeau/monitoring-location/monitoring-location.table';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'contaminant'
  },
  {
    path: 'contaminant',
    component: ContaminantTable
  },
  {
    path: 'monitoringLocation',
    component: MonitoringLocationTable
  }
];

@NgModule({
  imports: [
    SharedRoutingModule,
    AppHubEauModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class HubEauRoutingModule {
}
