import {FetchMoreFn} from '@sumaris-net/ngx-components';

export const LocationLevelIds = {
  HUBEAU_MONITORING_LOCATION: 3,
};

export declare interface HubEauResult<T> {
  page: HubEauResultPage<T>;
  errors?: any[];
  fetchMore?: FetchMoreFn<HubEauResult<T>>;
}

export declare interface HubEauResultPage<T> {
  apiVersion?: string;
  first?: string;
  last?: string;
  next?: string;
  prev?: string;

  download?: string;
  count?: number;

  data: T[];
}
