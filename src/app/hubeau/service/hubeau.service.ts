import {Directive} from '@angular/core';
import {
  BaseGraphqlService,
  BaseGraphqlServiceOptions,
  ConfigService,
  Configuration,
  EntitiesServiceWatchOptions,
  Entity,
  EntityFilter,
  EntityServiceLoadOptions,
  GraphqlService,
  IEntitiesService,
  LoadResult
} from '@sumaris-net/ngx-components';
import {SortDirection} from '@angular/material/sort';
import {Observable, Subject} from 'rxjs';
import {environment} from '@environments/environment';
import {first, map} from 'rxjs/operators';
import {HubEauResult} from '@app/hubeau/service/hubeau.model';
import {HubEauErrorCodes} from '@app/hubeau/service/hubeau.errors';

export interface HubEauBaseServiceQueries {
  loadAll: any;
}

export interface HubEauBaseServiceOptions extends BaseGraphqlServiceOptions {
  queries: HubEauBaseServiceQueries;
  defaultSortBy: string;
  defaultSortDirection: SortDirection;
}
export interface HubEauBaseServiceWatchOptions extends EntitiesServiceWatchOptions {
  query?: any;
}
export interface HubEauBaseServiceLoadOptions extends EntityServiceLoadOptions {
  query?: any;
}
@Directive()
export abstract class HubEauBaseService<T extends Entity<T, ID>,
  F extends EntityFilter<F, T, ID>,
  ID = number,
  WO extends HubEauBaseServiceWatchOptions = HubEauBaseServiceWatchOptions,
  LO extends HubEauBaseServiceLoadOptions = HubEauBaseServiceLoadOptions
  >
  extends BaseGraphqlService<T, F, ID>
  implements IEntitiesService<T, F, WO>{

  protected logTypeName: string = null;
  protected queries: HubEauBaseServiceQueries = null;
  protected defaultSortBy: string = null;
  protected defaultSortDirection: SortDirection = null;

  protected constructor(
    graphql: GraphqlService,
    protected configService: ConfigService,
    protected dataType: new () => T,
    protected filterType: new () => F,
    options: HubEauBaseServiceOptions
  ) {
    super(
      graphql,
      {
        production: environment.production,
        ...options
      });

    this.queries = options?.queries;
    this._logPrefix = '[hubeau-base-service] ';
    this.logTypeName = new this.dataType().__typename;

    this.start();
  }

  watchAll(offset: number, size: number, sortBy?: string, sortDirection?: SortDirection, filter?: any, opts?: WO): Observable<LoadResult<any>> {

    filter = this.asFilter(filter);

    const variables: any = {
      offset: offset || 0,
      size: size || 100,
      sortBy: sortBy || this.defaultSortBy,
      sortDirection: sortDirection || this.defaultSortDirection,
      filter: filter && filter.asPodObject()
    };

    let now = this._debug && Date.now();
    if (this._debug) console.debug(this._logPrefix + `Watching ${this.logTypeName}...`, variables);

    const query = (opts && opts.query) || this.queries.loadAll;
    return this.graphql.watchQuery<HubEauResult<any>>({
      query,
      variables,
      error: {code: HubEauErrorCodes.LOAD_DATA_ERROR, message: 'HUBEAU.ERROR.LOAD_DATA_ERROR'},
      fetchPolicy: opts && opts.fetchPolicy || 'network-only'
    })
      .pipe(
        map(({page}) => {
          const total = page.count;
          // Convert to entity (if need)
          const entities = (!opts || opts.toEntity !== false)
            ? (page.data || []).map(json => this.fromObject(json))
            : (page.data || []) as T[];

          if (now) {
            console.debug(this._logPrefix + `${this.logTypeName} loaded in ${Date.now() - now}ms`, entities);
            now = null;
          }

          const res: LoadResult<T> = {data: entities, total};

          // Add fetch more
          const nextOffset = variables.offset + variables.size;
          if (nextOffset < total) {
            res.fetchMore = () => this.loadAll(nextOffset, size, sortBy, sortDirection, filter, {...opts, fetchPolicy: 'network-only' } as unknown as LO);
          }

          return res;
        })
      );
  }

  async loadAll(offset: number, size: number, sortBy?: string, sortDirection?: SortDirection, filter?: any, opts?: LO): Promise<LoadResult<any>> {

    filter = this.asFilter(filter);

    const variables: any = {
      offset: offset || 0,
      size: size || 100,
      sortBy: sortBy || this.defaultSortBy,
      sortDirection: sortDirection || this.defaultSortDirection,
      filter: filter && filter.asPodObject()
    };

    let now = this._debug && Date.now();
    if (this._debug) console.debug(this._logPrefix + `Loading ${this.logTypeName}...`, variables);

    const query = (opts && opts.query) || this.queries.loadAll;
    const {page} = await this.graphql.query<HubEauResult<any>>({
      query,
      variables,
      error: {code: HubEauErrorCodes.LOAD_DATA_ERROR, message: 'HUBEAU.ERROR.LOAD_DATA_ERROR'},
      fetchPolicy: opts && opts.fetchPolicy || 'network-only'
    });
    const total = page.count;
    // Convert to entity (if need)
    const entities = (!opts || opts.toEntity !== false)
      ? (page.data || []).map(json => this.fromObject(json))
      : (page.data || []) as T[];

    if (now) {
      console.debug(this._logPrefix + `${this.logTypeName} loaded in ${Date.now() - now}ms`, entities);
      now = null;
    }

    const res: LoadResult<T> = {data: entities, total};

    // Add fetch more
    const nextOffset = variables.offset + variables.size;
    if (nextOffset < total) {
      console.debug(this._logPrefix + 'Fetch more page offset: ' + nextOffset);
      res.fetchMore = () => this.loadAll(nextOffset, size, sortBy, sortDirection, filter, {...opts, fetchPolicy: 'network-only' } as unknown as LO);
    }

    return res;
  }

  async suggest(value: any, filter?: Partial<F>): Promise<LoadResult<T>> {
    if (value && typeof value === 'object') return {data: [value]};
    value = (typeof value === 'string' && value !== '*') && value || undefined;
    return this.loadAll(0, !value ? 30 : 10, undefined, undefined,
      {
        ...filter,
        searchText: value as string
      }
    );
  }

  saveAll(data: T[], opts?: any): Promise<T[]> {
    throw new Error('Not implemented!');
  }

  deleteAll(data: T[], opts?: any): Promise<any> {
    throw new Error('Not implemented!');
  }

  asFilter(source: Partial<F>): F {
    const target = new this.filterType();
    if (source) target.fromObject(source);
    return target;
  }

  fromObject(source: any, opts?: any): T {
    const target = new this.dataType();
    target.fromObject(source, opts);
    return target;
  }

  protected async ngOnStart(): Promise<void> {

    await super.ngOnStart();
    console.log(this._logPrefix + 'ngOnStart()');
    const $ready = new Subject<void>();

    this.registerSubscription(
      this.configService.config.subscribe(config => {
        this.updateModelEnumerations(config);
        if (!$ready.isStopped) {
          $ready.next();
          $ready.complete();
        }
      })
    );

    await $ready.pipe(first()).toPromise();

    console.log(this._logPrefix + 'ngOnStart() [OK]');
  }

  protected updateModelEnumerations(config: Configuration) {
    // Can be override
  }
}
