import {Directive} from '@angular/core';
import {BaseGraphqlService, BaseGraphqlServiceOptions, EntitiesServiceWatchOptions, Entity, EntityFilter, ErrorCodes, GraphqlService, IEntitiesService, LoadResult} from '@sumaris-net/ngx-components';
import {SortDirection} from '@angular/material/sort';
import {Observable} from 'rxjs';
import {environment} from '@environments/environment';
import {map, switchMap} from 'rxjs/operators';
import {HubEauResult} from '@app/hubeau/service/hubeau.model';
import {HubEauErrorCodes} from '@app/hubeau/service/hubeau.errors';
import {gql} from '@apollo/client/core';

export const HubEauFragments = {
  pageMeta: `
    apiVersion
    count
    next
    prev
    first
    last
  }`
};
