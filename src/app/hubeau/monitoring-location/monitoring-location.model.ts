import {EntityAsObjectOptions, EntityClass, Entity, EntityFilter, fromDateISOString, ReferentialRef, toDateISOString} from '@sumaris-net/ngx-components';
import {Moment} from 'moment';


@EntityClass({typename: 'MonitoringLocationVO'})
export class MonitoringLocation extends Entity<MonitoringLocation, string, EntityAsObjectOptions, any> {
  static fromObject: (source: any, opts?: any) => MonitoringLocation;

  code: string = null;
  label: string = null;
  name: string = null;
  utFormat: number = null;
  bathymetry: number = null;
  creationDate: Moment = null;
  updateDate: Moment = null;
  comment: string = null;

  // From programs
  startDate: Moment = null;
  endDate: Moment = null;
  programCodes: string[] = null;
  programNames: string[] = null;
  programUris: string[] = null;
  taxonCodes: string[] = null;
  taxonNames: string[] = null;
  taxonUris: string[] = null;

  constructor() {
    super(MonitoringLocation.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    Object.assign(this, source);
    super.fromObject(source);
    this.creationDate = fromDateISOString(this.creationDate);
    this.startDate = fromDateISOString(this.startDate);
    this.endDate = fromDateISOString(this.endDate);
  }
}


@EntityClass({ typename: 'MonitoringLocationFilterVO' })
export class MonitoringLocationFilter extends EntityFilter<MonitoringLocationFilter, MonitoringLocation, string> {
  static fromObject: (source: any, opts?: any) => MonitoringLocationFilter;

  hasContaminantData = false;
  dataDate: Moment = null;
  minUpdateDate: Moment = null;
  maxUpdateDate: Moment = null;

  constructor() {
    super(MonitoringLocationFilter.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.hasContaminantData = source.hasContaminantData;
    this.dataDate = fromDateISOString(source.dataDate);
    this.minUpdateDate = fromDateISOString(source.minUpdateDate);
    this.maxUpdateDate = fromDateISOString(source.maxUpdateDate);
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.dataDate = toDateISOString(this.dataDate);
    target.minUpdateDate = toDateISOString(this.minUpdateDate);
    target.maxUpdateDate = toDateISOString(this.maxUpdateDate);
    return target;
  }
}
