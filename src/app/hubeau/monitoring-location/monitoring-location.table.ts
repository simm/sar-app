import {AfterViewInit, ChangeDetectionStrategy, Component, Injector, OnInit} from '@angular/core';
import {slideUpDownAnimation} from '@sumaris-net/ngx-components';
import {MonitoringLocation, MonitoringLocationFilter} from '@app/hubeau/monitoring-location/monitoring-location.model';
import {MonitoringLocationService} from '@app/hubeau/monitoring-location/monitoring-location.service';
import {ReadonlyBaseTable} from '@app/shared/table/readonly-base.table';

@Component({
  selector: 'app-monitoring-location-table',
  templateUrl: './monitoring-location.table.html',
  styleUrls: ['./monitoring-location.table.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [slideUpDownAnimation]
})
export class MonitoringLocationTable
  extends ReadonlyBaseTable<MonitoringLocation, string, MonitoringLocationFilter>
  implements OnInit, AfterViewInit {

  constructor(
    protected injector: Injector,
    protected entityService: MonitoringLocationService
  ) {
    super(
      injector,
      MonitoringLocation,
      MonitoringLocationFilter,
      entityService,
      {
        i18nColumnPrefix: 'HUBEAU.MONITORING_LOCATION.'
      }
    );

    this.logPrefix = '[monitoring-location-table] ';
    this.enableInfiniteScroll = false;
    this.sticky = true;
    console.debug(this.logPrefix + 'Creating table...');
  }

  ngOnInit() {
    super.ngOnInit();

    this.markAsReady();
  }
}
