import {Injectable} from '@angular/core';
import {BaseEntityGraphqlQueries, ConfigService, Configuration, GraphqlService, LoadResult, SuggestService} from '@sumaris-net/ngx-components';
import {MonitoringLocation, MonitoringLocationFilter} from '@app/hubeau/monitoring-location/monitoring-location.model';
import {gql} from '@apollo/client/core';
import {environment} from '@environments/environment';
import {HubEauBaseService} from '@app/hubeau/service/hubeau.service';
import {LocationLevelIds} from '@app/hubeau/service/hubeau.model';
import {HUBEAU_CONFIG_OPTIONS} from '@app/hubeau/config/hubeau.config';

const fragments = {
  full: gql`fragment FullMonLocFragment on MonitoringLocationVO {
    id: code
    code
    label
    name
    utFormat
    bathymetry
    comment
    creationDate
    updateDate
    startDate
    endDate
    programCodes
    programNames
    programUris
    taxonCodes
    taxonNames
    taxonUris
  }`,

  light: gql`fragment LightMonLocFragment on MonitoringLocationVO {
    code
    label
    name
  }`
};

const queries: BaseEntityGraphqlQueries & { loadAllForSuggest: any} = {
  loadAll: gql`query FullMonLocs($offset: Int, $size: Int, $sortBy: String, $sortDirection: String, $filter: MonitoringLocationFilterVOInput) {
      page: monitoringLocations(offset: $offset, size: $size, sortBy: $sortBy, sortDirection: $sortDirection, filter: $filter) {
        count
        data {
          ...FullMonLocFragment
        }
      }
    }
    ${fragments.full}`,

  loadAllForSuggest: gql`query MonLocs($offset: Int, $size: Int, $sortBy: String, $sortDirection: String, $filter: MonitoringLocationFilterVOInput) {
    page: monitoringLocations(offset: $offset, size: $size, sortBy: $sortBy, sortDirection: $sortDirection, filter: $filter) {
      count
      data {
        ...LightMonLocFragment
      }
    }
  }
  ${fragments.light}`
};

@Injectable({providedIn: 'root'})
export class MonitoringLocationService
  extends HubEauBaseService<MonitoringLocation, MonitoringLocationFilter, string>
  implements SuggestService<MonitoringLocation, MonitoringLocationFilter> {

  constructor(graphql: GraphqlService,
              configService: ConfigService) {
    super(
      graphql,
      configService,
      MonitoringLocation,
      MonitoringLocationFilter,
      {
        queries,
        defaultSortBy: 'code',
        defaultSortDirection: 'asc',
        production: environment.production
      });

    this._logPrefix = '[monitoring-location-service] ';
  }

  async suggest(value: any, filter?: Partial<MonitoringLocationFilter>): Promise<LoadResult<MonitoringLocation>> {
    if (value && typeof value === 'object') return {data: [value]};
    value = (typeof value === 'string' && value !== '*') && value || undefined;
    return this.loadAll(0, !value ? 30 : 10, undefined, undefined,
      {
        ...filter,
        searchText: value as string,
        query: queries.loadAllForSuggest
      }
    );
  }

  /* -- protected functions-- */

  protected updateModelEnumerations(config: Configuration) {
    if (!config.properties) return;

    console.info('[monitoring-location-service] Updating model enumerations...');

    // Location Levels
    LocationLevelIds.HUBEAU_MONITORING_LOCATION = +config.getProperty(HUBEAU_CONFIG_OPTIONS.HUBEAU_MONITORING_LOCATION_LEVEL_ID);

  }
}
