
export interface IReferential<ID = number> {
  id: ID;
  label?: string;
  name: string;
}

export const ContaminantParameters: IReferential<number>[] = [
  {name: 'Aluminium', id: 1370, label: '1370'},
  {name: 'Argent', id: 1368, label: '1368'},
  {name: 'Cadmium', id: 1388, label: '1388'},
  {name: 'Chrome', id: 1389, label: '1389'},
  {name: 'Chrome hexavalent', id: 1371, label: '1371'},
  {name: 'Cuivre', id: 1392, label: '1392'},
  {name: 'Etain', id: 1380, label: '1380'},
  {name: 'Mercure', id: 1387, label: '1387'},
  {name: 'Nickel', id: 1386, label: '1386'},
  {name: 'Plomb', id: 1382, label: '1382'},
  {name: 'Zinc', id: 1383, label: '1383'}
];
