import {FormFieldDefinition, StatusIds} from '@sumaris-net/ngx-components';
import {LocationLevelIds} from '@app/hubeau/service/hubeau.model';

export const HUBEAU_CONFIG_OPTIONS = Object.freeze({
  HUBEAU_MONITORING_LOCATION_LEVEL_ID: <FormFieldDefinition>{
    key: 'sumaris.enumeration.LocationLevel.HUBEAU_MONITORING_LOCATION.id',
    label: 'sumaris.enumeration.LocationLevel.HUBEAU_MONITORING_LOCATION.id',
    type: 'entity',
    autocomplete: {
      filter: {
        entityName: 'LocationLevel',
        statusIds: [StatusIds.DISABLE, StatusIds.ENABLE]
      }
    },
    defaultValue: LocationLevelIds.HUBEAU_MONITORING_LOCATION
  }
});
