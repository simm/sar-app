import {AfterViewInit, ChangeDetectionStrategy, Component, Injector, OnInit, ViewChild} from '@angular/core';
import {
  Alerts,
  Color,
  ColorScale,
  hexToRgbArray,
  isNotNil,
  LoadResult,
  MatChipsField,
  ReferentialRef,
  referentialsToString,
  referentialToString,
  ReferentialUtils,
  slideUpDownAnimation,
  suggestFromArray
} from '@sumaris-net/ngx-components';
import {Contaminant, ContaminantFilter, Group} from '@app/hubeau/contaminant/contaminant.model';
import {ContaminantService, ContaminantServiceWatchOptions} from '@app/hubeau/contaminant/contaminant.service';
import {FormBuilder} from '@angular/forms';
import {TableElement} from '@e-is/ngx-material-table';
import {MonitoringLocationFilter} from '@app/hubeau/monitoring-location/monitoring-location.model';
import {MonitoringLocationService} from '@app/hubeau/monitoring-location/monitoring-location.service';
import {ContaminantParameters, IReferential} from '@app/hubeau/parameter/parameter.utils';
import {capitalize} from '@angular-devkit/core/src/utils/strings';
import {ReadonlyBaseTable} from '@app/shared/table/readonly-base.table';

interface IReferentialColor extends IReferential {
  color: Color,
}

@Component({
  selector: 'app-contaminant-table',
  templateUrl: './contaminant.table.html',
  styleUrls: ['./contaminant.table.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [slideUpDownAnimation]
})
export class ContaminantTable
  extends ReadonlyBaseTable<Contaminant, string, ContaminantFilter>
  implements OnInit, AfterViewInit {

  static addColor(items: IReferential[], colors?: {primary?: string; tertiary?: string; danger?: string}): IReferentialColor[] {
    const startColor = new Color(hexToRgbArray(colors?.primary || '#315da8')).rgb;
    const mainColor = new Color(hexToRgbArray(colors?.tertiary || '#37b9c6')).rgb;
    const endColor = new Color(hexToRgbArray(colors?.danger || '#f53d3d')).rgb;
    const colorScales = ColorScale.custom(items.length, {
      startColor,
      mainColor: items.length > 2 ? mainColor : undefined,
      mainColorIndex: Math.round(items.length / 2),
      endColor
    });
    return items.map((item, i) => ({
      ...item,
      color: colorScales.getValueColor(i)
    }));
  }

  contaminants: IReferentialColor[];
  groupByProperties = ['monLocCode', 'monLocLabel', 'monLocName'];
  displayGroupAttributes: string[];
  title: string;
  enableGroupBy = true;

  @ViewChild('filterParameterField', {static: true}) filterParameterField : MatChipsField;
  @ViewChild('filterMonitoringLocationField', {static: true}) filterMonitoringLocationField : MatChipsField;

  constructor(
    protected injector: Injector,
    protected entityService: ContaminantService,
    protected formBuilder: FormBuilder,
    protected monitoringLocationService: MonitoringLocationService
  ) {
    super(
      injector,
      Contaminant,
      ContaminantFilter,
      entityService,
      {
        i18nColumnPrefix: 'HUBEAU.CONTAMINANT.',
        watchAllOptions: <ContaminantServiceWatchOptions>{
          fetchPolicy: 'no-cache',
          groupByProperties: ['monLocCode', 'monLocLabel', 'monLocName'],
          aggProperties: ['parameterCode']
        }
      }
    );

    this.logPrefix = '[contaminant-table] ';
    this.enableInfiniteScroll = false; // FIXME true;
    this.sticky = true;

    this.contaminants = ContaminantTable.addColor(ContaminantParameters);
    console.debug(this.logPrefix + 'Creating table...');

  }

  ngOnInit() {

    super.ngOnInit();
    const parameterAttributes = this.settings.getFieldDisplayAttributes('parameter', ['label', 'name']);
    this.registerAutocompleteField('parameter', {
      showAllOnFocus: true,
      suggestFn:  (value, filter) => this.suggestParameters(value, filter),
      attributes: parameterAttributes,
      columnSizes: parameterAttributes.map(key => key === 'label' ? 3 : 'auto'),
      displayWith: (arg) => {
        if (Array.isArray(arg)) {
          return referentialsToString(arg, ['name']);
        }
        return referentialToString(arg, ['name']);
      },
      mobile: this.mobile
    });

    // Force recompute items, when changes
    this.registerSubscription(
      this.filterForm.get('parameter').valueChanges
        .subscribe(() => this.filterParameterField.reloadItems())
    );

    const monLocAttributes = this.settings.getFieldDisplayAttributes('monitoringLocation', ['label', 'name']);
    this.displayGroupAttributes = monLocAttributes.map(attr => 'id.monLoc' + capitalize(attr));
    this.registerAutocompleteField('monitoringLocation', {
      showAllOnFocus: true,
      service: this.monitoringLocationService,
      filter: <MonitoringLocationFilter>{
        hasContaminantData: true
      },
      /*suggestFn: (value, filter) => this.referentialRefService.suggest(value, {
        entityName: 'Location',
        levelId: LocationLevelIds.HUBEAU_MONITORING_LOCATION,
        ...filter
      }),*/
      attributes: monLocAttributes,
      columnSizes: parameterAttributes.map(key => key === 'label' ? 4 : 'auto'),
      displayWith: (arg) => {
        if (Array.isArray(arg)) {
          return referentialsToString(arg, ['label']);
        }
        return referentialToString(arg, ['label']);
      },
      mobile: this.mobile
    });

    // Force recompute items, when changes
    this.registerSubscription(
      this.filterForm.get('monitoringLocation').valueChanges
        .subscribe(() => this.filterParameterField.reloadItems())
    );

    this.markAsReady();
  }

  isGroup(index, row: TableElement<Contaminant>) {
    return row.currentData instanceof Group;
  }

  isDetails(index, row: TableElement<Contaminant>) {
    return row.currentData instanceof Contaminant;
  }

  toggleCompactMode() {
    this.enableGroupBy = !this.enableGroupBy;
    if (this.enableGroupBy) {
      this.dataSource.serviceOptions.groupByProperties = this.groupByProperties;
    }
    else {
      this.dataSource.serviceOptions.groupByProperties = undefined;
    }
    this.onRefresh.emit();
  }

  appendToFilter(filterFieldName: string, item: IReferential) {

    console.debug(`Add ${filterFieldName} value: ${item.id}`);
    const filter = this.filterForm.value;


    const value: IReferential | IReferential[] = filter[filterFieldName] || null;
    let updatedValue: IReferential | IReferential[] = null;
    if (!value) {
      updatedValue = [item];
    }
    else if (Array.isArray(value)) {
      if (!value.some(v => ReferentialUtils.equals(v, item))) {
        updatedValue = value.concat(item);
      }
    }
    else if (!ReferentialUtils.equals(value, item)) {
      updatedValue = [value, item];
    }

    // Applying the new value
    if (updatedValue) {
      if (this.filterExpansionPanel.closed) {
        this.filterExpansionPanel.open();
      }
      filter[filterFieldName] = updatedValue;
      this.setFilter(filter);
    }
  }

  downloadAll(event: UIEvent) {
    if (this.loading || this.totalRowCount === 0) return;

    if (event) {
      event.stopPropagation();
      event.preventDefault();
    }

    if (this.totalRowCount >= ContaminantService.MAX_DOWNLOAD_ROW_COUNT /* = API limit*/) {
      return Alerts.showError(this.i18nColumnPrefix + 'ERROR.TOO_MANY_ROWS_TO_DOWNLOAD',
        this.alertCtrl, this.translate, null, {
          maxCount: ContaminantService.MAX_DOWNLOAD_ROW_COUNT
        });
    }

    return this.downloadByFilter(event, this.filter);
  }

  downloadGroup(event: UIEvent, group: Group<any>) {
    const idProperty = this.groupByProperties
      .find(key => key.toLowerCase().endsWith('code') || key.toLowerCase().endsWith('id'));
    const location = idProperty && ReferentialRef.fromObject({
      id: group.id[idProperty]
    });
    if (!location) return; // No ID found
    const customFilter = {
      ...this.filter,
      // Replace mon loc by the selected group
      monitoringLocation: location
    };
    return this.downloadByFilter(event, customFilter);
  }

  /* -- protected functions -- */

  protected async downloadByFilter(event: UIEvent, filter: Partial<ContaminantFilter>) {
    if (this.loading) return;

    if (event) {
      event.stopPropagation();
      event.preventDefault();
    }

    console.debug(`[contaminant-table] Downloading...`);

    this.markAsLoading();
    this.resetError();
    this.disable();

    try {
      // Download file
      const uri = await this.entityService.getDownloadUrl(filter);
      if (isNotNil(uri)) {
        await this.platform.download({uri});
      }
    } catch (err) {
      console.error(err);
      this.setError(err && err.message || err);
    } finally {
      this.markAsLoaded();
      this.enable();
    }
  }

  protected async suggestParameters(value: any, filter?: any): Promise<LoadResult<IReferential>> {

    const excludedIds = (this.filterForm.get('parameter').value || [])
      .map(p => p.id);

    return suggestFromArray(this.contaminants, value, {
      ...filter,
      excludedIds
    });
  }
}
