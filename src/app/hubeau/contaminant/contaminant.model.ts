import {Entity, EntityAsObjectOptions, EntityClass, EntityFilter, fromDateISOString, ObjectMap, ReferentialRef, toDateISOString} from '@sumaris-net/ngx-components';
import {Moment} from 'moment';


@EntityClass({typename: 'ContaminantVO'})
export class Contaminant extends Entity<Contaminant, string, EntityAsObjectOptions, any> {
  static fromObject: (source: any, opts?: any) => Contaminant;

  // Survey
  parameterCode: string = null;
  parameterName: string = null;
  matrixCode: string = null;
  matrixName: string = null;
  fractionCode: string = null;
  fractionName: string = null;
  methodCode: string = null;
  methodName: string = null;
  measurement: number = null;
  unitCode: string = null;
  unitName: string = null;
  surveyCode: string = null;
  surveyLabel: string = null;
  monLocCode: string = null;
  monLocName: string = null;
  monLocLabel: string = null;
  longitude: number = null;
  latitude: number = null;
  programCodes: string[] = null;
  programNames: string[] = null;
  programUris: string[] = null;
  analysisInstrumentCode: string = null;
  analysisInstrumentName: string = null;
  samplingOperationDate: Moment = null;
  samplingOperationLongitude: number = null;
  samplingOperationLatitude: number = null;
  samplingOperationComment: string = null;
  recorderDepartmentCode: string = null;
  recorderDepartmentName: string = null;
  sampleCode: string = null;
  sampleLabel: string = null;
  taxonCode: string = null;
  taxonName: string = null;
  departmentCode: string = null;
  departmentName: string = null;
  controlDate: Moment = null;
  validationDate: Moment = null;
  qualificationDate: Moment = null;
  qualificationCode: string = null;
  qualificationComment: string = null;
  comment: string = null;


  constructor(__typename?: string) {
    super(__typename || Contaminant.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source);
    Object.assign(this, source);
  }
}


@EntityClass({ typename: 'ContaminantFilterVO' })
export class ContaminantFilter extends EntityFilter<ContaminantFilter, Contaminant, string> {
  static fromObject: (source: any, opts?: any) => ContaminantFilter;

  parameter: ReferentialRef | ReferentialRef[] = null;
  monitoringLocation: ReferentialRef | ReferentialRef[] = null;
  startDate: Moment = null;
  endDate: Moment = null;

  constructor() {
    super(ContaminantFilter.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.parameter = Array.isArray(source.parameter)
      ? source.parameter.map(ReferentialRef.fromObject)
      : ReferentialRef.fromObject(source.parameter);
    this.monitoringLocation = Array.isArray(source.monitoringLocation)
      ? source.monitoringLocation.map(ReferentialRef.fromObject)
      : ReferentialRef.fromObject(source.monitoringLocation);
    this.startDate = fromDateISOString(source.startDate);
    this.endDate = fromDateISOString(source.endDate);
  }

  asObject(opts?: EntityAsObjectOptions): any {
    const target = super.asObject(opts);
    target.startDate = toDateISOString(this.startDate);
    target.endDate = toDateISOString(this.endDate);
    if (opts && opts.minify) {
      target.parameterCode = Array.isArray(this.parameter)
      ? this.parameter.map(p => p.id).join(',')
      : this.parameter?.id;
      target.monitoringLocationCode = Array.isArray(this.monitoringLocation)
        ? this.monitoringLocation.map(m => m.id).join(',')
        : this.monitoringLocation?.id;
      delete target.parameter;
      delete target.monitoringLocation;
      delete target.searchAttributes;
    }
    return target;
  }
}

@EntityClass({typename: 'GroupVO'})
export class Group<T extends Entity<T, ID, AO, FO>, ID = string, AO = EntityAsObjectOptions, FO = any>
  extends Entity<any, T> {

  static isGroup(object: any): object is Group<any> {
    return object && object.__typename === Group.TYPENAME;
  };

  children: T[] = [];
  aggregations: ObjectMap<GroupAggregation> = {};

  constructor(id: T, properties?: string[]) {
    super(Group.TYPENAME);

    // Clone source id
    this.id = (typeof id?.clone === 'function' && id.clone())
      || Object.assign({}, id);

    // Reset not included properties
    Object.keys(this.id)
      .filter(key => !properties || !properties.includes(key))
      .forEach(key => this.id[key] = null);
    /*(properties || [])
      .forEach(key => this[key] = id[key]);*/

  }

  addChild(child: T) {
    this.children.push(child);
  }

  getAgg(aggProperty: string, aggValue: string) {
    const agg = this.aggregations[aggProperty];
    return agg && agg.countByValue[aggValue];
  }
}

export class GroupAggregation {

  id: string = null;
  values: string[] = [];
  names: string[] = [];
  countByValue: ObjectMap<number> = {};
}
