import {Injectable} from '@angular/core';
import {BaseEntityGraphqlQueries, ConfigService, GraphqlService, isEmptyArray, isNil, isNotEmptyArray, LoadResult, ReferentialRef} from '@sumaris-net/ngx-components';
import {Contaminant, ContaminantFilter, Group, GroupAggregation} from '@app/hubeau/contaminant/contaminant.model';
import {gql} from '@apollo/client/core';
import {environment} from '@environments/environment';
import {HubEauBaseService, HubEauBaseServiceWatchOptions} from '@app/hubeau/service/hubeau.service';
import {SortDirection} from '@angular/material/sort';
import {Observable, Subject} from 'rxjs';
import {switchMap} from 'rxjs/operators';
import {ContaminantParameters} from '@app/hubeau/parameter/parameter.utils';


const fragments = {
  full: gql`fragment FullContaminantFragment on ContaminantVO {
    surveyCode
    surveyLabel
    monLocCode
    monLocName
    monLocLabel
    parameterCode
    parameterName
    matrixCode
    matrixName
    fractionCode
    fractionName
    methodCode
    methodName
    unitCode
    unitName
    programCodes
    programNames
    programUris
    analysisInstrumentCode
    analysisInstrumentName
    longitude
    latitude
    samplingOperationDate
    samplingOperationLongitude
    samplingOperationLatitude
    samplingOperationComment
    recorderDepartmentCode
    recorderDepartmentName
    sampleCode
    sampleLabel
    taxonCode
    taxonName
    departmentCode
    departmentName
    controlDate
    validationDate
    qualificationDate
    qualificationCode
    qualificationComment
    measurement
    comment
  }`,

  light: gql`fragment LightContaminantFragment on ContaminantVO {
    monLocCode
    monLocName
    monLocLabel
    parameterCode
    parameterName
  }`
};

const queries: BaseEntityGraphqlQueries & { loadAllForGroupBy: any; downloadLink: any } = {
  loadAll: gql`query Contaminants($offset: Int, $size: Int, $sortBy: String, $sortDirection: String, $filter: ContaminantFilterVOInput) {
      page: contaminants(offset: $offset, size: $size, sortBy: $sortBy, sortDirection: $sortDirection, filter: $filter) {
        count
        data {
          ...FullContaminantFragment
        }
      }
    }
  ${fragments.full}`,

  loadAllForGroupBy: gql`query ContaminantsForGroup($offset: Int, $size: Int, $sortBy: String, $sortDirection: String, $filter: ContaminantFilterVOInput) {
    page: contaminants(offset: $offset, size: $size, sortBy: $sortBy, sortDirection: $sortDirection, filter: $filter) {
      count
      data {
        ...LightContaminantFragment
      }
    }
  }
  ${fragments.light}`,

  downloadLink: gql`query ContaminantDownloadLink($filter: ContaminantFilterVOInput) {
    data: contaminants(size: 1, filter: $filter) {
      download
      data {
        ...FullContaminantFragment
      }
    }
  }
  ${fragments.full}`,
};

export interface GroupByOptions {
  groupByProperties?: (keyof Contaminant)[];
  aggProperties?: (keyof Contaminant)[];
}

export interface ContaminantServiceWatchOptions extends HubEauBaseServiceWatchOptions, GroupByOptions {
}

@Injectable({providedIn: 'root'})
export class ContaminantService
  extends HubEauBaseService<Contaminant, ContaminantFilter, string, ContaminantServiceWatchOptions> {

  static MAX_DOWNLOAD_ROW_COUNT = 20000;

  constructor(graphql: GraphqlService,
    configService: ConfigService) {
    super(
      graphql, configService,
      Contaminant, ContaminantFilter,
      {
        queries,
        defaultSortBy: 'surveyCode',
        defaultSortDirection: 'asc',
        production: environment.production
      });

    this._logPrefix = '[contaminant-service] ';
  }


  watchAll(offset: number, size: number, sortBy?: string, sortDirection?: SortDirection, filter?: Partial<ContaminantFilter>, opts?: ContaminantServiceWatchOptions): Observable<LoadResult<any>> {

    const hasGroupBy = isNotEmptyArray(opts?.groupByProperties);
    const query = hasGroupBy ? queries.loadAllForGroupBy : undefined;
    const fetchSize = hasGroupBy ? Math.max(100, size || 20) : size;

    const res$: Observable<LoadResult<Contaminant>> = super.watchAll(offset, fetchSize, sortBy, sortDirection, filter, {
      fetchPolicy: 'no-cache',
      query,
      ...opts
    });

    // Group by
    if (hasGroupBy) {
      return res$.pipe(switchMap(res => this.groupBy(res, fetchSize, size, opts)));
    }

    return res$;
  }

  async getDownloadUrl(filter?: Partial<ContaminantFilter>): Promise<string> {

    filter = this.asFilter(filter);

    const {data} = await this.graphql.query<{data: { download: string } }>({
      query: queries.downloadLink,
      variables: {
        filter: filter.asPodObject()
      }
    });

    return data?.download;
  }

  /* -- protected function -- */

  asFilter(source: Partial<ContaminantFilter>): ContaminantFilter {
    const target = super.asFilter(source);

    if (!target.parameter || (Array.isArray(target.parameter) && isEmptyArray(target.parameter))) {
      target.parameter = ContaminantParameters.map(ReferentialRef.fromObject);
    }

    return target;
  }

  protected groupBy(result: LoadResult<Contaminant>,
                    fetchSize: number, groupSize: number,
                    opts?: GroupByOptions): Observable<LoadResult<Contaminant>> {


    console.info(this._logPrefix + 'Grouping items, using options:', opts);
    const $result = new Subject<LoadResult<Contaminant>>();

    setTimeout(async () => {
      let groups = [];
      let items = result.data;
      let stop = false;
      let processedItemCount = 0;
      do {
        groups = this.groupByInternal(items, groups, opts);
        $result.next({
          data: groups,
          total: result.total
        });

        // Compute if can fetch more
        processedItemCount += items.length;
        const hasMoreData = !!result.fetchMore && result.data?.length === fetchSize;
        const needFetchMore = hasMoreData && groups.length < groupSize;

        // Fetching more items
        if (needFetchMore) {
          console.info(this._logPrefix + 'Fetching more...');
          result = await result.fetchMore();
          items = result.data;
        }
        stop = !needFetchMore || isEmptyArray(items);
      } while(!stop);

      $result.complete();
    });

    return $result.asObservable();
  }

  private groupByInternal(data: Contaminant[],
                          groups: Group<Contaminant>[],

                          opts?: GroupByOptions): Group<Contaminant>[] {
    const groupByProperties = opts?.groupByProperties || ['monLocCode', 'monLocLabel', 'monLocName'];
    const aggProperties = opts?.aggProperties || ['parameterCode'];

    const key = (item) => groupByProperties.map(key => item[key]).join('|').replace(/[ \t]+/g, '_');
    const groupIndexMap = groups.reduce((res, group, index) => {
      const groupKey = key(group.id);
      res[groupKey] = index;
      return res;
    }, {});

    return (data || []).reduce((groups, item) => {
      const groupKey = key(item);
      const groupIndex = groupIndexMap[groupKey];
      const isNewGroup = isNil(groupIndex);
      const group = !isNewGroup ? groups[groupIndex] : new Group<Contaminant>(item, groupByProperties);
      // New group: create divider
      if (isNewGroup) {
        console.debug(this._logPrefix + 'Creating new aggregation group: ' + groupKey);
        groupIndexMap[groupKey] = groups.length;
      }

      // Add current item to the group
      group.addChild(item);

      aggProperties.forEach(aggProperty => {
        const value = item[aggProperty] as unknown as any;
        if (value) {
          const aggKey = aggProperty.replace(/[ \t]+/g, '_');
          let agg = group.aggregations[aggKey];
          if (!agg) {
            agg = new GroupAggregation();
            agg.id = aggKey;
            group.aggregations[aggKey] = agg;
          }
          // Add values
          if (!agg.values.includes(value)) {
            agg.values.push(value);
            agg.countByValue[value] = 0;
            const nameProperty = aggProperty.endsWith('Code')
              ? aggProperty.substr(0, aggProperty.length-4) + 'Name'
              : aggProperty;
            const name = item[nameProperty] || value;
            agg.names.push(name);
          }
          // Increment count
          agg.countByValue[value] = agg.countByValue[value] + 1;
        }
      });

      return isNewGroup ? groups.concat(group) : groups;
    }, groups);
  }
}
