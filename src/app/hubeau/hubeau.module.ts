import {ModuleWithProviders, NgModule} from '@angular/core';
import {AppCoreModule} from '../core/core.module';
import {TranslateModule} from '@ngx-translate/core';
import {CommonModule} from '@angular/common';
import {ContaminantService} from '@app/hubeau/contaminant/contaminant.service';
import {CoreModule} from '@sumaris-net/ngx-components';
import {ContaminantTable} from '@app/hubeau/contaminant/contaminant.table';
import {IonicModule} from '@ionic/angular';
import {MonitoringLocationTable} from '@app/hubeau/monitoring-location/monitoring-location.table';
import {AppSharedModule} from '@app/shared/shared.module';
import {MatChipsModule} from '@angular/material/chips';
import {AppReferentialModule} from '@app/referential/referential.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    CoreModule,
    AppCoreModule,
    AppSharedModule,
    TranslateModule.forChild(),
    MatChipsModule,
    AppReferentialModule
  ],
  exports: [
    IonicModule,
    CommonModule,
    AppCoreModule,
    CoreModule,
    TranslateModule,
    ContaminantTable,
    MonitoringLocationTable
  ],
  declarations: [
    ContaminantTable,
    MonitoringLocationTable
  ]
})
export class AppHubEauModule {

  constructor() {
    console.debug('[app-hubeau-module] Creating module');
  }
}
