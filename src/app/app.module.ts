import './vendor';

import {APP_BASE_HREF} from '@angular/common';
import {BrowserModule, HAMMER_GESTURE_CONFIG, HammerModule} from '@angular/platform-browser';
import {CUSTOM_ELEMENTS_SCHEMA, NgModule, SecurityContext} from '@angular/core';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {IonicStorageModule} from '@ionic/storage';
import {IonicModule} from '@ionic/angular';
import {CacheModule} from 'ionic-cache';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {
  APP_ABOUT_DEVELOPERS,
  APP_ABOUT_PARTNERS,
  APP_CONFIG_OPTIONS,
  APP_GRAPHQL_TYPE_POLICIES,
  APP_HOME_BUTTONS,
  APP_LOCAL_SETTINGS,
  APP_LOCAL_SETTINGS_OPTIONS,
  APP_LOCALES,
  APP_MENU_ITEMS,
  APP_TESTING_PAGES,
  AppGestureConfig,
  CORE_CONFIG_OPTIONS,
  DATE_ISO_PATTERN,
  Department,
  FormFieldDefinitionMap,
  LocalSettings,
  MenuItem,
  SHARED_TESTING_PAGES,
  TestingPage
} from '@sumaris-net/ngx-components';
import {TypePolicies} from '@apollo/client';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {environment} from '@environments/environment';
import {REFERENTIAL_CONFIG_OPTIONS, REFERENTIAL_GRAPHQL_TYPE_POLICIES} from '@app/referential/services/config/referential.config';
import {AppSharedModule} from '@app/shared/shared.module';
import {AppCoreModule} from '@app/core/core.module';
import {JDENTICON_CONFIG} from 'ngx-jdenticon';
import {MarkdownModule, MarkedOptions} from 'ngx-markdown';
import {PUBLIC_CONFIG_OPTIONS} from '@app/public/public.config';
import {MAT_TOOLTIP_DEFAULT_OPTIONS, MatTooltipDefaultOptions} from '@angular/material/tooltip';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    IonicModule.forRoot(), // FIXME: After Ionic v6 upgrade, override platform detection (see issue #323)
    CacheModule.forRoot(),
    IonicStorageModule.forRoot({
      name: 'sar', // default
      ...environment.storage
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (httpClient) => {
          if (environment.production) {
            // This is need to force a reload, after an app update
            return new TranslateHttpLoader(httpClient, './assets/i18n/', `-${environment.version}.json`);
          }
          return new TranslateHttpLoader(httpClient, './assets/i18n/', `.json`);
        },
        deps: [HttpClient]
      }
    }),
    MarkdownModule.forRoot({
      loader: HttpClient, // Allow to load using [src]
      sanitize: SecurityContext.NONE,
      markedOptions: {
        provide: MarkedOptions,
        useValue: <MarkedOptions>{
          gfm: true,
          breaks: false,
          pedantic: false,
          smartLists: true,
          smartypants: false,
        },
      }
    }),
    // Need for tap event, in app-toolbar
    HammerModule,

    // functional modules
    AppSharedModule.forRoot(environment),
    AppCoreModule.forRoot(),
    /*SocialModule,
    AppHubEauModule,*/
    AppRoutingModule
  ],
  providers: [

    {provide: APP_BASE_HREF, useFactory: () => {
        try {
          return document.getElementsByTagName('base')[0].href;
        }
        catch (err) {
          console.error(err);
          return environment.baseUrl || '/';
        }
      }
    },

    {provide: APP_LOCALES, useValue:
        [
          {
            key: 'fr',
            value: 'Français',
            country: 'fr'
          },
          {
            key: 'en',
            value: 'English',
            country: 'gb'
          }
        ]
    },

    {provide: MAT_TOOLTIP_DEFAULT_OPTIONS, useValue: <MatTooltipDefaultOptions>{
        showDelay: 100,
        hideDelay: 1500,
        position: 'below'
      }
    },

    {provide: MAT_DATE_LOCALE, useValue: environment.defaultLocale || 'en'},
    {
      provide: MAT_DATE_FORMATS, useValue: {
        parse: {
          dateInput: DATE_ISO_PATTERN,
        },
        display: {
          dateInput: 'L',
          monthYearLabel: 'MMM YYYY',
          dateA11yLabel: 'LL',
          monthYearA11yLabel: 'MMMM YYYY',
        }
      }
    },
    {provide: MomentDateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE, MAT_DATE_FORMATS]},
    {provide: DateAdapter, useExisting: MomentDateAdapter},

    // Configure hammer gesture
    {provide: HAMMER_GESTURE_CONFIG, useClass: AppGestureConfig},

    { provide: APP_LOCAL_SETTINGS, useValue: <Partial<LocalSettings>>{
        accountInheritance: false,
        pageHistoryMaxSize: 3
      }
    },
    // Setting options definition
    { provide: APP_LOCAL_SETTINGS_OPTIONS, useValue: <FormFieldDefinitionMap>{
        // Add here functional module settings options
        // ...MY_MODULE_OPTIONS
      }},

    // Config options definition (Core + functional modules)
    { provide: APP_CONFIG_OPTIONS, useValue: {
      ...CORE_CONFIG_OPTIONS,
      ...REFERENTIAL_CONFIG_OPTIONS,
      ...PUBLIC_CONFIG_OPTIONS
    }},

    // Menu items
    { provide: APP_MENU_ITEMS, useValue: <MenuItem[]>[
        {title: 'MENU.HOME', path: '/', icon: 'home'},

        // Data access
        {title: 'MENU.DATA_ACCESS_DIVIDER'},

        {title: 'HUBEAU.MENU.CONTAMINANT', path: '/hubeau/contaminant', matIcon: 'timeline', ifProperty: 'hubeau.enabled'},
        {title: 'INTERLOCUTOR.MENU.SEARCH', path: '/public/interlocutor', icon: 'business'},
        {title: 'COASTAL_STRUCTURE_TYPE.MENU.SEARCH', path: '/public/coastal-structure', icon: 'prism'},

        // Referential
        {title: 'MENU.ADMINISTRATION_DIVIDER', exactProfile: 'ADMIN'},
        {title: 'MENU.REFERENTIAL_DIVIDER', exactProfile: 'USER'},
        {title: 'HUBEAU.MENU.MONITORING_LOCATION', path: '/hubeau/monitoringLocation', matIcon: 'room', ifProperty: 'hubeau.enabled'},
        {title: 'MENU.REFERENTIAL', path: '/referential/list', icon: 'list', profile: 'USER'},
        {title: 'MENU.USERS', path: '/admin/users', icon: 'people', profile: 'ADMIN'},
        {title: 'MENU.SERVER', path: '/admin/config', icon: 'server', profile: 'ADMIN'},

        // Settings
        {title: '' /*empty divider*/, cssClass: 'flex-spacer'},
        {title: 'MENU.TESTING', path: '/testing', icon: 'code', color: 'danger', ifProperty: 'sumaris.testing.enable', profile: 'ADMIN'},
        {title: 'MENU.LOCAL_SETTINGS', path: '/settings', icon: 'settings', color: 'medium'},
        {title: 'MENU.ABOUT', action: 'about', matIcon: 'help_outline', color: 'medium', cssClass: 'visible-mobile'},

        // Logout
        {title: 'MENU.LOGOUT', action: 'logout', icon: 'log-out', profile: 'GUEST', color: 'medium hidden-mobile'},
        {title: 'MENU.LOGOUT', action: 'logout', icon: 'log-out', profile: 'GUEST', color: 'danger visible-mobile'}

      ]
    },

    // Home buttons
    { provide: APP_HOME_BUTTONS, useValue: [
        // Data access
        {title: 'MENU.DATA_ACCESS_DIVIDER'},
        {title: 'HUBEAU.MENU.CONTAMINANT', path: '/hubeau/contaminant', matIcon: 'timeline', ifProperty: 'hubeau.enabled'},
        {title: 'INTERLOCUTOR.MENU.SEARCH', path: '/public/interlocutor', icon: 'business'},
        {title: 'COASTAL_STRUCTURE_TYPE.MENU.SEARCH', path: '/public/coastal-structure', icon: 'prism'},

        // Administration
        { title: 'MENU.ADMINISTRATION_DIVIDER', exactProfile: 'ADMIN'},
        { title: 'MENU.REFERENTIAL_DIVIDER', exactProfile: 'USER'},
        { title: 'MENU.REFERENTIAL', path: '/referential/list', icon: 'list', profile: 'USER'},
        { title: '' /*empty divider*/, cssClass: 'visible-mobile'}
    ]
    },

    // Entities Apollo cache options
    { provide: APP_GRAPHQL_TYPE_POLICIES, useValue: <TypePolicies>{
        ...REFERENTIAL_GRAPHQL_TYPE_POLICIES
      }
    },

    { provide: APP_ABOUT_DEVELOPERS, useValue: <Partial<Department>[]>[
        {name: 'Environmental Information Systems', logo: 'assets/img/logo/logo-eis_50px.png', siteUrl: 'https://www.e-is.pro'}
    ]},

    { provide: APP_ABOUT_PARTNERS, useValue: <Partial<Department>[]>[
        {label: 'SIMM', logo: 'assets/img/logo/logo-simm.png', siteUrl: 'https://www.milieumarinfrance.fr/'},
        {label: 'Ifremer', logo: 'assets/img/logo/logo-ifremer.png', siteUrl: 'https://www.ifremer.fr'},
        {label: 'OFB', logo: 'assets/img/logo/logo-ofb.png', siteUrl: 'https://ofb.gouv.fr/'},
        {name: 'Ministère de la transition Ecologique', logo: 'assets/img/logo/logo-mte.png', siteUrl: 'https://www.ecologie.gouv.fr/'}
      ]},

    // Custom identicon style
    // https://jdenticon.com/icon-designer.html?config=4451860010ff320028501e5a
    {
      provide: JDENTICON_CONFIG,
      useValue: {
        lightness: {
          color: [0.26, 0.80],
          grayscale: [0.30, 0.90],
        },
        saturation: {
          color: 0.50,
          grayscale: 0.46,
        },
        backColor: '#0000'
      }
    },

    // Test pages link
    { provide: APP_TESTING_PAGES, useValue: <TestingPage[]>[
        ...SHARED_TESTING_PAGES,
        //...
    ]}
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {

  constructor() {
    console.debug('[app] Creating module');
  }
}
