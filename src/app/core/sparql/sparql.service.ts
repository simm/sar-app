import {
  ConfigService,
  ConnectionType,
  EntitiesServiceWatchOptions,
  ENVIRONMENT,
  ErrorCodes,
  firstNotNilPromise,
  isNotNil,
  NetworkService,
  Peer,
  PlatformService,
  StartableService
} from '@sumaris-net/ngx-components';
import {Inject, Injectable, Optional} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {from, Observable, Subject} from 'rxjs';
import {catchError, distinctUntilChanged, filter, map, mergeMap, switchMap, throttleTime} from 'rxjs/operators';
import {SparqlUtils, SparqlUtilsBindQuery} from '@app/core/sparql/sparql.utils';
import {NodeHealthResult} from '@app/shared/network/health.model';
import {KNOWN_RDF_SCHEMAS, RdfSchema, SparqlQueryOptions, SparqlQueryResult, SparqlResultFormat, SparqlResultFormatItem} from '@app/core/sparql/sparql.model';

export declare interface SparqlServiceWatchOptions extends EntitiesServiceWatchOptions {
  lightLoad?: boolean,
}

@Injectable({providedIn: 'root'})
export class SparqlService extends StartableService<RdfSchema> {

  private readonly _networkStatusChanged$: Observable<ConnectionType>;
  private readonly _logPrefix: string;
  private onNetworkError = new Subject();
  private _endpoint: string;
  private _additionalSchemas: RdfSchema[] = [];

  private get defaultSchema(): RdfSchema {
    return this._data;
  }

  private get knownSchemas(): RdfSchema[] {
    return [
      // Add well known prefixes
      ...KNOWN_RDF_SCHEMAS,
      // Add default prefix
      this.defaultSchema, {...this.defaultSchema, prefix: 'this'},
      // Registered schemas
      ...this._additionalSchemas
    ];
  }

  constructor(protected platformService: PlatformService,
              @Optional() private network: NetworkService,
              private configService: ConfigService,
              private httpClient: HttpClient,
              @Inject(ENVIRONMENT) protected environment) {
    super(platformService);

    // for DEV only
    this._debug = !environment.production;
    this._logPrefix = '[sparql] ';

    // Listen network status
    this._networkStatusChanged$ = network.onNetworkStatusChanges
      .pipe(
        filter(isNotNil),
        distinctUntilChanged()
      );

    // When getting network error: try to ping peer, and toggle to offline
    this.onNetworkError
      .pipe(
        throttleTime(300),
        filter(() => this.network.online),
        mergeMap(() => this.network.checkPeerAlive()),
        filter(alive => !alive)
      )
      .subscribe(() => this.network.setForceOffline(true, {showToast: true}));
  }

  /**
   * Execute a sparql query. Return a list of Object
   *
   * @param queryOptions
   * @param options
   * @return list of object
   */
  watch<T = any, TVariables = Record<string, any>>(queryOptions: SparqlQueryOptions<TVariables>, options?: SparqlUtilsBindQuery): Observable<T[]> {
    return this.sendRequest(queryOptions, options).pipe(
      map((result: SparqlQueryResult<T>) => SparqlUtils.parseSparqlResult(result)),
    );
  }

  /**
   * Execute a sparql query, and return an CSV string[]
   *
   * @param queryOptions
   * @param options
   */
  watchAsCsv<TVariables = Record<string, any>>(queryOptions: SparqlQueryOptions<TVariables>, options?: {
    filters: string[];
    exactMatch?: boolean;
  }): Observable<string[]> {
    if (!queryOptions) throw new Error('Missing \'query\' argument!');

    const queryString = SparqlUtils.bindQuery(queryOptions.query, queryOptions.variables, {
      ...options,
      schemas: this.knownSchemas
    });

    if (this._debug) console.debug(this._logPrefix + 'Executing SparQL query...', queryString);

    const data = new FormData();
    data.append('query', queryString);

    return this.httpClient.post(this._endpoint,
      data,
      {
        headers: {
          Accept: 'application/sparql-results+csv,*/*;q=0.9'
        },
        responseType: 'text'
      })
      .pipe(
        map((text: string) => text
          // Remove double quote
          .replace(/"/g, '')
          // Split rows
          .split('\n')
          // Exclude header row, and the last (empty) line
          .filter((line, index) => index > 0 && line && line.length)
        )
      );
  };


  registerSchemas(schemas: RdfSchema[], opts?: { overideSchema: boolean }) {
    (schemas || []).forEach(schema => this.registerSchema(schema, opts));
  }

  registerSchema(schema: RdfSchema, opts?: { overideSchema: boolean }) {
    if (!schema || !schema.prefix || !schema.namespace) throw new Error('Invalid \'schema\' argument');
    // Make sure prefix is unique
    const schemaIndex = this._additionalSchemas.findIndex(s => s.prefix === schema.prefix);
    if (opts && opts?.overideSchema && schemaIndex !== -1) {
      if (this._additionalSchemas[schemaIndex].namespace !== schema.namespace) {
        console.debug(this._logPrefix + `Update schema : prefix \'${schema.prefix}\'`);
        this._additionalSchemas[schemaIndex] = schema;
      }
    } else {
      if (schemaIndex !== -1) {
        console.debug(this._logPrefix + `Skipping additional schema registration : prefix \'${schema.prefix}\' already registered`);
        return;
      }
      this._additionalSchemas.push(schema);
    }
  }

  sendRequest<TVariables = Record<string, any>>(
    queryOptions: SparqlQueryOptions<TVariables>,
    options?: SparqlUtilsBindQuery,
    expectedFromat: SparqlResultFormatItem = SparqlResultFormat.json,
  ): Observable<any> {
    if (!queryOptions) throw new Error('Missing \'queryOptions\' argument!');
    if (!this.started) {
      console.info(this._logPrefix + 'Waiting ready...');
      return from(this.ready())
        .pipe(
          switchMap(() => this.sendRequest(queryOptions, options, expectedFromat))
        );
    }
    const queryString = SparqlUtils.bindQuery(queryOptions.query, queryOptions.variables, {
      ...options,
      schemas: this.knownSchemas
    });
    if (this._debug) console.debug(this._logPrefix + 'Executing SparQL query...', queryString);
    const data = new FormData();
    data.append('query', queryString);
    const result = this.httpClient.post(this._endpoint,
      data,
      {
        headers: {
          Accept: expectedFromat.contentType,
        },
        responseType: expectedFromat.responseType as any,
      });
    return result
      .pipe(
        catchError(error => this.onRequestError(error))
      );
  }

  /* -- protected functions -- */

  protected async ngOnStart(): Promise<RdfSchema> {

    await this.network.ready();

    const now = Date.now();
    console.info(this._logPrefix + 'Starting sparql...');

    const peer = this.network.peer;
    if (!peer) throw Error(this._logPrefix + 'Missing peer. Unable to start graphql service');
    this._endpoint = peer.url + '/sparql';


    try {
      const schema = await this.loadNodeSchema(peer, 'this');

      // Listen for network restart
      this.registerSubscription(
        this.network.on('start', () => this.restart())
      );

      // Listen for clear cache request, from network
      this.registerSubscription(
        this.network.on('resetCache', () => this.clearCache())
      );

      console.info(this._logPrefix + `Endpoint uri: ${this._endpoint}`);
      console.info(this._logPrefix + `Schema uri: ${schema?.namespace}`);
      console.info(this._logPrefix + `Starting sparql [OK] in ${Date.now() - now}ms`);

      return schema;

    } catch (err) {
      console.error(this._logPrefix + 'Failed to start sparql service: ', err);
      throw new Error(this._logPrefix + 'Failed to start: ' + (err && err.message || err));
    }
  }

  protected async ngOnStop() {
    console.info(this._logPrefix + 'Stopping sparql service...');
  }

  protected watchNodeHealth(peer: Peer): Observable<NodeHealthResult> {
    if (!peer) throw new Error('Missing \'peer\' argument.');
    const path = '/api/node/health';
    const url = peer.url + path;

    console.info(this._logPrefix + `Loading node health \'${path}\' ...`);
    return this.httpClient.get(url)
      .pipe(
        map(json => {
          console.info(this._logPrefix + `Loading node health [OK]`, json);
          return json as NodeHealthResult;
        }));
  }

  protected async loadNodeSchema(peer: Peer, defaultPrefix): Promise<RdfSchema> {
    defaultPrefix = defaultPrefix || 'this';
    const defaultNamespace = window.location.origin + '/schema/';
    const defaultVersion = '0.1';

    const health = await firstNotNilPromise(this.watchNodeHealth(peer));

    const details = health?.components && health.components.rdf?.details;
    const namespace = details && details.schemaUri || defaultNamespace;
    const prefix = details && details.modelPrefix || defaultPrefix;
    const version = details && details.modelVersion || defaultVersion;
    return {
      name: `${prefix.toUpperCase()} Model`,
      namespace,
      prefix,
      version
    };
  }

  protected onRequestError<T = any>(error: any): T[] {
    if (error?.code === ErrorCodes.UNKNOWN_NETWORK_ERROR) {
      console.error(this._logPrefix + 'original error: ', error);
      this.onNetworkError.next(error);
    } else {
      console.error(this._logPrefix + 'Unknown error: ', error);
    }

    return [];
  }

  protected async clearCache() {
    // Add here cache clean logic, if any
  }

}
