export declare type SparqlResultFormatItem = {
  contentType: string,
  responseType: 'text' | 'json',
  exportFileExt: string,
  exportFileEncoding: string,
  exportFileType: string,
}
export const SparqlResultFormat: {[key:string]: SparqlResultFormatItem} = {
  json: {
    contentType: 'application/sparql-results+json',
    responseType: 'json',
    exportFileExt: 'json',
    exportFileEncoding: 'UTF-8',
    exportFileType: 'text/json',
  },
  jsonRdf: {
    contentType: 'application/rdf+json',
    responseType: 'json',
    exportFileExt: 'rdf.json',
    exportFileEncoding: 'UTF-8',
    exportFileType: 'application/json',
  },
  xmlRdf: {
    contentType: 'application/rdf+xml',
    responseType: 'text',
    exportFileExt: 'rdf.xml',
    exportFileEncoding: 'UTF-8',
    exportFileType: 'application/xml',
  },
  turtle: {
    contentType: 'text/turtle',
    responseType: 'text',
    exportFileExt: 'ttl',
    exportFileEncoding: 'UTF-8',
    exportFileType: 'text/turtle',
  },
}
export type SparqlAvailableResultFormat = keyof typeof SparqlResultFormat;

export type SparqlResultBinding<T> = {
  [key in keyof T]: SparqlResultValue;
};

export interface SparqlResultValue {
  type: string|'uri'|'literal';
  datatype?: string;
  value: string;
}

export interface SparqlQueryResult<T> {
  head: {
    vars: (keyof T)[];
  };
  results: {
    bindings: SparqlResultBinding<T>[];
  };
}

export interface SparqlQueryDef {
  prefixes?: string[];
  query: string;
}

export interface SparqlQueryOptions<TVariables = Record<string, any>> {
  query: SparqlQueryDef;
  variables: TVariables;
}

export interface RdfSchema {
  name: string;
  prefix: string;
  namespace: string;
  version?: string;
}

export const KNOWN_RDF_SCHEMAS: RdfSchema[] = [
    // Common schemas
    {
      name: 'Dublin Core',
      prefix: 'dc',
      namespace: 'http://purl.org/dc/elements/1.1/'
    },
    {
      name: 'Dublin Core Terms',
      prefix: 'dcterms',
      namespace: 'http://purl.org/dc/terms/'
    },
    {
      name: 'Schema.org',
      prefix: 's',
      namespace: 'http://schema.org/'
    },
    {
      name: 'OWL',
      prefix: 'owl',
      namespace: 'http://www.w3.org/2002/07/owl#'
    },
    {
      name: 'RDF',
      prefix: 'rdf',
      namespace: 'http://www.w3.org/1999/02/22-rdf-syntax-ns#'
    },
    {
      name: 'RDF Schema',
      prefix: 'rdfs',
      namespace: 'http://www.w3.org/2000/01/rdf-schema#'
    },
    {
      name: 'SKOS thesaurus',
      prefix: 'skos',
      namespace: 'http://www.w3.org/2004/02/skos/core#'
    },

    // Social
    {
      name: 'Friend of a Friend',
      prefix: 'foaf',
      namespace: 'http://xmlns.com/foaf/0.1/'
    },
    {
      name: 'Good Relations',
      prefix: 'gr',
      namespace: 'http://purl.org/goodrelations/v1#'
    },
    {
      name: 'XML Schema Datatypes',
      prefix: 'xsd',
      namespace: 'http://www.w3.org/2001/XMLSchema#'
    },

    // {
    //     name: 'RDF Data',
    //     prefix: 'rdfdata',
    //     namespace: 'http://rdf.data-vocabulary.org/rdf.xml#'
    // },
    // {
    //     name: 'RDF Data format',
    //     prefix: 'rdfdf',
    //     namespace: 'http://www.openlinksw.com/virtrdf-data-formats#'
    // },
    // {
    //     name: 'RDF FG',
    //     prefix: 'rdfg',
    //     namespace: 'http://www.w3.org/2004/03/trix/rdfg-1/'
    // },
    // {
    //     name: 'RDF FP',
    //     prefix: 'rdfp',
    //     namespace: 'https://w3id.org/rdfp/'
    // },


    // Taxon
    {
      name: 'Darwin core terms (string literal objects)',
      prefix: 'dwc',
      namespace: 'http://rs.tdwg.org/dwc/terms/'
    },
    {
      name: 'Darwin core terms (IRI reference objects)',
      prefix: 'dwciri',
      namespace: 'http://rs.tdwg.org/dwc/iri/'
    },
    {
      name: 'Darwin core TaxonName',
      prefix: 'dwctax',
      namespace: 'http://rs.tdwg.org/ontology/voc/TaxonName#'
    },

    // Spatial
    {
      name: 'Spatial',
      prefix: 'spatial',
      namespace: 'http://www.w3.org/2004/02/skos/core#'
    },
    {
      name: 'GeoSparql',
      prefix: 'geo',
      namespace: 'http://www.opengis.net/ont/geosparql#'
    },
    {
      name: 'GeoNames',
      prefix: 'gn',
      namespace: 'http://www.geonames.org/ontology#'
    },

    // Taxon (custom)
    {
      name: 'TaxRef linked data (MNHN)',
      prefix: 'taxref',
      namespace: 'http://taxref.mnhn.fr/lod/'
    },
    {
      name: 'TaxRef properties (MNHN)',
      prefix: 'taxrefprop',
      namespace: 'http://taxref.mnhn.fr/lod/property/'
    },
    {
      name: 'Eau France (Sandre)',
      prefix: 'eaufrance',
      namespace: 'http://id.eaufrance.fr/'
    },
    {
      name: 'Appellation Taxon (Sandre)',
      prefix: 'apt',
      namespace: 'http://id.eaufrance.fr/ddd/APT/'
    },
    {
      name: 'Appellation Taxon (Sandre)',
      prefix: 'apt2',
      namespace: 'http://id.eaufrance.fr/ddd/APT/2.1/'
    },
    {
      name: 'Appellation Taxon (Sandre) data ',
      prefix: 'aptdata',
      namespace: 'http://id.eaufrance.fr/apt/'
    },

    // Organization
    {
      name: 'Organization (W3C)',
      prefix: 'org',
      namespace: 'http://www.w3.org/ns/org#'
    },

    // Organization (custom)
    {
      name: 'Interlocuteurs (Sandre)',
      prefix: 'inc',
      namespace: 'http://id.eaufrance.fr/ddd/INC/1.0/'
    },
    {
      name: 'Interlocuteurs (Sandre) data ',
      prefix: 'incdata',
      namespace: 'http://id.eaufrance.fr/inc/'
    },

    // Commun (Sandre)
    {
      name: 'Commun (Sandre)',
      prefix: 'com',
      namespace: 'http://id.eaufrance.fr/ddd/COM/4/'
    },
    {
      name: 'Statut Sandre',
      prefix: 'status',
      namespace: 'http://id.eaufrance.fr/nsa/727#'
    },
    {
      name: 'Validity Statut Sandre',
      prefix: 'validityStatus',
      namespace: 'http://id.eaufrance.fr/nsa/390#'
    },
    {
      name: 'SIREN Vocab',
      prefix: 'sirene',
      namespace: 'https://sireneld.io/vocab/sirene#'
    }
  ];
