import {
  EntitiesServiceWatchOptions,
  Entity,
  EntityFilter,
  EntityFilterUtils,
  IEntitiesService,
  LoadResult,
  PlatformService,
  StartableService,
  SuggestService,
  toBoolean
} from '@sumaris-net/ngx-components';
import {Optional} from '@angular/core';
import {SparqlService} from '@app/core/sparql/sparql.service';
import {Observable} from 'rxjs';
import {SortDirection} from '@angular/material/sort';
import {first} from 'rxjs/operators';
import {environment} from '@environments/environment';

export class EntitySparqlServiceOptions {
  production?: boolean;
}

export abstract class BaseEntitySparqlService<
  T extends Entity<T, ID>,
  F extends EntityFilter<F, T, any>,
  ID = number
> extends StartableService implements IEntitiesService<T, F>, SuggestService<T, F> {

  protected _logPrefix: string;

  protected constructor(protected sparql: SparqlService,
                        protected dataType: new() => T,
                        protected filterType: new() => F,
                        @Optional() options?: EntitySparqlServiceOptions) {
    super(sparql);

    // for DEV only
    this._debug = toBoolean(options && !options.production, !environment.production);
    this._logPrefix = `[base-sparql-service] `;
  }

  loadAll(offset: number, size: number, sortBy?: string, sortDirection?: SortDirection, filter?: Partial<F>, options?: EntitiesServiceWatchOptions): Promise<LoadResult<T>> {
    return this.watchAll(offset, size, sortBy, sortDirection, filter, options).pipe(first()).toPromise();
  }

  fromObject(source: any): T {
    if (!source) return source;
    const target = new this.dataType();
    target.fromObject(source);
    return target;
  }

  asFilter(source: any): F {
    return EntityFilterUtils.fromObject(source, this.filterType);
  }

  saveAll(data: T[], opts?: any): Promise<T[]> {
    throw new Error('Not implemented!');
  }

  deleteAll(data: T[], opts?: any): Promise<any> {
    throw new Error('Not implemented!');
  }

  async suggest(value: any, filter?: Partial<F>): Promise<LoadResult<T>> {
    if (value && typeof value === 'object') return {data: [value]};
    value = (typeof value === 'string' && value !== '*') && value || undefined;
    return this.loadAll(0, !value ? 30 : 10, undefined, undefined,
      {
        ...filter,
        searchText: value as string
      }
    );
  }

  /* -- protected functions -- */

  protected async ngOnStart(): Promise<any> {
    // Nothing to do
  }

  /* -- abstract functions -- */

  abstract watchAll(offset: number, size: number, sortBy?: string, sortDirection?: SortDirection, filter?: Partial<F>, options?: EntitiesServiceWatchOptions): Observable<LoadResult<T>>;

}
