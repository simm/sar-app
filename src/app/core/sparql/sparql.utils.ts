import {isEmptyArray, isNil, isNilOrBlank, isNotEmptyArray, isNotNil, isNotNilOrBlank, toBoolean} from '@sumaris-net/ngx-components';
import {KNOWN_RDF_SCHEMAS, RdfSchema, SparqlQueryDef, SparqlQueryResult, SparqlResultBinding, SparqlResultValue} from '@app/core/sparql/sparql.model';

export declare type SparqlUtilsBindQuery = {
  filters?: string[];
  exactMatch?: boolean;
  schemas?: RdfSchema[];
}

export abstract class SparqlUtils {

  static PHRASE_SEPARATOR_REGEXP = /"([^"]+)"/g;
  static WORD_SEPARATOR_REGEXP = /[",;\t\s]+/;

  static parseSparqlResult<T>(result: SparqlQueryResult<T>): T[] {
    const propertyNames = result.head.vars as (keyof T)[];
    return (result.results?.bindings || []).map((source: SparqlResultBinding<T>) => {
      const target = <T>{};
      propertyNames.forEach(p => target[p] = this.parseValue(source[p]));
      return target;
    });
  }

  static parseValue(source: SparqlResultValue): any {
    if (!source) {
      return undefined;
    }
    const datatype = source.datatype || 'string';
    switch (datatype) {
      case 'http://www.w3.org/2001/XMLSchema#integer':
        return +source.value;
      case 'http://www.w3.org/2001/XMLSchema#dateTime':
        return ''+source.value;
      case 'string':
        return isNil(source.value) ? null: (''+source.value);
      default:
        console.error('[sparql-utils] Unknown dataType: ' + datatype);
        return isNil(source.value) ? null: (''+source.value);
    }
  }

  /***
   *
   * @param queryDef a SPARQL query
   */
  static toCountQuery(queryDef: SparqlQueryDef, opts?: {fieldName?: string; distinct?: boolean}): SparqlQueryDef {
    if (!queryDef) throw new Error('Missing \'query\' argument!');

    return {
      prefixes: queryDef.prefixes,
      query: this.countQueryFromSelect(queryDef.query, opts)
    };
  }

  /***
   *
   * @param queryString a SPARQL select query
   */
  static countQueryFromSelect(queryString: string, opts?: {fieldName?: string; distinct?: boolean}): string {
    if (!queryString) throw new Error('Missing \'query\' argument!');
    opts = opts || {};
    opts.fieldName = opts.fieldName || 'sourceUri';
    opts.distinct = toBoolean(opts.distinct, true);

    console.debug('[sparql-utils] Computing sparql count query, from a select...');

    // Create the count query, from a select query
    const countQuery = queryString
      // Transform SELECT into COUNT
      .replace(/SELECT[^{]+WHERE/gm, `SELECT\n\t(COUNT(${opts.distinct ? 'DISTINCT ' : ''}?${opts.fieldName}) as ?count)\nWHERE`)
      // Remove ORDER BY and LIMIT
      .replace(/[\n\s]*ORDER BY\s+[^\n]*/gm, '')
      .replace(/[\n\s]*OFFSET\s+[^\n]*/gm, '')
      .replace(/[\n\s]*LIMIT\s+[^\n]*/gm, '');

    return countQuery;
  }

  static getSearchTerms(value: string): string[] {
    if (isNilOrBlank(value)) return [];

    value = value.trim();

    // Add phrases
    const matches = this.PHRASE_SEPARATOR_REGEXP.exec(value);
    const phrases = matches && matches.slice(1) || [];
    // Non-phrases elements = words
    const words = value.replace(this.PHRASE_SEPARATOR_REGEXP, '')
      // Remove space between '+' or '*'
      .replace(/\s*([+*])\s*/g, '$1')
      // Split words
      .split(this.WORD_SEPARATOR_REGEXP)
      .filter(isNotNilOrBlank)
      .map(s => s.trim().replace(/[*+]+/g, '*'));

    return phrases.concat(words);
  }

  static bindQuery<TVariables = Record<string, any>>(queryDef: SparqlQueryDef,
                                                     variables: TVariables,
                                                     opts?: SparqlUtilsBindQuery): string {

    let queryString = this.bindSparql(queryDef.query, variables, opts);

    // Add prefixes
    if (isNotEmptyArray(queryDef.prefixes))  {
      const schemas = opts.schemas || KNOWN_RDF_SCHEMAS;
      const prefixes = queryDef.prefixes.map(prefix => {
        const schema = schemas.find(schema => schema?.prefix === prefix && isNotNilOrBlank(schema.namespace));
        if (isNilOrBlank(schema)) return null;
        return `PREFIX ${prefix}: <${schema.namespace}>`;
      }).filter(isNotNil);
      queryString = [...prefixes, queryString].join('\n');
    }

    return queryString;
  }

  static bindSparql<TVariables = Record<string, any>>(queryString: string,
                                                      variables: TVariables & { q?: string|string[]; filter?: string },
                                                      opts?: {
                                                        filters?: string[];
                                                        exactMatch?: boolean;
                                                      }): string {

    if (isNil(variables.filter) && isNotEmptyArray(opts?.filters)) {
      variables.filter = this.bindFilters(opts.filters, variables, {exactMatch: opts?.exactMatch});
    }

    if (queryString.includes('{{')) {
      queryString = Object.keys(variables).reduce((query, key) => {
        const regexp = new RegExp('{{' + key + '}}', 'g');
        return query.replace(regexp, variables[key]);
      }, queryString);
    }

    // Remove empty FILTER
    queryString = queryString.replace(/FILTER\s*\(\s*\)\s*\.?/, '')
      // Remove OFFSET 0 => no offset
      .replace(/OFFSET\s+0\s*/, '')
      // Remove LIMIT -1 => no limit
      .replace(/LIMIT\s+-1\s*/, '');

    return queryString;
  }

  static bindFilters<TVariables = Record<string, any>>(filters: string[],
                                                       variables: TVariables & { q?: string|string[] },
                                                       opts?: {
                                                             exactMatch?: boolean;
                                                           }): string {
    if (isEmptyArray(filters)) return '';

    let queryString = '{{filter}}';

    if (isNotNil(variables.q)) {
      let searchTerms: string[];
      if (typeof variables.q === 'string') {
        searchTerms = this.getSearchTerms(variables.q);
      } else {
        searchTerms = variables.q.filter(isNotNilOrBlank).map(term => term.trim());
      }

      // Copy variables, without 'q'
      variables = {...variables};
      delete variables.q;

      // Create a filter part, for each terms
      if (isNotEmptyArray(searchTerms)) {
        let nbLoop = 0;
        do {
          queryString = searchTerms.reduce((query, term) => {

            // Create filter clause (include only if '{{q}})
            const filterClause = this.joinFilterClauses(filters, {
              selector: filter => filter.includes('{{q}}'),
              operator: '&&'
            });

            // Insert filter clause into query
            if (isNotNilOrBlank(filterClause)) {
              queryString = queryString.replace('#~filter', '\n\t|| (' + filterClause + ') #~filter\n\t')
                .replace('{{filter}}', '(' + filterClause + ' #~filter)\n\t');
            }

            // Replace wildcards by regexp, if NOT exact match
            console.debug('MYTEST TERM', term);
            term = opts?.exactMatch ? term
              : term.trim()
                .replace(/[*]+/g, '.*')
                // Ignore accent characters
                .replace(/[eéèêë]/ig, '[eéèêë]')
                .replace(/[aáàâäâ]/ig, '[aáàâäâ]')
                .replace(/[iïî]/ig, '[iïî]')
                .replace(/[oôö]/ig, '[oôö]')
                .replace(/[cç]/ig, '[cç]')
                .replace(/[ùû]/ig, '[ùû]');

            // Bind params
            return SparqlUtils.bindSparql(queryString, {...variables, q: term});
          }, queryString);

        } while (queryString.includes('{{') && ++nbLoop < 10);

        // Clean the query
        queryString = queryString.replace('#~filter)', ')\n\t&& {{filter}}');
      }
    }

    if (queryString.includes('{{')) {
      let nbLoop = 0;
      do {
        // Create filter clause (include only if NOT '{{q}})
        const filterClause = this.joinFilterClauses(filters, {
          selector: filter => !filter.includes('{{q}}'),
          operator: '&&'
        });

        // Insert filter clause
        if (isNotNilOrBlank(filterClause)) {
          queryString = queryString.replace('#~filter', '\n\t|| (' + filterClause + ') #~filter\n\t')
            .replace('{{filter}}', '(' + filterClause + ') #~filter\n\t');
        }

        // Bind params
        queryString = SparqlUtils.bindSparql(queryString, {...variables, q: undefined});

      } while (queryString.includes('{{') && ++nbLoop < 10);

      // Clean filter variable
      queryString = queryString.replace('&& {{filter}}', '')
        .replace('#~filter', '')
        .replace('{{filter}}', 'true');
    }

    return queryString;
  }

  /**
   * Join many filter clauses, using operator
   */
  static joinFilterClauses(filterClauses: string[], opts?: {
    selector?: (sparqlClause: string) => boolean;
    operator?: '&&' | '||';
  }) {
    const selector = opts && opts.selector || ((_) => true);
    const operator = opts && opts.operator || '&&';
    return (filterClauses || [])
      .filter(selector)
      .join('\n\t' + operator + ' ');
  }


  static jsonTree(source: Object, separator = '_'): void {
    Object.keys(source)
      .filter(key => key.indexOf(separator) !== -1 && key !== '__typename')
      .forEach(key => {
        const value = source[key];
        delete source[key];
        const parts = key.split(separator);
        let current = source;
        let lastKey = parts.pop();
        for (let key of parts) {
          if (!current[key]) current[key] = {};
          current = current[key];
        }
        current[lastKey] = value;
      })
  }
}
