import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {CoreModule} from '@sumaris-net/ngx-components';

@NgModule({
  imports: [
    CoreModule,
    HttpClientModule
  ],
  exports: [
    HttpClientModule
  ]
})
export class AppSparQLModule {

  constructor() {
    console.debug('[app-sparql-module] Creating module...');
  }
}

