import {ModuleWithProviders, NgModule} from '@angular/core';
import {IonicStorageModule} from '@ionic/storage';
import {HttpClientModule} from '@angular/common/http';
import {CacheModule} from 'ionic-cache';
import {AppGraphQLModule, CoreModule, NetworkService, PlatformService} from '@sumaris-net/ngx-components';
import {AppSharedModule} from '@app/shared/shared.module';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {SparqlService} from '@app/core/sparql/sparql.service';
import {AppSparQLModule} from '@app/core/sparql/sparql.module';

@NgModule({
  imports: [
    CoreModule,
    HttpClientModule,
    CacheModule,
    IonicStorageModule,

    // App modules
    AppSharedModule,
    AppSparQLModule
  ],
  declarations: [
  ],
  exports: [
    CoreModule,

    // App modules
    AppSharedModule,
    AppSparQLModule
  ]
})
export class AppCoreModule {

  static forRoot(): ModuleWithProviders<AppCoreModule> {
    console.debug('[app-core-module] Creating module');
    return {
      ngModule: AppCoreModule,
      providers: [
        ...CoreModule.forRoot().providers
      ]
    };
  }
}
