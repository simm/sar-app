import {FormFieldDefinitionMap} from '@sumaris-net/ngx-components';
import {INTERLOCUTOR_CONFIG_OPTIONS} from '@app/public/interlocutor/interlocuteur.config';

export const PUBLIC_CONFIG_OPTIONS: FormFieldDefinitionMap = {
  ...INTERLOCUTOR_CONFIG_OPTIONS
};

export const PUBLIC_CONFIG_DEFAULTS = {
  debounceTime: 650
};
