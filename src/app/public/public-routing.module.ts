import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {SharedRoutingModule} from '@sumaris-net/ngx-components';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'interlocutor'
  },
  {
    path: 'interlocutor',
    loadChildren: () => import('./interlocutor/interlocutor-routing.module').then(m => m.InterlocutorRoutingModule)
  },
  {
    path: 'coastal-structure',
    loadChildren: () => import('@app/referential/coastal-structure/coastal-structure-routing.module.ts').then(m => m.CoastalStructureRoutingModule)
  }
];

@NgModule({
  imports: [
    SharedRoutingModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class AppPublicRoutingModule {
}
