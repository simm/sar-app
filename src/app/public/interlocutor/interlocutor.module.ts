import {NgModule} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {AppSharedModule} from '@app/shared/shared.module';
import {InterlocutorTable} from '@app/public/interlocutor/interlocutor.table';
import {AppCoreModule} from '@app/core/core.module';
import {AppSparQLModule} from '@app/core/sparql/sparql.module';

@NgModule({
  imports: [
    AppCoreModule,
    TranslateModule.forChild(),
    AppSparQLModule
  ],
  exports: [
    TranslateModule,
    InterlocutorTable
  ],
  declarations: [
    InterlocutorTable
  ]
})
export class AppInterlocutorModule {

  constructor() {
    console.debug('[interlocuteur-module] Creating module');
  }
}
