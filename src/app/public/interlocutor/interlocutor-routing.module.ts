import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {SharedRoutingModule} from '@sumaris-net/ngx-components';
import {InterlocutorTable} from './interlocutor.table';
import {AppInterlocutorModule} from '@app/public/interlocutor/interlocutor.module';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'search'
  },
  {
    path: 'search',
    component: InterlocutorTable
  },
  {
    path: 'embed',
    component: InterlocutorTable,
    data: {
      menu: false,
      showToolbar: false
    }
  }
];

@NgModule({
  imports: [
    SharedRoutingModule,
    AppInterlocutorModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class InterlocutorRoutingModule {
}
