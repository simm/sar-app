import {Inject, Injectable} from '@angular/core';
import {EntitiesServiceWatchOptions, ENVIRONMENT, isNilOrBlank, isNotEmptyArray, isNotNilOrBlank, LoadResult, toNumber} from '@sumaris-net/ngx-components';
import {Interlocutor, InterlocutorFilter, SandreValidityStatusByUri} from './interlocutor.model';
import {SparqlService} from '@app/core/sparql/sparql.service';
import {BaseEntitySparqlService} from '@app/core/sparql/base-entity-sparql.service';
import {SortDirection} from '@angular/material/sort';
import {SparqlUtils} from '@app/core/sparql/sparql.utils';
import {map, switchMap} from 'rxjs/operators';
import {combineLatest, from, Observable} from 'rxjs';
import {SparqlQueryDef} from '@app/core/sparql/sparql.model';


const ENDPOINTS_BY_URI = {
  THIS: 'this',
  EAU_FRANCE: 'http://id.eaufrance.fr/sparql'
};
const FILTER_MAP = {
  // Schema filter
  rdfType: 'rdf:type {{rdfType}}',
  rdfsLabel: '?id rdfs:label ?label .',
  dwcScientificName: '?id dwc:scientificName ?scientificName .',

  // Regex filter, on a property
  exactMatch: '?{{searchField}}="{{q}}"',
  prefixMatch: 'regex( ?{{searchField}}, "^{{q}}", "i" )',
  anyMatch: 'regex( ?{{searchField}}, "{{q}}", "i" )',

  // Regex filter, on uri (label)
  codeExactMatch: 'strEnds( str(?id), "/{{code}}" )',
  codePrefixMatch: 'regex( str(?id), "/{{code}}", "i" )',

  // Sandre filter
  edmoExactMatch: '?edmo="{{edmo}}"',
  edmoPrefixMatch: 'regex( ?edmo, "^{{edmo}}", "i" )',
  siretExactMatch: '?siret="{{siret}}"',
  siretPrefixMatch: 'regex( ?siret, "^{{siret}}", "i" )',
  validityStatusExactMatch: '?validityStatus = validityStatus:{{validityStatus}}',
  countryExactMatch: '?country=UCASE({{country}})',
  countryPrefixMatch: 'regex( ?country, "^{{country}}", "i" )',
};

const Queries = {
  loadAll: <SparqlQueryDef>{
    prefixes: ['dc', 'dcterms', 'rdf', 'rdfs', 'owl', 'skos', 'foaf', 'org', 's', 'inc', 'incdata', 'validityStatus', 'sirene', 'sarshr', 'sartscb'],
    query: `SELECT DISTINCT
        ?id ?name ?address ?country
        ?validityStatus ?creationDate ?updateDate ?isMain ?siret ?edmo
      WHERE {
        ?id rdfs:label ?name ;
             a org:Organization ;
             dc:identifier ?code .
        FILTER ( {{filter}} ) .
        OPTIONAL {
         ?id sarshr:transcribingItem _:edmo .
          _:edmo a sartscb:TranscribingItem ;
            rdfs:label ?edmo .
        }
        OPTIONAL {
          ?id sarshr:validityStatus ?validityStatus .
        }
        OPTIONAL {
          ?id org:hasprimarySite ?bnSite .
          OPTIONAL{ ?bnSite sirene:siret ?siret }
          OPTIONAL {
            ?id org:hasRegisteredSite ?bnSite
            BIND("1" as ?isMain)
          }
          ?bnSite org:siteAddress [
                s:streetAddress ?streetAddress ;
                s:postalCode ?postalCode ;
                s:addressLocality ?city ;
                s:addressCountry ?country
            ]
          BIND(
            REPLACE(REPLACE(
                CONCAT(?postalCode, '|', ?city),
                '^[|]+', '', 'i'),
              '[|]+', ' ', 'i')
            as ?postalCodeAndCity
          )
          BIND(
            REPLACE(REPLACE(
                CONCAT(?streetAddress, '|', ?postalCodeAndCity),
                '^[|]+', '', 'i'),
              '[|]+', ' ', 'i')
            as ?address
          )
        }
        OPTIONAL {
          ?id dc:created|dcterms:created ?creationDate ;
            dc:modified|dcterms:modified ?updateDate .
        }
      }
      ORDER BY {{sortDirection}} (?{{sortBy}})
      OFFSET {{offset}}
      LIMIT {{size}}`
  },
  loadAllFromSandre: <SparqlQueryDef>{
    prefixes: ['dc', 'dcterms', 'rdf', 'rdfs', 'owl', 'skos', 'foaf', 'org', 'gr', 's', 'inc', 'incdata', 'com', 'validityStatus', 'sirene'],
    query: `SELECT DISTINCT
        ?name ?id ?address ?country
        ?validityStatus ?creationDate ?updateDate ?isMain ?siret ?edmo
      WHERE {
       { SERVICE <{{endpoint}}> {
        ?id a inc:Interlocuteur ;
          rdfs:label ?name ;
          inc:CdInterlocuteur ?code ;
          inc:StInterlocuteur ?validityStatus ;
          inc:CdAlternatifInt _:simm .
        _:simm inc:OrCdAlternInterlocuteur "SIMM" .
        FILTER({{filter}})
        OPTIONAL {
          ?id inc:CdAlternatifInt _:edmo .
          _:edmo inc:OrCdAlternInterlocuteur "EDMO" ;
            inc:CdAlternInterlocuteur ?edmo .
        }
        OPTIONAL {
              ?id inc:Etablissement ?bnEtab .
              OPTIONAL { ?bnEtab sirene:siret ?siret . }
              OPTIONAL { ?bnEtab inc:EtabSiege ?isMain . }
        }
        OPTIONAL {
          ?id inc:AdresseInterlocuteur ?bnAddress .
          OPTIONAL { ?bnAddress inc:Compl2Adresse ?addressCompl2 . }
          OPTIONAL { ?bnAddress inc:Compl3Adresse ?addressCompl3 . }
          OPTIONAL { ?bnAddress inc:NumLbVoieAdresse ?addressRoad . }
          OPTIONAL { ?bnAddress inc:LgAcheAdresse ?postalCodeAndCity . }
          BIND(
            REPLACE(REPLACE(
              CONCAT(?addressCompl2, '|', ?addressCompl3, '|', ?addressRoad),
                '^[|]+', '', 'i'),
              '[|]+', ', ', 'i')
            as ?streetAddress
          )
          BIND(
            REPLACE(REPLACE(
              CONCAT(?streetAddress, '|', ?postalCodeAndCity),
                '^[|]+', '', 'i'),
              '[|]+', ', ', 'i')
            as ?address
          )
        }
        OPTIONAL {
          ?id inc:PaysInterlocuteur|inc:Pays _:country .
          _:country com:NomPays ?country .
        }
        OPTIONAL {
          ?id dc:created|inc:DateCreInterlocuteur ?creationDate ;
            dc:modified|inc:DateMAJInterlocuteur ?updateDate .
        }
        OPTIONAL { ?id skos:exactMatch|owl:sameAs ?exactMatch }
       }}
      }
      ORDER BY {{sortDirection}} (?{{sortBy}})
      OFFSET {{offset}}
      LIMIT {{size}}`,
    variables: {
      endpoint: ENDPOINTS_BY_URI.EAU_FRANCE
    }
  },

  loadCountries: {
    query: `PREFIX s: <http://schema.org/>
      PREFIX org: <http://www.w3.org/ns/org#>
      SELECT DISTINCT
        ?label
      WHERE {
        ?id a org:Organization ;
          org:hasprimarySite ?bnSite .
        ?bnSite org:siteAddress [
          s:addressCountry ?label
        ]
      }
      ORDER BY {{sortDirection}} (?label)
      OFFSET {{offset}}
      LIMIT {{size}}`,
    variables: {
      limit: -1
    }
  },

  // Generate later
  countAll: null
};

Queries.countAll = SparqlUtils.toCountQuery(Queries.loadAll, {fieldName: 'id'});

const INC_CODE_REGEXP = new RegExp(/^INC\d+$/);

@Injectable({providedIn: 'root'})
export class InterlocutorService
  extends BaseEntitySparqlService<Interlocutor, InterlocutorFilter, string> {

  constructor(sparql: SparqlService,
              @Inject(ENVIRONMENT) protected environment) {
    super(sparql,
      Interlocutor,
      InterlocutorFilter,
      environment);
  }

  watchAll(offset: number, size: number, sortBy?: string, sortDirection?: SortDirection, filter?: Partial<InterlocutorFilter>,
           options?: EntitiesServiceWatchOptions): Observable<LoadResult<Interlocutor>> {

    if (!this.started) {
      console.info(this._logPrefix + 'Waiting while service not ready...');
      return from(this.ready())
        .pipe(
          switchMap(() => this.watchAll(offset, size, sortBy, sortDirection, filter, options))
        );
    }

    const now = Date.now();
    console.debug(this._logPrefix + `Loading interlocutors...`);

    const variables = {
      offset,
      size,
      sortBy: sortBy === 'label' ? 'id' : (sortBy || 'name'),
      sortDirection: sortDirection && sortDirection.toUpperCase() || 'ASC',
      filter: this.bindFilter(filter)
    };

    return combineLatest([
      // get data
      this.sparql.watch<any>({
        query: Queries.loadAll,
        variables
      }),
      // get count
      this.sparql.watch<any>({
        query: Queries.countAll,
        variables
      })
    ]).pipe(
      map(([res1, res2]) => {
        const entities = (!options || options.toEntity !== false)
          ? (res1 || []).map(this.fromObject)
          : (res1 || []) as Interlocutor[];
        const total = res2?.length ? res2[0].count : entities.length;

        console.debug(this._logPrefix + `${entities.length} interlocutors loaded in ${Date.now() - now}ms`);

        return {
          data: entities,
          total
        } ;
      })
    );
  }

  watchCountries(offset?: number, size?: number, sortDirection?: SortDirection): Observable<string[]> {
    return this.sparql.watch<any>({
      query: Queries.loadCountries,
      variables: {
        offset: offset || 0,
        size: toNumber(size, -1),
        sortDirection: sortDirection && sortDirection.toUpperCase() || 'ASC'
      }
    }).pipe(
      map(res => (res || []).map(item => item.label))
    );
  }

  /* -- protected functions-- */

  fromObject(source: any): Interlocutor {
    if (!source) return undefined;

    // Compute code from the id (URI)
    if (!source.code && source.id?.includes('/')) {
      source.code = source.id.substring(source.id.lastIndexOf('/') + 1);
    }

    // Resolve validityStatus, from URI
    if (source.validityStatus) {
      const validityStatus = SandreValidityStatusByUri[source.validityStatus];
      source.validityStatus = validityStatus?.id;
    }

    return Interlocutor.fromObject(source);
  }

  protected async ngOnStart(): Promise<any> {

    // Get the default schema (e.g. 'sar' schema)
    const schema = await this.sparql.ready();
    if (isNilOrBlank(schema?.namespace)) throw Error(this._logPrefix + 'Missing Pod\'s RDF schema namespace.');
    if (isNilOrBlank(schema?.version)) throw Error(this._logPrefix + 'Missing Pod\'s RDF schema version.');

    this.sparql.registerSchemas([
      // Shared schema
      {
        name: 'sarshr',
        prefix: 'sarshr',
        namespace: schema.namespace + 'shr/' + schema.version + '/',
      },
      // Transcribing schema
      {
        name: 'sartscb',
        prefix: 'sartscb',
        namespace: schema.namespace + 'tscb/' + schema.version + '/',
      },
      // Organization schema
      {
        name: 'sarorg',
        prefix: 'sarorg',
        namespace: schema.namespace + 'org/' + schema.version + '/',
      }
    ]);
  }

  protected bindFilter(filter: Partial<InterlocutorFilter>, opts = {exactMatch: false}): string {
    filter = this.asFilter(filter) || new InterlocutorFilter();

    // By default, enable search on name
    const filterKeys = ['anyMatch'];
    const variables: Record<string, any> = {
      searchField: <keyof Interlocutor>'name'
    };

    // Split search text as terms (e.g. keep phrases protected by double quote)
    const searchTerms = SparqlUtils.getSearchTerms(filter.searchText);

    // INC code
    let code = filter.code?.trim();
    // When no code, try to parse it input search, to find a code
    if (isNilOrBlank(filter.code) && isNotEmptyArray(searchTerms)) {
      const codeIndex = searchTerms.findIndex(s => INC_CODE_REGEXP.test(s));
      if (codeIndex !== -1) {
        code = searchTerms.splice(codeIndex, 1)[0];
      }
    }
    if (isNotNilOrBlank(code)) {
      variables.code = code.trim();
      if (!filterKeys.includes('codeExactMatch')) filterKeys.push('codeExactMatch');
    }

    // EDMO
    if (isNotNilOrBlank(filter.edmo)) {
      variables.edmo = filter.edmo.trim();
      if (!filterKeys.includes('edmoExactMatch')) filterKeys.push('edmoExactMatch');
    }

    // SIRET
    if (isNotNilOrBlank(filter.siret)) {
      variables.siret = filter.siret.trim();
      if (!filterKeys.includes('siretExactMatch')) filterKeys.push('siretExactMatch');
    }

    // Validity Status
    if (isNotNilOrBlank(filter.validityStatus)) {
      variables.validityStatus = filter.validityStatus;
      if (!filterKeys.includes('validityStatusExactMatch')) filterKeys.push('validityStatusExactMatch');
    }

    // Country
    if (isNotNilOrBlank(filter.country)) {
      variables.country = filter.country;
      if (!filterKeys.includes('countryPrefixMatch')) filterKeys.push('countryPrefixMatch');
    }

    const filters = filterKeys
      .map(key => {
        // If exactMatch, replace 'prefixMatch' with 'exactMatch'
        if (opts.exactMatch) {
          if (key === 'prefixMatch' || key === 'anyMatch') return 'exactMatch';
          if (key === 'cityPrefixMatch' || key === 'cityAnyMatch') return 'cityExactMatch';
          if (key === 'codePrefixMatch') return 'codeExactMatch';
          if (key === 'edmoPrefixMatch') return 'edmoExactMatch';
          if (key === 'siretPrefixMatch') return 'siretExactMatch';
          if (key === 'countryPrefixMatch') return 'countryExactMatch';
        }
        return key;
      })
      .map(key => FILTER_MAP[key])
      .filter(isNotNilOrBlank);

    // Set q with search terms
    variables.q = searchTerms;

    return SparqlUtils.bindFilters(filters, variables, {exactMatch: opts?.exactMatch});
  }

}
