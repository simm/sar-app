import {Entity, EntityAsObjectOptions, EntityClass, EntityFilter, FilterFn, fromDateISOString, ReferentialRef, splitByProperty, toDateISOString} from '@sumaris-net/ngx-components';
import {Moment} from 'moment';
import {StoreObject} from '@apollo/client/core';

export const SandreValidityStatusList = Object.freeze([
  {
    id: 'Validé',
    uri: 'http://id.eaufrance.fr/nsa/390#Validé',
    icon: 'checkmark',
    label: 'REFERENTIAL.VALIDITY_STATUS_ENUM.VALID'
  },
  {
    id: 'Gelé',
    uri: 'http://id.eaufrance.fr/nsa/390#Gelé',
    icon: 'close',
    label: 'REFERENTIAL.VALIDITY_STATUS_ENUM.INVALID'
  }
]);
export const SandreValidityStatusByUri = splitByProperty(SandreValidityStatusList, 'uri');

@EntityClass({typename: 'InterlocutorVO'})
export class Interlocutor extends Entity<Interlocutor, string> {
  static fromObject: (source: any, opts?: any) => Interlocutor;

  code: string = null;
  name: string = null;
  address: string = null;
  country: string = null;
  validityStatus: string = null;
  creationDate: Moment = null;
  isMain: boolean = null;

  siret: string = null;
  edmo: string = null;

  constructor() {
    super(Interlocutor.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source);
    Object.assign(this, source);
    this.creationDate = fromDateISOString(this.creationDate);
    // eslint-disable-next-line eqeqeq
    this.isMain = source.isMain == 1 || source.isMain == true;
  }

  asObject(opts?: EntityAsObjectOptions): StoreObject {
    const target = super.asObject(opts);
    target.creationDate = toDateISOString(this.creationDate);
    return target;
  }
}


@EntityClass({ typename: 'InterlocutorFilterVO' })
export class InterlocutorFilter extends EntityFilter<InterlocutorFilter, Interlocutor, number> {
  static fromObject: (source: any, opts?: any) => InterlocutorFilter;

  searchText: string;
  code: string;
  edmo: string;
  siret: string;
  validityStatus: string;
  country: string;

  constructor() {
    super(InterlocutorFilter.TYPENAME);
  }

  fromObject(source: any, opts?: any) {
    super.fromObject(source, opts);
    this.searchText = source.searchText;
    this.code = source.code;
    this.edmo = source.edmo;
    this.siret = source.siret;
    this.validityStatus = source.validityStatus;
    this.country = source.country;
  }

}
