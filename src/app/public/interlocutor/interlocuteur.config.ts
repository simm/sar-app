import {FormFieldDefinition} from '@sumaris-net/ngx-components';

export const INTERLOCUTOR_CONFIG_OPTIONS = {
  INTERLOCUTOR_HELP_URL: <FormFieldDefinition>{
    key: 'interlocuteur.help.url',
    label: 'INTERLOCUTEUR.OPTIONS.HELP_URL',
    type: 'string',
    defaultValue: 'https://sar.milieumarinfrance.fr/content/download/7487/file/GuideUtilisationOutilInterlocuteurs.pdf'
  },
  INTERLOCUTOR_NEW_DATA_URL: <FormFieldDefinition>{
    key: 'interlocuteur.newData.url',
    label: 'INTERLOCUTEUR.OPTIONS.NEW_DATA_URL',
    type: 'string',
    defaultValue: 'https://mdm.sandre.eaufrance.fr/node/add/interlocuteur?origine=SIMM'
  },

};
