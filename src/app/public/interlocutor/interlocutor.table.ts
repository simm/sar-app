import {AfterViewInit, ChangeDetectionStrategy, Component, Injector, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {AccountService, ConfigService, isNil, isNotNilOrBlank, MenuService, PlatformService, slideUpDownAnimation, splitById, toBoolean, waitFor} from '@sumaris-net/ngx-components';
import {Interlocutor, InterlocutorFilter, SandreValidityStatusList} from './interlocutor.model';
import {InterlocutorService} from './interlocutor.service';
import {PUBLIC_CONFIG_DEFAULTS} from '@app/public/public.config';
import {INTERLOCUTOR_CONFIG_OPTIONS} from '@app/public/interlocutor/interlocuteur.config';
import {ColumnType, ReadonlyBaseTable} from '@app/shared/table/readonly-base.table';
import {TableElement} from '@e-is/ngx-material-table';
import {BehaviorSubject} from 'rxjs';
import {debounceTime, tap} from 'rxjs/operators';
import {Popovers} from '@app/shared/popover/popover.utils';
import {PopoverController} from '@ionic/angular';


@Component({
  selector: 'app-interlocutor-table',
  templateUrl: './interlocutor.table.html',
  styleUrls: ['../public.scss', './interlocutor.table.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [slideUpDownAnimation]
})
export class InterlocutorTable
  extends ReadonlyBaseTable<Interlocutor, string, InterlocutorFilter>
  implements OnInit, AfterViewInit {

  helpUrl: string;
  newDataUrl: string;
  validityStatusList = SandreValidityStatusList;
  validityStatusById = splitById(this.validityStatusList);
  $countries = new BehaviorSubject<string[]>(undefined);

  @Input() showToolbar = true;
  @Input() showFooter = true;
  @Input() debounceTime: number = PUBLIC_CONFIG_DEFAULTS.debounceTime;
  protected popoverController: PopoverController;

  constructor(
    protected injector: Injector,
    protected platform: PlatformService,
    protected accountService: AccountService,
    protected entityService: InterlocutorService,
    protected configService: ConfigService,
    protected menu: MenuService
  ) {
    super(
      injector,
      Interlocutor,
      InterlocutorFilter,
      entityService,
      {
        i18nColumnPrefix: 'INTERLOCUTOR.SEARCH.',
        // restoreFilterOrLoad: accountService.isLogin() || false,
        restoreCompactMode: accountService.isLogin() || false,
        restoreColumnWidths: accountService.isLogin() || false
      }
    );

    this.logPrefix = '[interlocutor-table] ';
    this.sticky = true;
    this.canDownload = true;
    this.defaultSortBy = 'label';
    this.defaultSortDirection = 'asc';
  }

  ngOnInit() {

    // Configure from route data (.e.g menu, toolbar, etc.)
    const routeData = this.route.snapshot.data;
    if (routeData) {
      const showMenu = toBoolean(routeData.menu, true);
      this.menu.enable(showMenu);
      this.showToolbar = toBoolean(routeData.showToolbar, showMenu);
      this.showFooter = toBoolean(routeData.showFooter, !this.mobile);
    }

    super.ngOnInit();

    // Get help page URL
    this.registerSubscription(
      this.configService.config.subscribe(config => {
        this.helpUrl = config.getProperty(INTERLOCUTOR_CONFIG_OPTIONS.INTERLOCUTOR_HELP_URL);
        this.newDataUrl = config.getProperty(INTERLOCUTOR_CONFIG_OPTIONS.INTERLOCUTOR_NEW_DATA_URL);
      })
    );


    // Load countries
    this.registerSubscription(
      this.entityService.watchCountries()
        .subscribe(items => {
          this.$countries.next(items);
          this.markAsReady();
        })
    );

    this.registerSubscription(
      this.filterForm.get('searchText').valueChanges
        .pipe(
          tap(() => this.markAsLoading()),
          debounceTime(this.debounceTime)
        ).subscribe(q => {
          if (isNotNilOrBlank(q)) {
            this.onRefresh.emit();
          }
          else {
            this.totalRowCount === null;
            this.markAsLoaded();
          }
        })
    );

  }

  // Override default behaviour, to avoid
  resetFilter(event?: UIEvent) {
    this.totalRowCount = null; // Hide table and footer
    super.resetFilter(event, {emitEvent: false /*avoid to refresh table*/});
    this.resetError();
  }

  applyFilterAndClosePanel(event?: UIEvent) {
    this.resetError();
    super.applyFilterAndClosePanel(event);
  }

  async showHelp() {
    if (!this.helpUrl) {
      await waitFor(() => !!this.helpUrl);
    }
    this.platform.open(this.helpUrl, '_blank');
  }

  clickRow(event: UIEvent | undefined, row: TableElement<Interlocutor>): boolean {
    const data = row.currentData;
    if (isNotNilOrBlank(data.id)) {
      this.platform.open(data.id, '_blank');
      return true;
    }
    return false;
  }

  showEmbeddedPopover(event: UIEvent) {
    if (!this.popoverController) {
      this.popoverController = this.injector.get(PopoverController);
    }
    const url = window.location.origin
      + this.router.createUrlTree(['..', 'embed'], {relativeTo: this.route}).toString();
    const title = this.translate.instant(this.title);
    const html = `<iframe src="${url}" title="${title}" width="100%" height="800px" sandbox="allow-same-origin allow-scripts allow-popups allow-forms allow-downloads" frameborder="0" allow="clipboard-write;"></iframe>`;
    return Popovers.showText(this.popoverController, event,
      {
      editing: false,
      multiline: true,
      text: html,
      placeholder: this.translate.instant('INTERLOCUTOR.SEARCH.EMBEDDED_IFRAME_HELP'),
      maxLength: 0 // Disable text progress hint
    });
  }

  /* -- protected functions -- */

  protected getEntityDisplayProperties<T>(dataType: { new(): any }): string[] {
    return super.getEntityDisplayProperties(dataType)
      // Insert updateDate just after 'creationDate'
      .reduce((res, key) => {
        if (key === <keyof Interlocutor>'creationDate') return res.concat(key, <keyof Interlocutor>'updateDate');
        return res.concat(key);
      }, []);
  }

  protected getColumnType(key: string): ColumnType {
    if (key === <keyof Interlocutor>'isMain') return 'boolean';
    return super.getColumnType(key);
  }

  protected getFilterFormConfig(): any {
    console.debug('[interlocuteur-table] Creating filter form group...');
    return {
      searchText: [null],
      code: [null],
      siret: [null],
      edmo: [null],
      country: [null],
      validityStatus: [SandreValidityStatusList[0]?.id] // Valid by default
    };
  }

}
