import {NgModule} from '@angular/core';
import {AdminModule, SocialModule} from '@sumaris-net/ngx-components';
import {CommonModule} from '@angular/common';
import {AppCoreModule} from '@app/core/core.module';
import {NgxJdenticonModule} from 'ngx-jdenticon';
import {AppReferentialModule} from '@app/referential/referential.module';
import {ConfigurationPage} from '@app/admin/config/configuration.page';


@NgModule({
  imports: [
    CommonModule,
    SocialModule,
    NgxJdenticonModule,

    // App modules
    AdminModule,
    AppCoreModule,
    AppReferentialModule
  ],
  declarations: [
    ConfigurationPage
  ],
  exports: [
    AdminModule,
    ConfigurationPage
  ]
})
export class AppAdminModule {

}
