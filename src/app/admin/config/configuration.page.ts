import {ChangeDetectionStrategy, Component, Inject, Injector, Optional} from '@angular/core';
import {
  Alerts,
  APP_CONFIG_OPTIONS,
  ConfigService,
  Configuration,
  Department,
  EntityServiceLoadOptions,
  firstNotNilPromise,
  FormFieldDefinitionMap,
  HistoryPageReference,
  NetworkService
} from '@sumaris-net/ngx-components';
import {SoftwareValidatorService} from '@app/referential/software/software.validator';
import {BehaviorSubject} from 'rxjs';
import {AbstractSoftwarePage} from '@app/referential/software/abstract-software.page';


@Component({
  selector: 'app-configuration-page',
  templateUrl: './configuration.page.html',
  styleUrls: ['./configuration.page.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ConfigurationPage extends AbstractSoftwarePage<Configuration, ConfigService> {

  partners = new BehaviorSubject<Department[]>(null);

  get config(): Configuration {
    return this.data && (this.data as Configuration) || undefined;
  }

  constructor(
    injector: Injector,
    validatorService: SoftwareValidatorService,
    public dataService: ConfigService,
    public network: NetworkService,
    @Optional() @Inject(APP_CONFIG_OPTIONS) configOptions: FormFieldDefinitionMap,
  ) {
    super(injector,
      Configuration,
      dataService,
      validatorService,
      configOptions,
      {
        tabCount: 3
      });

    // default values
    this.defaultBackHref = null;

    //this.debug = !environment.production;
    this.markAsReady();
  }

  async load(id?: number, opts?: EntityServiceLoadOptions): Promise<void> {

    const config = await firstNotNilPromise(this.dataService.config);

    // Force the load of the config
    await super.load(config.id, {...opts, fetchPolicy: "network-only"});

  }

  protected setValue(data: Configuration) {
    if (!data) return; // Skip

    const json = data.asObject();
    this.partners.next(json.partners);

    super.setValue(data);
  }

  protected async getJsonValueToSave(): Promise<any> {
    const json = await super.getJsonValueToSave();

    // Re add partners
    json.partners = this.partners.getValue();

    return json;
  }


  async clearCache(event?: UIEvent, cacheName?: string) {
    const confirm = await Alerts.askActionConfirmation(this.alertCtrl, this.translate, true, event);
    if (confirm) {
      await this.network.clearCache();
      await this.settings.removeOfflineFeatures();
      await this.dataService.clearCache({cacheName: cacheName});
    }
  }

  protected async computePageHistory(title: string): Promise<HistoryPageReference> {
    return null; // No page history
  }
}

