import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {AuthGuardService, SharedRoutingModule, UsersPage} from '@sumaris-net/ngx-components';
import {AppAdminModule} from '@app/admin/admin.module';
import {ConfigurationPage} from '@app/admin/config/configuration.page';

const routes: Routes = [
  {
    path: 'users',
    pathMatch: 'full',
    component: UsersPage,
    canActivate: [AuthGuardService],
    data: {
      profile: 'ADMIN'
    }
  },
  {
    path: 'config',
    pathMatch: 'full',
    component: ConfigurationPage,
    canActivate: [AuthGuardService],
    data: {
      profile: 'ADMIN'
    }
  }
];

@NgModule({
  imports: [
    SharedRoutingModule,
    AppAdminModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class SarAdminRoutingModule { }
