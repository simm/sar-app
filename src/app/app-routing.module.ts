import {NgModule} from '@angular/core';
import {ExtraOptions, RouterModule, Routes} from '@angular/router';
import {AccountPage, AuthGuardService, HomePage, SettingsPage, SharedRoutingModule} from '@sumaris-net/ngx-components';
import {QuicklinkModule, QuicklinkStrategy} from 'ngx-quicklink';

const routes: Routes = [
  // Core path
  {
    path: '',
    component: HomePage
  },

  {
    path: 'home/:action',
    component: HomePage
  },
  {
    path: 'account',
    pathMatch: 'full',
    component: AccountPage,
    canActivate: [AuthGuardService]
  },
  {
    path: 'settings',
    pathMatch: 'full',
    component: SettingsPage
  },

  // Admin
  {
    path: 'admin',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./admin/admin-routing.module').then(m => m.SarAdminRoutingModule)
  },

  // Referential
  {
    path: 'referential',
    canActivate: [AuthGuardService],
    loadChildren: () => import('./referential/referential-routing.module').then(m => m.ReferentialRoutingModule)
  },

  // HubEau
  {
    path: 'hubeau',
    loadChildren: () => import('./hubeau/hubeau-routing.module').then(m => m.HubEauRoutingModule)
  },

  // Public
  {
    path: 'public',
    loadChildren: () => import('./public/public-routing.module').then(m => m.AppPublicRoutingModule)
  },

  // Data
  {
    path: 'data',
    loadChildren: () => import('./data/data-routing.module').then(m => m.DataRoutingModule),
  },

  // Test module (disable in menu, by default - can be enable by the Pod configuration page)
  {
    path: 'testing',
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'shared',
      },
      // Shared module
      {
        path: 'shared',
        loadChildren: () => import('@sumaris-net/ngx-components').then(m => m.SharedTestingModule)
      }
    ]
  },

  // Other route redirection (should at the end of the array)
  {
    path: '**',
    redirectTo: '/'
  }
];

export const ROUTE_OPTIONS: ExtraOptions = {
  enableTracing: false,
  //enableTracing: !environment.production,
  useHash: false,
  onSameUrlNavigation: 'reload',
  preloadingStrategy: QuicklinkStrategy
};

@NgModule({
  imports: [
    QuicklinkModule,
    SharedRoutingModule,
    RouterModule.forRoot(routes, ROUTE_OPTIONS)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
