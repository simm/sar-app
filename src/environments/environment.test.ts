// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
import 'zone.js/dist/zone-error';

// Environment to use only with unit tests
import {Environment} from '@sumaris-net/ngx-components';

const pkg = require('../../package.json');

export const environment = Object.freeze(<Environment>{
  name: (pkg.name as string),
  version: (pkg.version as string),
  production: false,
  baseUrl: '/',
  defaultLocale: 'fr',
  defaultLatLongFormat: 'DDMM',
  apolloFetchPolicy: 'cache-first',
  mock: false,

  // FIXME: GraphQL subscription never unsubscribe...
  listenRemoteChanges: false,

  // FIXME: enable cache
  persistCache: false,

  peerMinVersion: '0.0.1',

  // TODO: make this works
  //offline: true,

  defaultPeer: {
    host: 'localhost',
    port: 8080
  },
  defaultPeers: [
    {
      host: 'localhost',
      port: 8080
    },
    {
      host: 'localhost',
      port: 8081
    },
    {
      host: 'visi-common-docker1.ifremer.fr',
      port: 8084
    },
    {
      host: 'id.milieumarinfrance.fr',
      port: 443
    }
  ],

  defaultAppName: 'SAR',
  defaultAndroidInstallUrl: 'https://play.google.com/store/apps/details?id=fr.milieumarinfrance.sar.app',

  // Storage
  storage: {
    driverOrder: ['sqlite', 'indexeddb', 'websql', 'localstorage']
  },

  account: {
    showAccountButton: false,
    listenIntervalInSeconds: 30,
    enableListenChanges: true
  },

  // About
  sourceUrl: 'https://gitlab.ifremer.fr/simm/sar-app',
  reportIssueUrl: 'https://gitlab.ifremer.fr/simm/sar-app/-/issues',
  helpUrl: 'https://gitlab.ifremer.fr/simm/sar-doc/-/tree/master/user-manual/index.md'
});

