import {Environment} from '@sumaris-net/ngx-components';

const pkg = require('../../package.json');

/* eslint-disable */
export const environment = Object.freeze(<Environment>{
  name: (pkg.name as string),
  version: (pkg.version as string),
  production: true,
  baseUrl: "/",
  defaultLocale: "fr",
  defaultLatLongFormat: "DDMM",
  apolloFetchPolicy: "cache-first",
  mock: false,

  // Must be change manually. Can be override using Pod properties 'quadrige3.app.min.version'
  peerMinVersion: '0.0.1',

  // FIXME: GraphQL subscription never unsubscribe...
  listenRemoteChanges: false,

  // FIXME: enable cache
  persistCache: false,

  // Leave null,
  defaultPeer: {
    host: 'id.milieumarinfrance.fr',
    port: 443
  },

  // Production and public peers
  defaultPeers: [
    {
      host: 'visi-common-docker1.ifremer.fr',
      port: 8084
    },
    {
      host: 'id.milieumarinfrance.fr',
      port: 443
    },
    {
      host: 'simm-pod.isival.ifremer.fr',
      port: 443
    }
  ],

  defaultAppName: 'SAR',
  defaultAndroidInstallUrl: 'https://play.google.com/store/apps/details?id=fr.milieumarinfrance.sar.app',

  // Storage
  storage: {
    driverOrder: ['sqlite', 'indexeddb', 'websql', 'localstorage']
  },

  account: {
    showAccountButton: false,
    listenIntervalInSeconds: 30,
    enableListenChanges: true
  },

  // About
  sourceUrl: 'https://gitlab.ifremer.fr/simm/sar-app',
  reportIssueUrl: 'https://gitlab.ifremer.fr/simm/sar-app/-/issues',
  helpUrl: 'https://gitlab.ifremer.fr/simm/sar-doc/-/tree/master/user-manual/index.md'
});
/* eslint-enable */
